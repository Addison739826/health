package com.uphone.chronicdisease.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.CheckBean;

import java.util.List;


/**
 * @Author: lzy
 * @CreateDate: 2019/12/25
 * @Description:
 */
public class SingleTextAdapter extends BaseQuickAdapter<CheckBean, BaseViewHolder> {


    public SingleTextAdapter(@Nullable List<CheckBean> data) {
        super(R.layout.item_single_text, data);
    }

    boolean flag = false;

    public SingleTextAdapter(int layoutResId, @Nullable List<CheckBean> data) {
        super(layoutResId, data);
        flag = !flag;
    }

    @Override
    protected void convert(BaseViewHolder helper, CheckBean item) {
        helper.setText(R.id.tv, item.getText());
        if (item.isChecked()) {
            if(flag){
                helper.setBackgroundColor(R.id.tv, Color.parseColor("#e0eaf6"));
                helper.setTextColor(R.id.tv, Color.parseColor("#333333"));
            }else{
                helper.setBackgroundColor(R.id.tv, Color.parseColor("#e3eaef"));
                helper.setTextColor(R.id.tv, Color.parseColor("#0063BE"));
            }
        } else {
            if (flag) {
                helper.setBackgroundRes(R.id.tv,R.drawable.shape_rectangle_gray2);
                helper.setTextColor(R.id.tv, Color.parseColor("#666666"));
            }else{
                helper.setBackgroundColor(R.id.tv, Color.parseColor("#ffffff"));
                helper.setTextColor(R.id.tv, Color.parseColor("#000000"));
            }

        }

    }
}
