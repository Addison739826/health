package com.uphone.chronicdisease.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.AuditFailedBean;
import com.uphone.chronicdisease.bean.CdkProjectBean;
import com.uphone.chronicdisease.bean.UnapproveBean;

/**
 * @ClassName: UnapproveDataAdapter
 * @Description: 审核未通过列表
 * @Date: 2019/12/23
 * @Author: zx
 */
public class UnapproveDataAdapter extends BaseQuickAdapter<AuditFailedBean, BaseViewHolder> {
    private LinearLayout mLl_add_subject;
    private Activity mActivity;
    private TextView mTvsubject;

    public UnapproveDataAdapter(Activity activity) {
        super(R.layout.item_unapprove);
        mActivity = activity;

    }


    @Override
    protected void convert(BaseViewHolder helper, AuditFailedBean item) {
        mLl_add_subject = helper.getView(R.id.ll_add_subject);
        TextView tv_name = helper.getView(R.id.tv_name);
        TextView tv_group_type = helper.getView(R.id.tv_group_type);
        mTvsubject =  helper.getView(R.id.tv_subject);
        tv_name.setText(item.getPat_name());

        if ("开同组".equals(item.getRes_name())) {
            tv_group_type.setBackgroundResource(R.drawable.btn_shape_orange);
        } else {
            tv_group_type.setBackgroundResource(R.drawable.btn_shape_green);
        }
        tv_group_type.setText(item.getRes_name());

        helper.setText(R.id.tv_date, item.getUpdate_time())
                .setText(R.id.tv_state,item.getTitle())
                .setText(R.id.tv_number,item.getData_id()+"")
                .setText(R.id.tv_sex,item.getSex() == 0 ? "女" : "男")
                .setText(R.id.tv_age,item.getAge()+"岁")
                .setText(R.id.tv_condition,item.getDiagnosis())
                .setText(R.id.tv_diagnosis,item.getReason());

        if(item.getLable().contains("T0")){
            mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.dfGreen)));
        }else if(item.getLable().contains("T6")){
            mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.text_date)));
        }else if(item.getLable().contains("T3")){
            mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.color_red)));
        }
        mTvsubject.setText(item.getLable());
    }
}