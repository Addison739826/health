package com.uphone.chronicdisease.adapter;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;

import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.SelectPictureBean;

/**
 * @author 侯娜
 * @version 1.0 2018/4/11 15:28
 * @package com.heyi.oa.view.adapter.word.newIntelligence
 * @fileName SelectPictureAdapter
 * @describe 选择图片adapter
 */
public class SelectPictureAdapter extends BaseMultiItemQuickAdapter<SelectPictureBean, BaseViewHolder> {

    public static final int TYPE_NORMAL = 101; // 之前普通的选择图片
    public static final int TYPE_ARTICLE = 102; // 发布文章图片

    public int mType;

    public SelectPictureAdapter(List data) {
      this(data, TYPE_NORMAL);
    }

    public SelectPictureAdapter(List data, int type){
        super(data);
        mType = type;
        switch (type){
            case TYPE_NORMAL:
                addItemType(SelectPictureBean.TYPE_PICTURE, R.layout.recycler_select_picture);
                addItemType(SelectPictureBean.TYPE_ADD, R.layout.recycler_select_picture_add);
                break;
            /*case TYPE_ARTICLE:
                addItemType(SelectPictureBean.TYPE_ADD, R.layout.recycler_select_picture_add_nursing);
                addItemType(SelectPictureBean.TYPE_PICTURE, R.layout.recycler_select_picture_nursing);
                break;*/
        }

    }

    @Override
    protected void convert(BaseViewHolder helper, SelectPictureBean item) {
        switch (helper.getItemViewType()){
            case SelectPictureBean.TYPE_ADD:
                helper.addOnClickListener(R.id.iv_add_img);
                helper.addOnClickListener(R.id.iv_selected);

                break;
            case SelectPictureBean.TYPE_PICTURE:
                helper.addOnClickListener(R.id.iv_selected);
                helper.addOnClickListener(R.id.iv_delete);
                ImageView ivSelected = helper.getView(R.id.iv_selected);
                if(mType == TYPE_ARTICLE){
                   /* Glide.with(helper.itemView).load(item.getPicture())
                            .apply(GlideUtils.getRoundCorner())
                            .into(ivSelected);*/
                }else if(mType == TYPE_NORMAL){
                    Glide.with(helper.itemView).load(item.getPicture())
                            .into(ivSelected);
                }

                break;
        }

    }
}
