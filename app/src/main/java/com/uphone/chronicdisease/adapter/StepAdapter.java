package com.uphone.chronicdisease.adapter;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.CdkProjectBean;

import java.util.List;

/**
 * @Author: lzy
 * @CreateDate: 2020/1/7
 * @Description:
 */
public class StepAdapter extends BaseQuickAdapter<CdkProjectBean.Patientlist.Research, BaseViewHolder> {
    private Activity mActivity;
    public StepAdapter(@Nullable List<CdkProjectBean.Patientlist.Research> data,Activity activity) {
        super(R.layout.children_add_subject,data);
        mActivity = activity;
    }

    @Override
    protected void convert(BaseViewHolder helper,CdkProjectBean.Patientlist.Research item) {
        helper.setText(R.id.tv_subject,item.getStepName());
        TextView mTvsubject = helper.getView(R.id.tv_subject);
        if(item.getStepName().contains("T0")){
            mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.dfGreen)));
        }else if(item.getStepName().contains("T6")){
            mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.text_date)));
        }else if(item.getStepName().contains("T3")){
            mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.color_red)));
        }
    }
}
