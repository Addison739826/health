package com.uphone.chronicdisease.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.CdkProjectBean;
import com.uphone.chronicdisease.bean.MessageBean;

/**
 * @ClassName: CdkProjectDetailAdapter
 * @Description: cdk项目详情Adapter
 * @Date: 2019/12/22
 * @Author: zx
 */
public class MessageAdapter extends BaseQuickAdapter<MessageBean, BaseViewHolder> {
    private Activity mActivity;
    public MessageAdapter(Activity activity) {
        super(R.layout.item_message);
        mActivity = activity;

    }


    @Override
    protected void convert(BaseViewHolder helper, MessageBean item) {

        helper.setText(R.id.tv_research_group,"研究组："+item.getResearch());
        helper.setText(R.id.tv_time,item.getSendTime());
        helper.setText(R.id.tv_title,item.getTitle());
        helper.setText(R.id.tv_uptime,"上传时间："+item.getUpTime());
        helper.setText(R.id.tv_reason,"无效原因："+item.getReason());



    }
}