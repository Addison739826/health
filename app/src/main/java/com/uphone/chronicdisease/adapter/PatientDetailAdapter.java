package com.uphone.chronicdisease.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.PatientDetailBean;
import com.uphone.chronicdisease.bean.researchStrpBean;

/**
 * @ClassName: ResearchAdapter
 * @Description: 患者信息详情
 * @Date: 2019/12/22
 * @Author: zx
 */
public class PatientDetailAdapter extends BaseQuickAdapter<PatientDetailBean.Step, BaseViewHolder> {
    private Activity mActivity;
    private TextView tv_type;
    public PatientDetailAdapter(Activity activity) {
        super(R.layout.item_research);
        mActivity = activity;

    }


    @Override
    protected void convert(BaseViewHolder helper, PatientDetailBean.Step item) {

        helper.setText(R.id.tv_title,item.getTitle());
        helper.setText(R.id.tv_time,"更新时间："+item.getUpTime());

        TextView tv_stepRemark = helper.getView(R.id.tv_stepRemark);
        tv_stepRemark.setVisibility(View.GONE);
        tv_type = helper.getView(R.id.tv_type);
        if(item.getState() == 0){//未完成
            tv_type.setText("未完成");
            tv_type.setTextColor(mActivity.getResources().getColor(R.color.text_date));
        }else if(item.getState() == 1){//已提交
            tv_type.setText("已提交");
            tv_type.setTextColor(mActivity.getResources().getColor(R.color.dfGreen));
        }else if(item.getState() == 2){//审核未通过
            tv_type.setText("审核未通过");
            tv_type.setTextColor(mActivity.getResources().getColor(R.color.dfRed));
        }else if(item.getState() == 3){//已完成
            tv_type.setText("已完成");
            tv_type.setTextColor(mActivity.getResources().getColor(R.color.dfGreen));
        }




    }
}