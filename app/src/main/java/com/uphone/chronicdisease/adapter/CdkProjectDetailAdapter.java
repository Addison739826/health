package com.uphone.chronicdisease.adapter;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.uphone.chronicdisease.bean.CdkProjectBean;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.Patientlist;

import java.util.List;

/**
 * @ClassName: CdkProjectDetailAdapter
 * @Description: cdk项目详情Adapter
 * @Date: 2019/12/22
 * @Author: zx
 */
public class CdkProjectDetailAdapter extends BaseQuickAdapter<Patientlist, BaseViewHolder> {
    private LinearLayout mLl_add_subject;
    private Activity mActivity;
    private TextView mTvsubject;
    private ImageView mIvSelect;
    private boolean isShow;

    private boolean isCheckAll;


    public CdkProjectDetailAdapter(Activity activity) {
        super(R.layout.item_cdk_project_details);
        mActivity = activity;

    }


    @Override
    protected void convert(BaseViewHolder helper, Patientlist item) {
        TextView tv_group_show = helper.getView(R.id.tv_group_type);

        helper.setText(R.id.tv_name, item.getPatient().getPat_name())
                .setText(R.id.tv_group_type, item.getPatient().getRes_name())
                // .setText(R.id.tv_date,item.getPatient().get)
                .setText(R.id.tv_state, item.getPatient().getTitle())
                .setText(R.id.tv_number, item.getPatient().getId() + "")
                .setText(R.id.tv_sex, item.getPatient().getSex() == 0 ? "女" : "男")
                .setText(R.id.tv_age, item.getPatient().getAge() + "")
                .setText(R.id.tv_condition, item.getPatient().getDiagnosis());

        if ("开同组".equals( item.getPatient().getRes_name())) {
            tv_group_show.setBackgroundResource(R.drawable.btn_shape_orange);
        } else {
            tv_group_show.setBackgroundResource(R.drawable.btn_shape_green);
        }

        mLl_add_subject = helper.getView(R.id.ll_add_subject);
        mLl_add_subject.removeAllViews();
        /*for (int i = 0; i < item.getResearch().size(); i++) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.children_add_subject, null);
            mTvsubject = view.findViewById(R.id.tv_subject);
            mLl_add_subject.addView(view);
            mTvsubject.setText(item.getResearch().get(i).getStepName());
            if(item.getResearch().get(i).getStepName().contains("T0")){
                mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.dfGreen)));
            }else if(item.getResearch().get(i).getStepName().contains("T6")){
                mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.text_date)));
            }else if(item.getResearch().get(i).getStepName().contains("T3")){
                mTvsubject.setTextColor((mActivity.getResources().getColor(R.color.color_red)));
            }
        }*/

        RecyclerView recyclerView = helper.getView(R.id.rv_step);
        StepAdapter stepAdapter = new StepAdapter(item.getResearch(),mActivity);
        recyclerView.setLayoutManager(new GridLayoutManager(mActivity, item.getResearch().size()==0?1:item.getResearch().size()));
        recyclerView.setAdapter(stepAdapter);
        mIvSelect = (ImageView) helper.getView(R.id.iv_select);
        if (item.isChoosed) {
            helper.getView(R.id.iv_select).setBackgroundResource(R.mipmap.selected);
        } else {
            helper.getView(R.id.iv_select).setBackgroundResource(R.mipmap.unselect);
        }
        if (isShow) {
            helper.getView(R.id.iv_select).setVisibility(View.VISIBLE);
        } else {
            helper.getView(R.id.iv_select).setVisibility(View.GONE);
        }

    }

    public void changeSwitch(int position) {
        if (getData().get(position).isChoosed) {
            getData().get(position).setChoosed(false);
        } else {
            getData().get(position).setChoosed(true);
        }

        notifyDataSetChanged();
    }

    public void showSwitch(boolean isShow) {
        this.isShow = isShow;
        notifyDataSetChanged();
    }

}