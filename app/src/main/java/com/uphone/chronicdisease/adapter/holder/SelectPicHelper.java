package com.uphone.chronicdisease.adapter.holder;


import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.luck.picture.lib.entity.LocalMedia;
import java.util.ArrayList;
import java.util.List;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.adapter.SelectPictureAdapter;
import com.uphone.chronicdisease.base.BaseTakePhotoActivity;
import com.uphone.chronicdisease.bean.SelectPictureBean;

import static com.uphone.chronicdisease.bean.SelectPictureBean.TYPE_ADD;
import static com.uphone.chronicdisease.bean.SelectPictureBean.TYPE_PICTURE;



/**
 * Created by 沿用之前上传图片封装
 * 包名：com.meiduoduo.adapter.holder
 *
 * @time 2018/11/29 10:00
 * 描述：上传图片选择器
 */
public class SelectPicHelper {

    private BaseTakePhotoActivity mActivity;
    private RecyclerView mRv;
    private SelectPictureAdapter mAdapter;
    private int mMaxNums;
    private boolean hasAddType = true;

    public SelectPicHelper(BaseTakePhotoActivity activity, RecyclerView rv, int maxNums, int adapterType, int spanCount,List<String> list) {
        mActivity= activity;
        mRv = rv;
        mMaxNums = maxNums;
        initPics(spanCount, adapterType);
    }

    public void initPics(int spanCount, int adapterType) {
        mRv.setLayoutManager(new GridLayoutManager(mActivity, spanCount));
        List<SelectPictureBean> list = new ArrayList();
        list.add(new SelectPictureBean(TYPE_ADD, null));
        mAdapter = new SelectPictureAdapter(list, adapterType);
        mRv.setAdapter(mAdapter);
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.iv_add_img:
                        int maxCount = Math.max(mMaxNums + 1 - mAdapter.getItemCount(), 1);
                        mActivity.getPhoto(false, maxCount);
                        break;
                    case R.id.iv_delete:
                        mAdapter.remove(position);

                       /* if (!hasAddType) { // 说明目前没有添加图标
                            mAdapter.addData(mMaxNums - 1, new SelectPictureBean(TYPE_ADD, null));
                            hasAddType = true;
                        }*/
                        break;
                }
            }
        });
    }

    public void onActivityResult(List<LocalMedia> selectList) {
        if((mAdapter.getItemCount() + selectList.size() == mMaxNums + 1)){
            mAdapter.remove(mAdapter.getItemCount() - 1);
            hasAddType = false;

        }
        ArrayList<LocalMedia> medias = (ArrayList<LocalMedia>) selectList;
        for(LocalMedia localMedia: medias){
            String compressPath = localMedia.getCompressPath();
            mAdapter.addData(mAdapter.getItemCount(), new SelectPictureBean(TYPE_PICTURE, compressPath));
        }



    }

    public ArrayList<SelectPictureBean> getData(){
        if(mAdapter == null){
            return new ArrayList<>();
        }
        return (ArrayList<SelectPictureBean>) mAdapter.getData();
    }

}
