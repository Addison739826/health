package com.uphone.chronicdisease.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.CheckBean;
import com.uphone.chronicdisease.bean.PatientDataBean;

import java.util.List;


/**
 * @Author: lzy
 * @CreateDate: 2019/12/25
 * @Description:
 */
public class IndexAdapter extends BaseQuickAdapter<PatientDataBean.DataBean, BaseViewHolder> {


    public IndexAdapter(@Nullable List<PatientDataBean.DataBean> data) {
        super(R.layout.item_analysis_index, data);
    }

    boolean flag = false;

    public IndexAdapter(boolean flag, @Nullable List<PatientDataBean.DataBean> data) {
        super(R.layout.item_single_text, data);
        this.flag = flag;
    }

    @Override
    protected void convert(BaseViewHolder helper, PatientDataBean.DataBean item) {

        if (flag) {
            helper.setText(R.id.tv, item.getDdValue());
            if (item.isChecked()) {
                helper.setBackgroundColor(R.id.tv, Color.parseColor("#e3eaef"));
                helper.setTextColor(R.id.tv, Color.parseColor("#0063BE"));
            }else{
                helper.setBackgroundColor(R.id.tv, Color.parseColor("#ffffff"));
                helper.setTextColor(R.id.tv, Color.parseColor("#000000"));
            }
        } else {
            helper.setText(R.id.tv, item.getDdValue());
        }
    }
}
