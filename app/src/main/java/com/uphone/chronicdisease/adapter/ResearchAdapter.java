package com.uphone.chronicdisease.adapter;

import android.app.Activity;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.MessageBean;
import com.uphone.chronicdisease.bean.researchStrpBean;

/**
 * @ClassName: ResearchAdapter
 * @Description: 添加患者信息阶段列表
 * @Date: 2019/12/22
 * @Author: zx
 */
public class ResearchAdapter extends BaseQuickAdapter<researchStrpBean, BaseViewHolder> {
    private Activity mActivity;
    private TextView tv_type;
    public ResearchAdapter(Activity activity) {
        super(R.layout.item_research);
        mActivity = activity;

    }


    @Override
    protected void convert(BaseViewHolder helper, researchStrpBean item) {

        helper.setText(R.id.tv_title,item.getStepName());
        helper.setText(R.id.tv_time,item.getStepEndTime());
        helper.setText(R.id.tv_stepRemark,"原因："+item.getStepRemark());
        tv_type = helper.getView(R.id.tv_type);
       /* if(){
            tv_type.setText("已完结");
            tv_type.setTextColor(getResources().getColor(R.color.colorPrimary));
        }*/

      /*  helper.setText(R.id.tv_research_group,"研究组："+item.getResearch());
        helper.setText(R.id.tv_time,item.getSendTime());
        helper.setText(R.id.tv_title,item.getToDoc()+"");
        helper.setText(R.id.tv_uptime,"上传时间："+item.getUpTime());
        helper.setText(R.id.tv_reason,"无效原因："+item.getReason());*/



    }
}