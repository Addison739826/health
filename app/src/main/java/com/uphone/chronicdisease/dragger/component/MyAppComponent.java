package com.uphone.chronicdisease.dragger.component;

import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.dragger.ContextLife;
import com.uphone.chronicdisease.dragger.module.MyAppModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Administrator
 */
@Singleton
@Component(modules = MyAppModule.class)
public interface MyAppComponent {

    /**
     * 提供App的Context
     *
     * @return
     */
    @ContextLife("Application")
    MyApp getContext();

}
