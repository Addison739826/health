package com.uphone.chronicdisease.dragger.component;

import android.app.Activity;

import com.uphone.chronicdisease.dragger.FragmentScope;
import com.uphone.chronicdisease.dragger.module.FragmentModule;
import com.uphone.chronicdisease.pro.fragment.home.HomeFragment;
import com.uphone.chronicdisease.pro.fragment.message.MessageFragment;
import com.uphone.chronicdisease.pro.fragment.personal.PersonalFragment;

import dagger.Component;

/**
 * @author Administrator
 */
@FragmentScope
@Component(dependencies = MyAppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {

    Activity getActivity();

    void inject(HomeFragment fragment);

    void inject(MessageFragment fragment);

    void inject(PersonalFragment fragment);
}
