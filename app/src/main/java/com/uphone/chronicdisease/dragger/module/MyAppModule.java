package com.uphone.chronicdisease.dragger.module;



import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.dragger.ContextLife;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Administrator
 */
@Module
public class MyAppModule {
    private final MyApp application;

    public MyAppModule(MyApp application) {
        this.application = application;
    }

    @Provides
    @Singleton
    @ContextLife("Application")
    MyApp provideApplicationContext() {
        return application;
    }

}
