package com.uphone.chronicdisease.dragger.component;

import android.app.Activity;

import com.uphone.chronicdisease.dragger.ActivityScope;
import com.uphone.chronicdisease.dragger.module.ActivityModule;
import com.uphone.chronicdisease.pro.activity.login.LoginActivity;

import dagger.Component;

/**
 * @author Administrator
 */
@ActivityScope
@Component(dependencies = MyAppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    Activity getActivity();

    void inject(LoginActivity activity);
}
