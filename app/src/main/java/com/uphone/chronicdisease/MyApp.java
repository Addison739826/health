package com.uphone.chronicdisease;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.blankj.utilcode.util.SPUtils;
import com.radish.baselibrary.base.BaseApplication;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.uphone.chronicdisease.dragger.component.DaggerMyAppComponent;
import com.uphone.chronicdisease.dragger.component.MyAppComponent;
import com.uphone.chronicdisease.dragger.module.MyAppModule;
import com.uphone.chronicdisease.http.HttpManager;

/**
 * author : SuMeng
 * e-mail : 986335838@qq.com
 * time   : 2019/05/24
 * desc   : xxxx 描述
 */
public class MyApp extends BaseApplication {
    private HttpManager mHttpManager = null;
    public static Context mContext;
    private String token;
    @Override
    protected void init() {
        //初始化网络
        token = SPUtils.getInstance().getString("token");
        mContext = getApplicationContext();
        mHttpManager = new HttpManager();
        //判断是否显示登录
        if(TextUtils.isEmpty(token)){
            Intent intent = new Intent();
            intent.setAction("android.intent.action.MAIN");
            intent.setComponent(new ComponentName("com.uphone.chronicdisease","com.uphone.chronicdisease.pro.activity.login.LoginActivity"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity(intent);
        }else {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.MAIN");
            intent.setComponent(new ComponentName("com.uphone.chronicdisease","com.uphone.chronicdisease.MainActivity"));
            startActivity(intent);
        }

       // initBugly();
    }

    public static Context getContext() {
        return mContext;
    }

    private void initBugly() {
        Bugly.init(getApplicationContext(), "a7c1082f9f", true);
        Beta.autoDownloadOnWifi = true;
        Beta.autoInit = true;
        Beta.autoCheckUpgrade = true;
        Beta.enableHotfix = true;
    }
    /**
     * 关联apiService
     *
     * @param clz
     * @param <T>
     * @return
     */
    public static <T> T apiService(Class<T> clz) {
        return (getInstance()).mHttpManager.getService(clz);
    }

    public static synchronized MyApp getInstance() {
        return (MyApp) mInstance;
    }


    public static MyAppComponent getAppComponent() {
        return DaggerMyAppComponent.builder()
                .myAppModule(new MyAppModule(getInstance()))
                .build();
    }


}
