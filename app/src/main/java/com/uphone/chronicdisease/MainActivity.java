package com.uphone.chronicdisease;

import android.Manifest;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.uber.autodispose.AutoDispose;
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider;
import com.uphone.chronicdisease.base.BaseGActivity;
import com.uphone.chronicdisease.base.OnPermissionCallBack;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.bean.CdkProjectBean;
import com.uphone.chronicdisease.bean.IsUnreadBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.RequestBodyUtil;
import com.uphone.chronicdisease.http.RxSchedulers;
import com.uphone.chronicdisease.http.simple.RxSimpleTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.pro.fragment.FirstFragment;
import com.uphone.chronicdisease.pro.fragment.home.HomeFragment;
import com.uphone.chronicdisease.pro.fragment.message.MessageFragment;
import com.uphone.chronicdisease.pro.fragment.personal.PersonalFragment;
import com.uphone.chronicdisease.view.NoScrollViewPager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

public class MainActivity extends BaseGActivity {
    @BindView(R.id.vp_main)
    NoScrollViewPager mVpMain;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    private ViewPagerAdapter mViewPagerAdapter;
    List<Fragment> fragments = new ArrayList<>();
    private String token;
    private String userId;
    @Override
    protected void initBundle() {

    }

    @Override
    protected int initLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        token  =  SPUtils.getInstance().getString("token");
        userId =   SPUtils.getInstance().getString("userId");
        mVpMain.setNoScroll(true);

        fragments.add(FirstFragment.newInstance("首页"));
        fragments.add(MessageFragment.newInstance("消息"));
        fragments.add(PersonalFragment.newInstance("个人中心"));

        //除去自带效果
        bottomNavigation.setItemIconTintList(null);


        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragments);
        mVpMain.setAdapter(mViewPagerAdapter);
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigation.getChildAt(0);
        View tab = menuView.getChildAt(1);
        BottomNavigationItemView itemView = (BottomNavigationItemView) tab;
        View badge = LayoutInflater.from(this).inflate(R.layout.layout_red_point, menuView, false);
        itemView.addView(badge);
        tvRedPoint = badge.findViewById(R.id.msg);

        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("userId", userId);
        MyApp.apiService(ApiService.class)
                .isUnRead(setParams(map))
                .compose(new RxSimpleTransformer(new Object()))
                .subscribe(new SimpleObserver<IsUnreadBean>(mActivity){
                    @Override
                    public void onNext(IsUnreadBean data) {
                        Log.e("addison","ssssssssssssssssss");
                        super.onNext(data);
                        //todo 接口未返回值，根据状态控制显隐
                        if("false".equals(data.getIsRead())){
                            tvRedPoint.setVisibility(View.GONE);
                        }else {
                            tvRedPoint.setVisibility(View.VISIBLE);
                        }

                    }
                });
    }

    private TextView tvRedPoint;

    @Override
    protected void initTitle() {

    }

    @Override
    protected void initData() {

       /* getPermission(new OnPermissionCallBack() {
            @Override
            public void permissionPass(String[] permissions) {
                File file = new File("/storage/emulated/0/DCIM/Screenshots/Screenshot_2019-05-24-18-18-57-594_com.uphone.kingmall.png");
                Map<String, RequestBody> mapRB = new HashMap<>();
                mapRB.put("token", RequestBodyUtil.convertToRequestBody("d7f55d40ff168e4158b4833f37d51ae6"));
                mapRB.put("uid", RequestBodyUtil.convertToRequestBody("8"));
                MultipartBody.Part part = RequestBodyUtil.filesToMultipartBodyPart("file", file);
                MyApp.apiService(ApiService.class)
                        .userHightScoreUpload(mapRB, part)
                        .compose(RxSchedulers.io_main())
                        .doOnSubscribe(d -> showLoading())
                        .doFinally(() -> closeLoading())
                        .as(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from
                                (MainActivity.this)))
                        .subscribe(bean -> {
                            LogUtils.e("bean:" + bean.string());
                        }, throwable -> {
                            // 异常
                            LogUtils.e("throwable:" + throwable.toString());
                            onFail();
                        }, () -> {
                            // 完成
                        });
            }

            @Override
            public void permissionRefuse(String[] permissions) {

            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);*/
    }

    @Override
    protected void initListener() {
        bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_home:
                    naviTab(0);
                    break;
                case R.id.action_project:
                    naviTab(1);
                    break;
                case R.id.action_system:
                    naviTab(2);
                    break;
                default:
                    break;
            }
            return true;
        });

    }

    /**
     * 点击Navigation切换Tab
     *
     * @param position tab下标
     */
    private void naviTab(int position) {
        mVpMain.setCurrentItem(position, true);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragments;
        private List<String> listTitle = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm, List<Fragment> mFragments) {
            super(fm);
            this.mFragments = mFragments;
        }

        public ViewPagerAdapter(FragmentManager fm, List<Fragment> mFragments, List<String> listTitle) {
            super(fm);
            this.mFragments = mFragments;
            this.listTitle = listTitle;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return listTitle.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        public void recreateItems(List<Fragment> fragmentList, List<String> titleList) {
            this.mFragments = fragmentList;
            this.listTitle = titleList;
            notifyDataSetChanged();
        }
    }
}
