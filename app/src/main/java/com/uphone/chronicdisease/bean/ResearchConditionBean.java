package com.uphone.chronicdisease.bean;

import java.util.List;

public class ResearchConditionBean {
    private List<Res> res;
    private List<Step> step;
    private List<Dia> dia;

    public List<Res> getRes() {
        return res;
    }

    public void setRes(List<Res> res) {
        this.res = res;
    }

    public List<Step> getStep() {
        return step;
    }

    public void setStep(List<Step> step) {
        this.step = step;
    }

    public List<Dia> getDia() {
        return dia;
    }

    public void setDia(List<Dia> dia) {
        this.dia = dia;
    }

    public class Dia {
        private String diagnosis;

        public String getDiagnosis() {
            return diagnosis;
        }

        public void setDiagnosis(String diagnosis) {
            this.diagnosis = diagnosis;
        }
    }
    public class Step {
        private int stepId;
        private String stepName;
        private String stepRemark;
        private String stepStartTime;
        private String stepEndTime;
        private int tempId;
        private int resId;
        private String lable;
        private int mark1;
        private int mark2;
        private String tempName;

        public int getStepId() {
            return stepId;
        }

        public void setStepId(int stepId) {
            this.stepId = stepId;
        }

        public String getStepName() {
            return stepName;
        }

        public void setStepName(String stepName) {
            this.stepName = stepName;
        }

        public String getStepRemark() {
            return stepRemark;
        }

        public void setStepRemark(String stepRemark) {
            this.stepRemark = stepRemark;
        }

        public String getStepStartTime() {
            return stepStartTime;
        }

        public void setStepStartTime(String stepStartTime) {
            this.stepStartTime = stepStartTime;
        }

        public String getStepEndTime() {
            return stepEndTime;
        }

        public void setStepEndTime(String stepEndTime) {
            this.stepEndTime = stepEndTime;
        }

        public int getTempId() {
            return tempId;
        }

        public void setTempId(int tempId) {
            this.tempId = tempId;
        }

        public int getResId() {
            return resId;
        }

        public void setResId(int resId) {
            this.resId = resId;
        }

        public String getLable() {
            return lable;
        }

        public void setLable(String lable) {
            this.lable = lable;
        }

        public int getMark1() {
            return mark1;
        }

        public void setMark1(int mark1) {
            this.mark1 = mark1;
        }

        public int getMark2() {
            return mark2;
        }

        public void setMark2(int mark2) {
            this.mark2 = mark2;
        }

        public String getTempName() {
            return tempName;
        }

        public void setTempName(String tempName) {
            this.tempName = tempName;
        }
    }

    public class Res {
        private int resId;
        private String resName;
        private int projId;

        public int getResId() {
            return resId;
        }

        public void setResId(int resId) {
            this.resId = resId;
        }

        public String getResName() {
            return resName;
        }

        public void setResName(String resName) {
            this.resName = resName;
        }

        public int getProjId() {
            return projId;
        }

        public void setProjId(int projId) {
            this.projId = projId;
        }
    }
}
