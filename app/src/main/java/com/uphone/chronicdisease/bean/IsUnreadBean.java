package com.uphone.chronicdisease.bean;

/**
 * @ClassName: IsUnreadBean
 * @Description: java类作用描述
 * @Date: 2020/1/11
 * @Author: zx
 */
public class IsUnreadBean {
    private String isRead;

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }
}
