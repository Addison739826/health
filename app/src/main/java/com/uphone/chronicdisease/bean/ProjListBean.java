package com.uphone.chronicdisease.bean;

/**
 * @ClassName: ProjListBean
 * @Description: 首页数据
 * @Date: 2020/1/8
 * @Author: zx
 */
public class ProjListBean {
    private int id;
    private String projName;
    private String projAgreement;
    private String projIcon;
    private String projIntroduce;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProjName() {
        return projName;
    }

    public void setProjName(String projName) {
        this.projName = projName;
    }

    public String getProjAgreement() {
        return projAgreement;
    }

    public void setProjAgreement(String projAgreement) {
        this.projAgreement = projAgreement;
    }

    public String getProjIcon() {
        return projIcon;
    }

    public void setProjIcon(String projIcon) {
        this.projIcon = projIcon;
    }

    public String getProjIntroduce() {
        return projIntroduce;
    }

    public void setProjIntroduce(String projIntroduce) {
        this.projIntroduce = projIntroduce;
    }
}
