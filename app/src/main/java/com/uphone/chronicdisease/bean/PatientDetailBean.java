package com.uphone.chronicdisease.bean;

import java.util.List;

/**
 * @ClassName: PatientDetailBean
 * @Description: 患者详情
 *
 * @Date: 2020/1/8
 * @Author: zx
 */
public class PatientDetailBean {
    private Dw dw;
    private Patient patient;
    private List<Step> step;
    private Tp tp;
    private Research research;

    public Dw getDw() {
        return dw;
    }

    public void setDw(Dw dw) {
        this.dw = dw;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<Step> getStep() {
        return step;
    }

    public void setStep(List<Step> step) {
        this.step = step;
    }

    public Tp getTp() {
        return tp;
    }

    public void setTp(Tp tp) {
        this.tp = tp;
    }

    public Research getResearch() {
        return research;
    }

    public void setResearch(Research research) {
        this.research = research;
    }

    public class Dw {
        private int dwId;
        private String title;
        private int projId;

        public int getDwId() {
            return dwId;
        }

        public void setDwId(int dwId) {
            this.dwId = dwId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getProjId() {
            return projId;
        }

        public void setProjId(int projId) {
            this.projId = projId;
        }
    }

    public class Patient {
        private int id;
        private String patName;
        private int sex;
        private int age;
        private int tall;
        private int projId;
        private int resId;
        private String diagnosis;
        private int docId;
        private int tpId;
        private int dwId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPatName() {
            return patName;
        }

        public void setPatName(String patName) {
            this.patName = patName;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public int getTall() {
            return tall;
        }

        public void setTall(int tall) {
            this.tall = tall;
        }

        public int getProjId() {
            return projId;
        }

        public void setProjId(int projId) {
            this.projId = projId;
        }

        public int getResId() {
            return resId;
        }

        public void setResId(int resId) {
            this.resId = resId;
        }

        public String getDiagnosis() {
            return diagnosis;
        }

        public void setDiagnosis(String diagnosis) {
            this.diagnosis = diagnosis;
        }

        public int getDocId() {
            return docId;
        }

        public void setDocId(int docId) {
            this.docId = docId;
        }

        public int getTpId() {
            return tpId;
        }

        public void setTpId(int tpId) {
            this.tpId = tpId;
        }

        public int getDwId() {
            return dwId;
        }

        public void setDwId(int dwId) {
            this.dwId = dwId;
        }
    }

    public class Step {
        private String upTime;
        private String img;
        private int state;
        private String title;
        private int dataId;
        private int tempId;

        public int getTempId() {
            return tempId;
        }

        public void setTempId(int tempId) {
            this.tempId = tempId;
        }

        public int getDataId() {
            return dataId;
        }

        public void setDataId(int dataId) {
            this.dataId = dataId;
        }

        public String getUpTime() {
            return upTime;
        }

        public void setUpTime(String upTime) {
            this.upTime = upTime;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }


    public class Tp {
        private int tpId;
        private String title;
        private String unit;
        private int projId;

        public int getTpId() {
            return tpId;
        }

        public void setTpId(int tpId) {
            this.tpId = tpId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public int getProjId() {
            return projId;
        }

        public void setProjId(int projId) {
            this.projId = projId;
        }
    }

    public class Research {
        private int resId;
        private String resName;
        private int projId;

        public int getResId() {
            return resId;
        }

        public void setResId(int resId) {
            this.resId = resId;
        }

        public String getResName() {
            return resName;
        }

        public void setResName(String resName) {
            this.resName = resName;
        }

        public int getProjId() {
            return projId;
        }

        public void setProjId(int projId) {
            this.projId = projId;
        }
    }
}
