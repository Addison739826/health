package com.uphone.chronicdisease.bean;

/**
 * @ClassName: ResearchDataDetailBean
 * @Description: java类作用描述
 * @Date: 2020/1/12
 * @Author: zx
 */
public class ResearchDataDetailBean {

    private int dataId;
    private int miEat;
    private int miSpirit;
    private int miWeak;
    private String oiImg;
    private int aiWeight;
    private int aiGrip;
    private int aiSkinfoldThicknessOfTricepsBrachii;
    private int aiBodyFatPercentage;
    private int aiSga;
    private int aiDPcare;
    private int patientId;
    private int resStepId;
    private String dataImg1;
    private String dataImg2;
    private String dataImg3;
    private String dataImg4;
    private String dataImg5;
    private String dataImg6;
    private String dataImg7;
    private String dataImg8;
    private String dataImg9;
    private String dataImg1Time;
    private String dataImg2Time;
    private String dataImg3Time;
    private String dataImg4Time;
    private String dataImg5Time;
    private String dataImg6Time;
    private String dataImg7Time;
    private String dataImg8Time;
    private String dataImg9Time;
    private String updateTime;
    private int docId;
    private int state;
    private String stepName;
    private String startTime;
    private String endTime;
    private String reason;
    private String updateDate;

    public int getDataId() {
        return dataId;
    }

    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    public int getMiEat() {
        return miEat;
    }

    public void setMiEat(int miEat) {
        this.miEat = miEat;
    }

    public int getMiSpirit() {
        return miSpirit;
    }

    public void setMiSpirit(int miSpirit) {
        this.miSpirit = miSpirit;
    }

    public int getMiWeak() {
        return miWeak;
    }

    public void setMiWeak(int miWeak) {
        this.miWeak = miWeak;
    }

    public String getOiImg() {
        return oiImg;
    }

    public void setOiImg(String oiImg) {
        this.oiImg = oiImg;
    }

    public int getAiWeight() {
        return aiWeight;
    }

    public void setAiWeight(int aiWeight) {
        this.aiWeight = aiWeight;
    }

    public int getAiGrip() {
        return aiGrip;
    }

    public void setAiGrip(int aiGrip) {
        this.aiGrip = aiGrip;
    }

    public int getAiSkinfoldThicknessOfTricepsBrachii() {
        return aiSkinfoldThicknessOfTricepsBrachii;
    }

    public void setAiSkinfoldThicknessOfTricepsBrachii(int aiSkinfoldThicknessOfTricepsBrachii) {
        this.aiSkinfoldThicknessOfTricepsBrachii = aiSkinfoldThicknessOfTricepsBrachii;
    }

    public int getAiBodyFatPercentage() {
        return aiBodyFatPercentage;
    }

    public void setAiBodyFatPercentage(int aiBodyFatPercentage) {
        this.aiBodyFatPercentage = aiBodyFatPercentage;
    }

    public int getAiSga() {
        return aiSga;
    }

    public void setAiSga(int aiSga) {
        this.aiSga = aiSga;
    }

    public int getAiDPcare() {
        return aiDPcare;
    }

    public void setAiDPcare(int aiDPcare) {
        this.aiDPcare = aiDPcare;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getResStepId() {
        return resStepId;
    }

    public void setResStepId(int resStepId) {
        this.resStepId = resStepId;
    }

    public String getDataImg1() {
        return dataImg1;
    }

    public void setDataImg1(String dataImg1) {
        this.dataImg1 = dataImg1;
    }

    public String getDataImg2() {
        return dataImg2;
    }

    public void setDataImg2(String dataImg2) {
        this.dataImg2 = dataImg2;
    }

    public String getDataImg3() {
        return dataImg3;
    }

    public void setDataImg3(String dataImg3) {
        this.dataImg3 = dataImg3;
    }

    public String getDataImg4() {
        return dataImg4;
    }

    public void setDataImg4(String dataImg4) {
        this.dataImg4 = dataImg4;
    }

    public String getDataImg5() {
        return dataImg5;
    }

    public void setDataImg5(String dataImg5) {
        this.dataImg5 = dataImg5;
    }

    public String getDataImg6() {
        return dataImg6;
    }

    public void setDataImg6(String dataImg6) {
        this.dataImg6 = dataImg6;
    }

    public String getDataImg7() {
        return dataImg7;
    }

    public void setDataImg7(String dataImg7) {
        this.dataImg7 = dataImg7;
    }

    public String getDataImg8() {
        return dataImg8;
    }

    public void setDataImg8(String dataImg8) {
        this.dataImg8 = dataImg8;
    }

    public String getDataImg9() {
        return dataImg9;
    }

    public void setDataImg9(String dataImg9) {
        this.dataImg9 = dataImg9;
    }

    public String getDataImg1Time() {
        return dataImg1Time;
    }

    public void setDataImg1Time(String dataImg1Time) {
        this.dataImg1Time = dataImg1Time;
    }

    public String getDataImg2Time() {
        return dataImg2Time;
    }

    public void setDataImg2Time(String dataImg2Time) {
        this.dataImg2Time = dataImg2Time;
    }

    public String getDataImg3Time() {
        return dataImg3Time;
    }

    public void setDataImg3Time(String dataImg3Time) {
        this.dataImg3Time = dataImg3Time;
    }

    public String getDataImg4Time() {
        return dataImg4Time;
    }

    public void setDataImg4Time(String dataImg4Time) {
        this.dataImg4Time = dataImg4Time;
    }

    public String getDataImg5Time() {
        return dataImg5Time;
    }

    public void setDataImg5Time(String dataImg5Time) {
        this.dataImg5Time = dataImg5Time;
    }

    public String getDataImg6Time() {
        return dataImg6Time;
    }

    public void setDataImg6Time(String dataImg6Time) {
        this.dataImg6Time = dataImg6Time;
    }

    public String getDataImg7Time() {
        return dataImg7Time;
    }

    public void setDataImg7Time(String dataImg7Time) {
        this.dataImg7Time = dataImg7Time;
    }

    public String getDataImg8Time() {
        return dataImg8Time;
    }

    public void setDataImg8Time(String dataImg8Time) {
        this.dataImg8Time = dataImg8Time;
    }

    public String getDataImg9Time() {
        return dataImg9Time;
    }

    public void setDataImg9Time(String dataImg9Time) {
        this.dataImg9Time = dataImg9Time;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
