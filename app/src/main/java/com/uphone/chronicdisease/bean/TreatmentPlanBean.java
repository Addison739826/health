package com.uphone.chronicdisease.bean;

/**
 * @ClassName: TreatmentPlanBean
 * @Description: 治疗方案列表Bean
 * @Date: 2019/12/31
 * @Author: zx
 */
public class TreatmentPlanBean {
    private int tpId;
    private String title;
    private String unit;
    private int projId;

    public int getTpId() {
        return tpId;
    }

    public void setTpId(int tpId) {
        this.tpId = tpId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getProjId() {
        return projId;
    }

    public void setProjId(int projId) {
        this.projId = projId;
    }
}
