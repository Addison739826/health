package com.uphone.chronicdisease.bean;

/**
 * @ClassName: UnapproveBean
 * @Description: java类作用描述
 * @Date: 2020/1/8
 * @Author: zx
 */
public class UnapproveBean {
    private String pat_name;

    public UnapproveBean(String pat_name) {
        this.pat_name = pat_name;
    }

    public String getPat_name() {
        return pat_name;
    }

    public void setPat_name(String pat_name) {
        this.pat_name = pat_name;
    }
}
