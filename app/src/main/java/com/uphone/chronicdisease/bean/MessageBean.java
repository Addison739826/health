package com.uphone.chronicdisease.bean;

/**
 * @ClassName: MessageBean
 * @Description: 消息列表
 * @Date: 2020/1/4
 * @Author: zx
 */
public class MessageBean {
    private String title;
    private int id;
    private String research;
    private String upTime;
    private String reason;
    private int toDoc;
    private int isRead;
    private int toId;
    private String sendTime;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResearch() {
        return research;
    }

    public void setResearch(String research) {
        this.research = research;
    }

    public String getUpTime() {
        return upTime;
    }

    public void setUpTime(String upTime) {
        this.upTime = upTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getToDoc() {
        return toDoc;
    }

    public void setToDoc(int toDoc) {
        this.toDoc = toDoc;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }
}
