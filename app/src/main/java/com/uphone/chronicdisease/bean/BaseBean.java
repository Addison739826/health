package com.uphone.chronicdisease.bean;

/**
 * @ClassName: BaseBean
 * @Description: java类作用描述
 * @Date: 2020/1/4
 * @Author: zx
 */
public class BaseBean<T> {
    private int code;
    private T data;
    private String msg;
    T path;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return null == data ? path : data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
