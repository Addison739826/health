package com.uphone.chronicdisease.bean;

/**
 * @Author: lzy
 * @CreateDate: 2019/12/27
 * @Description:
 */
public class CheckBean {

    private String text;
    private int id;
    private boolean checked;

    public CheckBean(String text,int id) {
        this.text = text;
        this.id = id;
    }
    public CheckBean(String text) {
        this.text = text;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CheckBean(String text, boolean checked) {
        this.text = text;
        this.checked = checked;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
