package com.uphone.chronicdisease.bean;

/**
 * @ClassName: PersonalBean
 * @Description: 个人中心表格bean
 * @Date: 2019/12/24
 * @Author: zx
 */
public class PersonalBean {

    private String subject;

    public PersonalBean(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
