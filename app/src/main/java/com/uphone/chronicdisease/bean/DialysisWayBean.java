package com.uphone.chronicdisease.bean;

/**
 * @ClassName: DialysisWayBean
 * @Description: 透析方式列表bean
 * @Date: 2019/12/31
 * @Author: zx
 */
public class DialysisWayBean {
    private int dwId;
    private String title;
    private int projId;

    public int getDwId() {
        return dwId;
    }

    public void setDwId(int dwId) {
        this.dwId = dwId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getProjId() {
        return projId;
    }

    public void setProjId(int projId) {
        this.projId = projId;
    }
}
