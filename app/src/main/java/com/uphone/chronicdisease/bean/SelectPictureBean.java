package com.uphone.chronicdisease.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

public class SelectPictureBean implements MultiItemEntity {

    public static final int TYPE_ADD = 1;
//    public static final int TYPE_ADD_NURSING = 11;
    public static final int TYPE_PICTURE = 2;
    private int itemType;

    private String picture;

    public SelectPictureBean(int itemType, String picture){
        this.itemType = itemType;
        this.picture = picture;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public int getItemType() {
        return itemType;
    }
}
