package com.uphone.chronicdisease.bean;

/**
 * @ClassName: researchStrpBean
 * @Description: java类作用描述
 * @Date: 2020/1/7
 * @Author: zx
 */
public class researchStrpBean {
    private int stepId;
    private String stepName;
    private String stepRemark;
    private String stepStartTime;
    private String stepEndTime;
    private int  tempId;
    private String resId;
    private String lable;
    private String mark1;
    private String mark2;



    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public String getStepRemark() {
        return stepRemark;
    }

    public void setStepRemark(String stepRemark) {
        this.stepRemark = stepRemark;
    }

    public String getStepStartTime() {
        return stepStartTime;
    }

    public void setStepStartTime(String stepStartTime) {
        this.stepStartTime = stepStartTime;
    }

    public String getStepEndTime() {
        return stepEndTime;
    }

    public void setStepEndTime(String stepEndTime) {
        this.stepEndTime = stepEndTime;
    }


    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public String getMark1() {
        return mark1;
    }

    public void setMark1(String mark1) {
        this.mark1 = mark1;
    }

    public String getMark2() {
        return mark2;
    }

    public void setMark2(String mark2) {
        this.mark2 = mark2;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public int getTempId() {
        return tempId;
    }

    public void setTempId(int tempId) {
        this.tempId = tempId;
    }
}
