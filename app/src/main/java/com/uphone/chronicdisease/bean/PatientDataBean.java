package com.uphone.chronicdisease.bean;

import java.util.List;

/**
 * @ClassName: PatientDataBean
 * @Description: 患者数据分析 曲线图界面
 * @Date: 2020/1/8
 * @Author: zx
 */
public class PatientDataBean {

    /**
     * data : [[{"ddId":1,"ddType":1,"ddValue":"11/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"ALB"},{"ddId":2,"ddType":2,"ddValue":"12/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"CPR"},{"ddId":3,"ddType":3,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"TCHO"},{"ddId":4,"ddType":4,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"TG"},{"ddId":5,"ddType":5,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"LDL-C"},{"ddId":6,"ddType":6,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"HDL-C"},{"ddId":7,"ddType":7,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"SCR"},{"ddId":8,"ddType":8,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"BUN"},{"ddId":9,"ddType":9,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":1,"name":"P"}],[{"ddId":10,"ddType":1,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":2,"name":"ALB"},{"ddId":11,"ddType":2,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":2,"name":"CPR"},{"ddId":12,"ddType":3,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":2,"name":"TCHO"},{"ddId":13,"ddType":4,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":2,"name":"TG"},{"ddId":14,"ddType":5,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":2,"name":"LDL-C"},{"ddId":15,"ddType":6,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":2,"name":"HDL-C"},{"ddId":16,"ddType":7,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":2,"name":"SCR"},{"ddId":17,"ddType":8,"ddValue":"1/g","ddReferenceValue":"1/g","dataDetailId":2,"name":"BUN"}],[]]
     * patient : {"id":1,"patName":"李佳琪","sex":1,"age":10,"tall":150,"projId":11,"resId":1,"diagnosis":"慢性病","docId":1,"tpId":1,"dwId":2}
     * step : ["T0首诊","T3复诊","T6复诊"]
     * research : 开同组
     */

    private PatientBean patient;
    private String research;
    private List<List<DataBean>> data;
    private List<String> step;

    public PatientBean getPatient() {
        return patient;
    }

    public void setPatient(PatientBean patient) {
        this.patient = patient;
    }

    public String getResearch() {
        return research;
    }

    public void setResearch(String research) {
        this.research = research;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public List<String> getStep() {
        return step;
    }

    public void setStep(List<String> step) {
        this.step = step;
    }

    public static class PatientBean {
        /**
         * id : 1
         * patName : 李佳琪
         * sex : 1
         * age : 10
         * tall : 150
         * projId : 11
         * resId : 1
         * diagnosis : 慢性病
         * docId : 1
         * tpId : 1
         * dwId : 2
         */

        private int id;
        private String patName;
        private int sex;
        private int age;
        private int tall;
        private int projId;
        private int resId;
        private String diagnosis;
        private int docId;
        private int tpId;
        private int dwId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPatName() {
            return patName;
        }

        public void setPatName(String patName) {
            this.patName = patName;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public int getTall() {
            return tall;
        }

        public void setTall(int tall) {
            this.tall = tall;
        }

        public int getProjId() {
            return projId;
        }

        public void setProjId(int projId) {
            this.projId = projId;
        }

        public int getResId() {
            return resId;
        }

        public void setResId(int resId) {
            this.resId = resId;
        }

        public String getDiagnosis() {
            return diagnosis;
        }

        public void setDiagnosis(String diagnosis) {
            this.diagnosis = diagnosis;
        }

        public int getDocId() {
            return docId;
        }

        public void setDocId(int docId) {
            this.docId = docId;
        }

        public int getTpId() {
            return tpId;
        }

        public void setTpId(int tpId) {
            this.tpId = tpId;
        }

        public int getDwId() {
            return dwId;
        }

        public void setDwId(int dwId) {
            this.dwId = dwId;
        }
    }

    public static class DataBean {
        private boolean checked=true;

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public DataBean(String ddValue) {
            this.ddValue = ddValue;
        }

        /**
         * ddId : 1
         * ddType : 1
         * ddValue : 11/g
         * ddReferenceValue : 1/g
         * dataDetailId : 1
         * name : ALB
         */

        private int ddId;
        private int ddType;
        private String ddValue;
        private String ddReferenceValue;
        private int dataDetailId;
        private String name;

        public int getDdId() {
            return ddId;
        }

        public void setDdId(int ddId) {
            this.ddId = ddId;
        }

        public int getDdType() {
            return ddType;
        }

        public void setDdType(int ddType) {
            this.ddType = ddType;
        }

        public String getDdValue() {
            return ddValue;
        }

        public void setDdValue(String ddValue) {
            this.ddValue = ddValue;
        }

        public String getDdReferenceValue() {
            return ddReferenceValue;
        }

        public void setDdReferenceValue(String ddReferenceValue) {
            this.ddReferenceValue = ddReferenceValue;
        }

        public int getDataDetailId() {
            return dataDetailId;
        }

        public void setDataDetailId(int dataDetailId) {
            this.dataDetailId = dataDetailId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}


