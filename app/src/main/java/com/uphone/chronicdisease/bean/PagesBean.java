package com.uphone.chronicdisease.bean;

import com.alibaba.fastjson.JSONObject;

import java.util.Iterator;
import java.util.List;

/**
 * @Author: lzy
 * @CreateDate: 2020/1/10
 * @Description:
 */
public class PagesBean<T> {


    private List<T> dataList;
    private int count;

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
