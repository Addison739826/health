package com.uphone.chronicdisease.bean;

/**
 * @ClassName: ResearchBean
 * @Description: 研究组列表bean
 * @Date: 2019/12/31
 * @Author: zx
 */
public class ResearchBean {
    private int resId;
    private String resName;
    private int projId;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public int getProjId() {
        return projId;
    }

    public void setProjId(int projId) {
        this.projId = projId;
    }
}
