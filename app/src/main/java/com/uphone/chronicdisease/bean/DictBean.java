package com.uphone.chronicdisease.bean;

import java.util.List;

/**
 * @Author: lzy
 * @CreateDate: 2020/1/8
 * @Description:
 */
public class DictBean {

    private List<AiBean> ai;
    private List<OiBean> oi;
    private List<MiBean> mi;

    public List<AiBean> getAi() {
        return ai;
    }

    public void setAi(List<AiBean> ai) {
        this.ai = ai;
    }

    public List<OiBean> getOi() {
        return oi;
    }

    public void setOi(List<OiBean> oi) {
        this.oi = oi;
    }

    public List<MiBean> getMi() {
        return mi;
    }

    public void setMi(List<MiBean> mi) {
        this.mi = mi;
    }

    public static class AiBean {
        /**
         * aiId : 1
         * aiType : 1
         * aiUnit : 1
         * aiMust : 1
         * aiShow : 1
         * tempId : 1
         */

        private int aiId;
        private int aiType;
        private int aiUnit;
        private int aiMust;
        private int aiShow;
        private int tempId;

        public int getAiId() {
            return aiId;
        }

        public void setAiId(int aiId) {
            this.aiId = aiId;
        }

        public int getAiType() {
            return aiType;
        }

        public void setAiType(int aiType) {
            this.aiType = aiType;
        }

        public int getAiUnit() {
            return aiUnit;
        }

        public void setAiUnit(int aiUnit) {
            this.aiUnit = aiUnit;
        }

        public int getAiMust() {
            return aiMust;
        }

        public void setAiMust(int aiMust) {
            this.aiMust = aiMust;
        }

        public int getAiShow() {
            return aiShow;
        }

        public void setAiShow(int aiShow) {
            this.aiShow = aiShow;
        }

        public int getTempId() {
            return tempId;
        }

        public void setTempId(int tempId) {
            this.tempId = tempId;
        }
    }

    public static class OiBean {
        /**
         * oiId : 1
         * oiType : 1
         * oiMust : 1
         * oiShow : 1
         * tempId : 1
         */

        private int oiId;
        private int oiType;
        private int oiMust;
        private int oiShow;
        private int tempId;

        public int getOiId() {
            return oiId;
        }

        public void setOiId(int oiId) {
            this.oiId = oiId;
        }

        public int getOiType() {
            return oiType;
        }

        public void setOiType(int oiType) {
            this.oiType = oiType;
        }

        public int getOiMust() {
            return oiMust;
        }

        public void setOiMust(int oiMust) {
            this.oiMust = oiMust;
        }

        public int getOiShow() {
            return oiShow;
        }

        public void setOiShow(int oiShow) {
            this.oiShow = oiShow;
        }

        public int getTempId() {
            return tempId;
        }

        public void setTempId(int tempId) {
            this.tempId = tempId;
        }
    }

    public static class MiBean {
        /**
         * miId : 1
         * tempId : 1
         * miType : 1
         * miNumber : 1
         * miRemark : 难以下咽
         * miMust : 1
         * miShow : 1
         */

        private int miId;
        private int tempId;
        private int miType;
        private int miNumber;
        private String miRemark;
        private int miMust;
        private int miShow;

        public int getMiId() {
            return miId;
        }

        public void setMiId(int miId) {
            this.miId = miId;
        }

        public int getTempId() {
            return tempId;
        }

        public void setTempId(int tempId) {
            this.tempId = tempId;
        }

        public int getMiType() {
            return miType;
        }

        public void setMiType(int miType) {
            this.miType = miType;
        }

        public int getMiNumber() {
            return miNumber;
        }

        public void setMiNumber(int miNumber) {
            this.miNumber = miNumber;
        }

        public String getMiRemark() {
            return miRemark;
        }

        public void setMiRemark(String miRemark) {
            this.miRemark = miRemark;
        }

        public int getMiMust() {
            return miMust;
        }

        public void setMiMust(int miMust) {
            this.miMust = miMust;
        }

        public int getMiShow() {
            return miShow;
        }

        public void setMiShow(int miShow) {
            this.miShow = miShow;
        }
    }
}
