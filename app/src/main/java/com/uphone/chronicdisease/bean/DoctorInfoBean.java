package com.uphone.chronicdisease.bean;

import java.util.List;

/**
 * @ClassName: DoctorInfoBean
 * @Description: java类作用描述
 * @Date: 2020/1/7
 * @Author: zx
 */
public class DoctorInfoBean {
    private List<ResearchList> researchList;
    private List<List<Result>> result;
    private DoctorInfo doctorInfo;

    public List<ResearchList> getResearchList() {
        return researchList;
    }

    public void setResearchList(List<ResearchList> researchList) {
        this.researchList = researchList;
    }

    public List<List<Result>> getResult() {
        return result;
    }

    public void setResult(List<List<Result>> result) {
        this.result = result;
    }


    public DoctorInfo getDoctorInfo() {
        return doctorInfo;
    }

    public void setDoctorInfo(DoctorInfo doctorInfo) {
        this.doctorInfo = doctorInfo;
    }

    public class ResearchList {
        private int resId;
        private String resName;
        private int projId;
        private int count;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getResId() {
            return resId;
        }

        public void setResId(int resId) {
            this.resId = resId;
        }

        public String getResName() {
            return resName;
        }

        public void setResName(String resName) {
            this.resName = resName;
        }

        public int getProjId() {
            return projId;
        }

        public void setProjId(int projId) {
            this.projId = projId;
        }
    }

    public class Result {

        private String name;
        private int un;
        private int finish;
        private int unfinished;
        public void setUn(int un) {
            this.un = un;
        }
        public int getUn() {
            return un;
        }

        public void setFinish(int finish) {
            this.finish = finish;
        }
        public int getFinish() {
            return finish;
        }

        public void setUnfinished(int unfinished) {
            this.unfinished = unfinished;
        }
        public int getUnfinished() {
            return unfinished;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class DoctorInfo {
        private String name;
        private String company;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }
    }
}
