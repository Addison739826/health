package com.uphone.chronicdisease.bean;

/**
 * @ClassName: resBsaseInfoBean
 * @Description: java类作用描述
 * @Date: 2020/1/18
 * @Author: zx
 */
public class resBsaseInfoBean {
    private int id;
    private int tpShow;
    private int dwShow;
    private int dgShow;
    private int projId;
    private int resId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTpShow() {
        return tpShow;
    }

    public void setTpShow(int tpShow) {
        this.tpShow = tpShow;
    }

    public int getDwShow() {
        return dwShow;
    }

    public void setDwShow(int dwShow) {
        this.dwShow = dwShow;
    }

    public int getDgShow() {
        return dgShow;
    }

    public void setDgShow(int dgShow) {
        this.dgShow = dgShow;
    }

    public int getProjId() {
        return projId;
    }

    public void setProjId(int projId) {
        this.projId = projId;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }
}
