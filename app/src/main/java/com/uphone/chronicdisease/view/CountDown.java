package com.uphone.chronicdisease.view;


import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;

import com.uphone.chronicdisease.R;


//短信验证码计时器
public class CountDown {
	/**
	 * 获取短信验证码按钮 Button:mGetPhoneCodeBtn
	 */
	private Button mGetPhoneCodeBtn;

	private int mCountTime_Task = 0;

	/**
	 * 短信间隔时间 int:ONCE_COUNTTIME
	 */
	public static final int ONCE_COUNTTIME = 60;// 短信间隔时间

	/**
	 * 注册 int:COUNTTIME_REGISTER
	 */
	public static final int COUNTTIME_REGISTER = 1;// 1-注册

	/**
	 * 修改手机号 int:COUNTTIME_REPHONE
	 */
	public static final int COUNTTIME_REPHONE = 2;// 2-修改手机号

	/**
	 * 认证支付 int:COUNTTIME_OAUTHPAY
	 */
	public static final int COUNTTIME_OAUTHPAY = 3;// 3-认证支付

	/**
	 * 快捷支付 int:COUNTTIME_QUICKPAY
	 */
	public static final int COUNTTIME_QUICKPAY = 4;// 4-快捷支付

	/**
	 * 绑定银行卡 int:COUNTTIME_ADDCARD
	 */
	public static final int COUNTTIME_ADDCARD = 5;// 5-绑定银行卡

	/**
	 * 重置密码 int:COUNTTIME_GETPW
	 */
	public static final int COUNTTIME_GETPW = 6;// 6-重置密码

	/**
	 * 修改密码 int:COUNTTIME_REPW
	 */
	public static final int COUNTTIME_REPW = 7;// 7-修改密码

	/**
	 * 开通推送服务 int:COUNTTIME_OPENPUSH
	 */
	public static final int COUNTTIME_OPENPUSH = 8;// 8-开通推送服务

	/**
	 * 注册 int:countRegisterTime
	 */
	public static int countRegisterTime = ONCE_COUNTTIME;// 1-注册

	/**
	 * 修改手机号 int:countRePhoneTime
	 */
	public static int countRePhoneTime = ONCE_COUNTTIME;// 2-修改手机号

	/**
	 * 认证支付 int:countOAuthPayTime
	 */
	public static int countOAuthPayTime = ONCE_COUNTTIME;// 3-认证支付

	/**
	 * 快捷支付 int:countQuickPayTime
	 */
	public static int countQuickPayTime = ONCE_COUNTTIME;// 4-快捷支付

	/**
	 * 绑定银行卡 int:countAddCardTime
	 */
	public static int countAddCardTime = ONCE_COUNTTIME;// 5-绑定银行卡

	/**
	 * 重置密码 int:countGetPwTime
	 */
	public static int countGetPwTime = ONCE_COUNTTIME;// 6-重置密码

	/**
	 * 修改密码 int:countRePwTime
	 */
	public static int countRePwTime = ONCE_COUNTTIME;// 7-修改密码

	/**
	 * 开通推送服务 int:countOpenPushTime
	 */
	public static int countOpenPushTime = ONCE_COUNTTIME;// 8-开通推送服务

	/**
	 * desc:
	 * <p>
	 * 创建人：wangtao3 , 2013-4-18 下午4:02:49
	 * </p>
	 * 
	 * @param aGetPhoneCodeBtn
	 * @param countTask
	 */
	public void getTime(Button aGetPhoneCodeBtn, int countTask) {
		mCountTime_Task = countTask;
		this.mGetPhoneCodeBtn = aGetPhoneCodeBtn;

		orginColor = aGetPhoneCodeBtn.getTextColors();

		aGetPhoneCodeBtn.setEnabled(false);
		new Thread(mRunnable).start();
	}

	@SuppressWarnings("unused")
	private ColorStateList orginColor;

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		@SuppressLint("ResourceAsColor")
		public void handleMessage(Message msg) {
			if (msg.arg1 == 0) {// 恢复为可获取
				mGetPhoneCodeBtn.setEnabled(true);

				mGetPhoneCodeBtn.setTextColor(Color.parseColor("#FF666666"));
				mGetPhoneCodeBtn.setText("发送验证码");

			} else {
				mGetPhoneCodeBtn.setText("("+msg.arg1 + "s)重新获取");
				mGetPhoneCodeBtn.setTextColor(Color.parseColor("#FF666666"));
				mGetPhoneCodeBtn.setTextSize(12);
			}
		}
	};

	private Runnable mRunnable = new Runnable() {
		public void run() {
			if (mCountTime_Task == COUNTTIME_REGISTER) {
				countRegisterTime = ONCE_COUNTTIME;
				for (int count = countRegisterTime; count >= 0; count--) {
					countRegisterTime = count;
					Message msg = new Message();
					if (count == 0) {
						countRegisterTime = ONCE_COUNTTIME;
						msg.arg1 = 0;
						mHandler.sendMessage(msg);
						break;
					} else {
						msg.arg1 = count;
						mHandler.sendMessage(msg);
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else if (mCountTime_Task == COUNTTIME_REPHONE) {
				countRePhoneTime = ONCE_COUNTTIME;
				for (int count = countRePhoneTime; count >= 0; count--) {
					countRePhoneTime = count;
					Message msg = new Message();
					if (count == 0) {
						countRePhoneTime = ONCE_COUNTTIME;
						msg.arg1 = 0;
						mHandler.sendMessage(msg);
						break;
					} else {
						msg.arg1 = count;
						mHandler.sendMessage(msg);
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else if (mCountTime_Task == COUNTTIME_OAUTHPAY) {
				countOAuthPayTime = ONCE_COUNTTIME;
				for (int count = countOAuthPayTime; count >= 0; count--) {
					countOAuthPayTime = count;
					Message msg = new Message();
					if (count == 0) {
						countOAuthPayTime = ONCE_COUNTTIME;
						msg.arg1 = 0;
						mHandler.sendMessage(msg);
						break;
					} else {
						msg.arg1 = count;
						mHandler.sendMessage(msg);
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else if (mCountTime_Task == COUNTTIME_QUICKPAY) {
				countQuickPayTime = ONCE_COUNTTIME;
				for (int count = countQuickPayTime; count >= 0; count--) {
					countQuickPayTime = count;
					Message msg = new Message();
					if (count == 0) {
						countQuickPayTime = ONCE_COUNTTIME;
						msg.arg1 = 0;
						mHandler.sendMessage(msg);
						break;
					} else {
						msg.arg1 = count;
						mHandler.sendMessage(msg);
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else if (mCountTime_Task == COUNTTIME_ADDCARD) {
				countAddCardTime = ONCE_COUNTTIME;
				for (int count = countAddCardTime; count >= 0; count--) {
					countAddCardTime = count;
					Message msg = new Message();
					if (count == 0) {
						countAddCardTime = ONCE_COUNTTIME;
						msg.arg1 = 0;
						mHandler.sendMessage(msg);
						break;
					} else {
						msg.arg1 = count;
						mHandler.sendMessage(msg);
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else if (mCountTime_Task == COUNTTIME_GETPW) {
				countGetPwTime = ONCE_COUNTTIME;
				for (int count = countGetPwTime; count >= 0; count--) {
					countGetPwTime = count;
					Message msg = new Message();
					if (count == 0) {
						countGetPwTime = ONCE_COUNTTIME;
						msg.arg1 = 0;
						mHandler.sendMessage(msg);
						break;
					} else {
						msg.arg1 = count;
						mHandler.sendMessage(msg);
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else if (mCountTime_Task == COUNTTIME_REPW) {
				countRePwTime = ONCE_COUNTTIME;
				for (int count = countRePwTime; count >= 0; count--) {
					countRePwTime = count;
					Message msg = new Message();
					if (count == 0) {
						countRePwTime = ONCE_COUNTTIME;
						msg.arg1 = 0;
						mHandler.sendMessage(msg);
						break;
					} else {
						msg.arg1 = count;
						mHandler.sendMessage(msg);
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else if (mCountTime_Task == COUNTTIME_OPENPUSH) {
				countOpenPushTime = ONCE_COUNTTIME;
				for (int count = countOpenPushTime; count >= 0; count--) {
					countOpenPushTime = count;
					Message msg = new Message();
					if (count == 0) {
						countOpenPushTime = ONCE_COUNTTIME;
						msg.arg1 = 0;
						mHandler.sendMessage(msg);
						break;
					} else {
						msg.arg1 = count;
						mHandler.sendMessage(msg);
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}
		}
	};
}
