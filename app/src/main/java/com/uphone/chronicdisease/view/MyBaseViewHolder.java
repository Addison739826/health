package com.uphone.chronicdisease.view;

import android.content.Context;
import android.support.annotation.IdRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.radish.framelibrary.view.glide.GlideUtils;

/**
 * <pre>
 *     author : radish
 *     e-mail : 15703379121@163.com
 *     time   : 2019/4/17
 *     desc   :
 * </pre>
 */
public class MyBaseViewHolder extends BaseViewHolder {
    public MyBaseViewHolder(View view) {
        super(view);
    }

    public MyBaseViewHolder setWidthHeight(@IdRes int viewId, int width, int height) {
        View view = getView(viewId);
        if (view != null) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            if (params != null) {
                params.width = width;
                params.height = height;
                view.setLayoutParams(params);
            }
        }
        return this;
    }

    public MyBaseViewHolder setWidth(@IdRes int viewId, int width) {
        View view = getView(viewId);
        if (view != null) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            if (params != null) {
                params.width = width;
                view.setLayoutParams(params);
            }
        }
        return this;
    }


    public MyBaseViewHolder setClick(@IdRes int viewId, View.OnClickListener listener) {
        if (listener != null) {
            View view = getView(viewId);
            if (view != null) {
                view.setOnClickListener(listener);
            }
        }
        return this;
    }

    public MyBaseViewHolder setImageView(Context context, @IdRes int viewId, Object img) {
        if (img != null) {
            ImageView view = getView(viewId);
            if (view != null) {
                GlideUtils.getInstance().loadNormalImage(context, img, view);
            }
        }
        return this;
    }

    public MyBaseViewHolder setImageView(Context context, @IdRes int viewId, int img) {
        ImageView view = getView(viewId);
        if (view != null) {
            view.setImageResource(img);
        }
        return this;
    }

    public MyBaseViewHolder setImageView(Context context, @IdRes int viewId, Object img, int defaultImg) {
        if (img != null) {
            ImageView view = getView(viewId);
            if (view != null) {
                GlideUtils.getInstance().loadNormalImage(context, img, view, defaultImg);
            }
        }
        return this;
    }

    public MyBaseViewHolder setSelect(@IdRes int viewId, boolean isSelect) {
        View view = getView(viewId);
        if (view != null) {
            view.setSelected(isSelect);
        }
        return this;
    }

    public MyBaseViewHolder setTextSize(@IdRes int viewId, int value) {
        TextView view = getView(viewId);
        view.setTextSize(value);
        return this;
    }

    public MyBaseViewHolder setImageViewCorner(Context context, @IdRes int viewId, Object img, int corner) {
        if (img != null) {
            ImageView view = getView(viewId);
            if (view != null) {
                GlideUtils.getInstance().loadCornerImage(context, img, corner, view);
            }
        }
        return this;
    }

    public MyBaseViewHolder setImageViewCircle(Context context, @IdRes int viewId, Object img) {
        if (img != null) {
            ImageView view = getView(viewId);
            if (view != null) {
                GlideUtils.getInstance().loadCircleImage(context, img, view);
            }
        }
        return this;
    }
}
