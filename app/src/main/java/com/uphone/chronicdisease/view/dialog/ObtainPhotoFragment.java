package com.uphone.chronicdisease.view.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.uphone.chronicdisease.R;


/**
 * Created by 赵亚坤 on 2017/11/28.
 */

public class ObtainPhotoFragment extends DialogFragment {
    private TextView mTvClose, mTvPhotograph, mTvSelectPhotos;
    private String mLeft;
    private String mRight;

    public void setPhotoClickListener(OnPhotoClickListener photoClickListener) {
        mListener = photoClickListener;
    }

    public OnPhotoClickListener mListener;

    public void setAboutText(String left, String right) {
        mLeft = left;
        mRight = right;
    }

    public interface OnPhotoClickListener {
        /**
         * 拍照
         */
        void onPhotograph();

        /**
         * 相册
         */
        void onAlbum();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // 使用不带Theme的构造器, 获得的dialog边框距离屏幕仍有几毫米的缝隙。
        Dialog dialog = new Dialog(getActivity(), R.style.ActionSheetDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // 设置Content前设定
        dialog.setContentView(R.layout.dialog_user_image_updata);
        dialog.setCanceledOnTouchOutside(true); // 外部点击取消
        // 设置宽度为屏宽, 靠近屏幕底部。
        final Window window = dialog.getWindow();
        window.setWindowAnimations(R.style.ProjectAnimation);
        final WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.BOTTOM; // 紧贴底部
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度持平
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        mTvClose = dialog.findViewById(R.id.tv_close);
        mTvPhotograph = dialog.findViewById(R.id.tv_photograph);
        mTvSelectPhotos = dialog.findViewById(R.id.tv_select_photos);
        if (!TextUtils.isEmpty(mLeft)) {
            mTvPhotograph.setText(mLeft);
        }
        if (!TextUtils.isEmpty(mRight)) {
            mTvSelectPhotos.setText(mRight);
        }
        mTvClose.setOnClickListener(listener);
        mTvPhotograph.setOnClickListener(listener);
        mTvSelectPhotos.setOnClickListener(listener);
        return dialog;
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_close:
                    dismiss();
                    break;
                case R.id.tv_photograph:
                    if (mListener != null)
                        mListener.onPhotograph();
                    dismiss();
                    break;
                case R.id.tv_select_photos:
                    if (mListener != null)
                        mListener.onAlbum();
                    dismiss();
                    break;
            }
        }
    };
}
