package com.uphone.chronicdisease.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.SeekBar;

import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.DictBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: lzy
 * @CreateDate: 2019/12/26
 * @Description:
 */
@SuppressLint("AppCompatCustomView")
public class MySeekBar extends SeekBar {


    private TextPaint mTextPaint;//绘制文本的大小
    private int mSeekBarMin = 0;//滑块开始值
    private List<String> list = new ArrayList<>();//要展示的文字
    /**
     * 刻度线画笔
     */
    private Paint mRulerPaint;
    /**
     * 刻度线的个数,等分数等于刻度线的个数加1
     */
    private int mRulerCount = 4;

    /**
     * 每条刻度线的宽度
     */
    private int mRulerWidth = 2;


    /**
     * 滑块上面是否要显示刻度线
     */
    private boolean isShowTopOfThumb = false;

    public MySeekBar(Context context) {
        this(context, null);
    }

    public MySeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.seekBarStyle);
    }

    public MySeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mTextPaint = new TextPaint();
        mTextPaint.setColor(getResources().getColor(R.color.black3));
        mTextPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.sp_12));
        mTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mRulerCount = getMax();
        //创建绘制刻度线的画笔
        mRulerPaint = new Paint();

        mRulerPaint.setColor(getResources().getColor(R.color.seekbar_df));
        mRulerPaint.setAntiAlias(true);

        //Api21及以上调用，去掉滑块后面的背景
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSplitTrack(false);
        }

//        list.add("1分：。");
//        list.add("2分：。。");
//        list.add("3分：。。。");
//        list.add("4分：。。。。");
//        list.add("5分：。。。。。");
//        list.add("6分：。。。。。");
//        list.add("7分：。。。。。");
//        list.add("8分：。。。。。");
//        list.add("9分：。。。。。");
//        list.add("10分：。。。。。");
    }

    public void setData(List<String> list) {
        this.list.addAll(list);
        invalidate();
    }

    private List<DictBean.MiBean> beanList = new ArrayList<>();

    public void setBeanData(List<DictBean.MiBean> mlist) {
        this.beanList.addAll(mlist);
        invalidate();
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //极限条件校验
        if (getWidth() <= 0 || mRulerCount <= 1) {
            return;
        }
        Log.e("addison", "height:" + getHeight() + "paddingTop:" + getPaddingTop());
        //获取每一份的长度
        int length = (getWidth() - getPaddingLeft() - getPaddingRight()) / (mRulerCount);

        //计算刻度线的顶部坐标和底部坐标
        int rulerTop = getHeight() / 2 - getMinimumHeight() / 2 + getPaddingTop() / 2;
        int rulerBottom = rulerTop + getMinimumHeight();


        //获取滑块的位置信息
        Rect thumbRect = null;
        if (getThumb() != null) {
            thumbRect = getThumb().getBounds();
        }

        //绘制刻度线
        for (int i = 0; i <= mRulerCount; i++) {
            //计算刻度线的左边坐标和右边坐标
            int rulerLeft = i * length + getPaddingLeft();
            int rulerRight = rulerLeft + mRulerWidth;
            //判断是否需要绘制刻度线
            if (!isShowTopOfThumb && thumbRect != null && rulerLeft - getPaddingLeft() > thumbRect.left && rulerRight - getPaddingLeft() < thumbRect.right) {
                continue;
            }
            //进行绘制
            canvas.drawRect(rulerLeft, rulerTop - 15, rulerRight, rulerBottom, mRulerPaint);
        }


        String progressText = "";
        if (0 != beanList.size()) {
            progressText = beanList.get(getProgress()).getMiRemark();
            if (null == progressText) {
                progressText = "";
            }
        }
        if (0 != list.size()) {
            progressText = list.get(getProgress());
            if (null == progressText) {
                progressText = "";
            }
        }
        Rect bounds = new Rect();
        mTextPaint.getTextBounds(progressText, 0, progressText.length(), bounds);
        int leftPadding = getPaddingLeft() - getThumbOffset();
        int rightPadding = getPaddingRight() - getThumbOffset();
        int width = getWidth() - leftPadding - rightPadding;
        float progressRatio = (float) getProgress() / getMax();
        float thumbOffset = .5f - progressRatio;
        float thumbX = progressRatio * width + leftPadding + thumbOffset;
        if (getProgress() == 0) {
            thumbX = bounds.width();
        } else if (getProgress() == getMax()) {
            thumbX = (float) (thumbX - bounds.width() * 0.5);
        }
        float thumbY = getHeight() / 2f + bounds.height() / 2f - 50;
        if (getProgress() <= list.size()) {
            canvas.drawText(progressText, thumbX, thumbY, mTextPaint);
        }
    }


}
