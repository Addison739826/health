package com.uphone.chronicdisease.view.dialog;


import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.blankj.utilcode.util.SPUtils;
import com.radish.baselibrary.utils.ToastUtil;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.http.ApiService;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.uphone.chronicdisease.http.ApiService.PicUrl;
import static com.uphone.chronicdisease.util.CommonUtil.setParams;


/**
 * @ClassName: DataReportDialog
 * @Description: 生成数据报表弹出框
 * @Date: 2019/12/23
 * @Author: zx
 */
public class DataReportDialog extends DialogFragment {
    private String ids;
    private String url;
    private EditText et_email;

    private static final int PERMISSION_REQUEST_CODE = 1000;
    //MainActivity activity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //activity = (MainActivity) getActivity();
        Bundle arguments = getArguments();
        ids = arguments.getString("ids");
        url = arguments.getString("url");
        // 使用不带Theme的构造器, 获得的dialog边框距离屏幕仍有几毫米的缝隙。
        final Dialog dialog = new Dialog(getActivity(), R.style.ActionSheetDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // 设置Content前设定
        dialog.setContentView(R.layout.dialog_data_report);

        dialog.setCanceledOnTouchOutside(true); // 外部点击取消
        // 设置宽度为屏宽, 靠近屏幕底部。
        final Window window = dialog.getWindow();
        window.setWindowAnimations(R.style.ProjectAnimation);
        final WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.VERTICAL_GRAVITY_MASK; // 紧贴底部
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度持平
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        et_email = ((EditText) dialog.findViewById(R.id.et_email));

        dialog.findViewById(R.id.dialog_textViewID).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });
        dialog.findViewById(R.id.dialog_textViewID1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(et_email.getText().toString())) {
                    ToastUtil.showShort("请填写邮箱");
                } else if (et_email.getText().toString().contains("@") && et_email.getText().toString().contains(".")) {
                    if (et_email.getText().toString().lastIndexOf(".") > et_email.getText().toString().lastIndexOf("@")) {
                        dismiss();
                        mail();
                    }
                } else {
                    ToastUtil.showShort("请填写正确的邮箱格式");
                }
            }
        });

        return dialog;
    }

    //保存数据
    private void mail() {
        Map<String, String> map = new HashMap<>();
        map.put("token", SPUtils.getInstance().getString("token"));
        map.put("pid", SPUtils.getInstance().getString("userId"));
        map.put("email", et_email.getText().toString());
        map.put("img", PicUrl + url);
        MyApp.apiService(ApiService.class)
                .mail(setParams(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(BaseBean bean) {
                        BaseBean baseBean = bean;
                        if (baseBean.getCode() == 0) {
                            ToastUtil.showShort("发送成功！");
                        } else {
                            ToastUtil.showShort("异常！");
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
