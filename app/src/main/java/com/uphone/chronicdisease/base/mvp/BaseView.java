package com.uphone.chronicdisease.base.mvp;

import com.uphone.chronicdisease.base.BaseGActivity;

/**
 * Created by hzy on 2019/1/23
 * BaseView
 *
 * @author hzy
 */
public interface BaseView {

    /**
     * 显示加载框
     */
    void showLoading();


    /**
     * 关闭加载框
     */
    void closeLoading();


    /**
     * 请求成功所做处理
     */
    void onSuccess(int code, String msg);


    /**
     * 请求失败所做处理
     */
    void onFail();


    /**
     * 网络异常
     */
    void onNetError();

    /**
     * 重新加载
     */
    void onReLoad();

    BaseGActivity getBaseActivity();
}
