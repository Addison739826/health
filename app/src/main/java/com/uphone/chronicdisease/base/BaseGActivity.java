package com.uphone.chronicdisease.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;

import com.gyf.immersionbar.ImmersionBar;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.radish.baselibrary.Intent.IntentUtils;
import com.radish.baselibrary.base.BaseActivity;
import com.radish.baselibrary.navigationbar.DefaultNavigationBar;
import com.radish.baselibrary.utils.PermissionsUtils;
import com.radish.baselibrary.utils.ToastUtil;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.mvp.BaseView;
import com.uphone.chronicdisease.util.CommonUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

/**
 * <pre>
 *     author : radish
 *     e-mail : 15703379121@163.com
 *     time   : 2019/4/16
 *     desc   :
 * </pre>
 */
public abstract class BaseGActivity extends BaseActivity implements BaseView {

    //状态栏工具
    protected ImmersionBar mImmersionBar;
    protected ProgressDialog progressDialog;
    protected boolean isUseImmersionBar = true;
    protected boolean isImmersion = true;
    protected boolean isEventBus = false;
    private OnPermissionCallBack mPermissionCallBack;

    protected KProgressHUD mKProgressHUD;
    protected Context context;
    public Activity mActivity;
    public int pageNum = 1;
    public int pageSize = 10;

    @Override
    protected void initLayoutAfter() {
        super.initLayoutAfter();
        context=this;
        mActivity=this;
        if (isEventBus) {
            EventBus.getDefault().register(this);
        }
        if (isImmersion) {
            mImmersionBar = ImmersionBar.with(this);
            //防止设置状态栏后  软键盘怎么设置也不会把布局顶上去
            mImmersionBar.keyboardEnable(true, WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
                    | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN).navigationBarWithKitkatEnable(false).init();
            if (isUseImmersionBar) {
                setImmersionBar();
            } else {
                setStatusBar();
            }
        } else {
            // 设置状态栏底色颜色
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        ButterKnife.bind(this);
        IntentUtils.init(this);
    }

    @Override
    public void onBackPressed() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            return;
        }
        super.onBackPressed();
        finish();
    }

    public void setImmersionBar() {
        mImmersionBar
                .fitsSystemWindows(false)
                .statusBarDarkFont(true, 0.2f)
                .statusBarColor(R.color.transparent)
                .navigationBarColor(R.color.black)
                .statusBarDarkFont(false)
                .fullScreen(false)

                .init();
    }

    protected DefaultNavigationBar initTitle(String title) {
        return new DefaultNavigationBar.Builder(this, llRoot).setTitle(CommonUtil.getStr(title)).builder();
    }

    protected DefaultNavigationBar initTitleWidthBottom(String title) {
        return new DefaultNavigationBar.Builder(this, llRoot)
                .setLineColor(getResources().getColor(R.color.color_graye))
                .setTitle(CommonUtil.getStr(title)).builder();
    }

    protected DefaultNavigationBar initTitle(String title, String rightText, View.OnClickListener listener) {
        return new DefaultNavigationBar.Builder(this, llRoot).setTitle(CommonUtil.getStr(title))
                .setRightTitle(rightText)
                .setRightClick(listener)
                .builder();
    }

    /**
     * 设置状态栏
     */
    public void setStatusBar() {
        mImmersionBar
                .fitsSystemWindows(true)
                .statusBarDarkFont(true, 0.2f)
                .statusBarColor(R.color.bg)
                .navigationBarColor(R.color.black)
                .statusBarDarkFont(true)
                .fullScreen(false)
                .init();
    }


    protected void getPermission(final OnPermissionCallBack callBack, final String... permissions) {
        this.mPermissionCallBack = callBack;
        PermissionsUtils.getInstance().checkPermissions(this, new PermissionsUtils.IPermissionsResult() {
            @Override
            public void passPermissions() {
                if (callBack != null) {
                    callBack.permissionPass(permissions);
                }
            }

            @Override
            public void refusePermissions() {
                if (callBack != null) {
                    callBack.permissionRefuse(permissions);
                }
            }
        }, permissions);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults != null && grantResults.length != 0) {
            boolean permission = true;
            for (int i = 0; i < permissions.length; i++) {
                if (ContextCompat.checkSelfPermission(this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    permission = false;
                    break;
                }
            }
            if (mPermissionCallBack != null) {
                if (permission) {
                    mPermissionCallBack.permissionPass(permissions);
                } else {
                    mPermissionCallBack.permissionRefuse(permissions);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        KeyboardUtils.hideSoftInput(this);

        if (isEventBus) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void showLoading() {
        if (mKProgressHUD == null || !mKProgressHUD.isShowing()) {
            mKProgressHUD = KProgressHUD.create(this);
            mKProgressHUD.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();
        }
    }

    @Override
    public void closeLoading() {
        if (mKProgressHUD != null) {
            mKProgressHUD.dismiss();
        }
    }

    @Override
    public void onSuccess(int code, String msg) {
        if (code != 0) {
            if (code == 500) {
//                CommonUtil.startLoginActivity();
            }
            if (!TextUtils.isEmpty(msg))
                ToastUtil.showShort(msg);
        }
    }

    @Override
    public void onFail() {
        ToastUtils.showShort("获取数据失败");
    }

    @Override
    public void onNetError() {
        ToastUtils.showShort("请检查网络是否连接");
    }

    @Override
    public void onReLoad() {

    }

    @Override
    public BaseGActivity getBaseActivity() {
        return this;
    }

}
