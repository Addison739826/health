package com.uphone.chronicdisease.base.mvp;

import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.base.BaseGActivity;
import com.uphone.chronicdisease.dragger.component.ActivityComponent;
import com.uphone.chronicdisease.dragger.component.DaggerActivityComponent;
import com.uphone.chronicdisease.dragger.module.ActivityModule;

import javax.inject.Inject;

import io.reactivex.annotations.Nullable;

/**
 * Created by hzy on 2019/1/17
 * <p>
 * MVP BaseMvpActivity
 *
 * @author hzy
 */
public abstract class BaseMvpActivity<T extends BasePresenter> extends BaseGActivity {


    @Inject //dragger
    @Nullable
    public T mPresenter;

    @Override
    protected void initLayoutAfter() {
        super.initLayoutAfter();
        initInject();
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
    }

    protected ActivityComponent getActivityComponent() {
        return DaggerActivityComponent.builder()
                .myAppComponent(MyApp.getAppComponent())
                .activityModule(new ActivityModule(this))
                .build();
    }
    protected abstract void initInject();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }
}
