package com.uphone.chronicdisease.base;


import android.content.Intent;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;


import java.util.ArrayList;
import java.util.List;

import com.radish.baselibrary.base.BaseActivity;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.view.dialog.ObtainPhotoFragment;


/**
 * 选择图片的Activity
 * 作者：赵亚坤
 * 时间：2018/2/2-10:52
 */

public abstract class BaseTakePhotoActivity extends BaseGActivity {

    private List<LocalMedia> mSelectList = new ArrayList<>();

    //单选拍照
    public void getCameraPhoto(boolean isCrop) {
        PictureSelector.create(mActivity)
                .openCamera(PictureMimeType.ofImage())
                .theme(R.style.picture_default_style)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .enableCrop(isCrop)
                .compress(true)
                .minimumCompressSize(100)
                .selectionMedia(mSelectList)// 是否传入已选图片
                .circleDimmedLayer(true)
                .synOrAsy(false)
                .forResult(PictureConfig.CHOOSE_REQUEST);
    }

    //相册选择
    public void getRadioPhoto(boolean isCrop) {
        PictureSelector.create(mActivity)
                .openGallery(PictureMimeType.ofImage())
                .theme(R.style.picture_default_style)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .enableCrop(isCrop)
                .compress(true)
                .minimumCompressSize(100)
                .selectionMedia(mSelectList)// 是否传入已选图片
                .circleDimmedLayer(true)
                .synOrAsy(false)
                .forResult(PictureConfig.CHOOSE_REQUEST);
    }

    //相册选择
    public void getMultiselectPhoto() {
        PictureSelector.create(mActivity)
                .openGallery(PictureMimeType.ofImage())
                .theme(R.style.picture_default_style)
                .compress(true)
                .minimumCompressSize(100)
                .synOrAsy(false)
                .forResult(PictureConfig.CHOOSE_REQUEST);
    }

    // 多张 相册选择
    public void getMultiAlbum(boolean isCrop, int maxCount){
        PictureSelector.create(mActivity)
                .openGallery(PictureMimeType.ofImage())
                .theme(R.style.picture_default_style)
                .selectionMode(PictureConfig.MULTIPLE)
                .maxSelectNum(maxCount)
                .enableCrop(isCrop)
                .compress(true)
                .minimumCompressSize(100)
                .circleDimmedLayer(true)
                .synOrAsy(false)
                .forResult(PictureConfig.CHOOSE_REQUEST);
    }

    /**
     * @author 赵亚坤
     * @time 2017/12/25  10:21
     * @describe 拍照或者选择
     */
    public void getPhoto(final boolean isCrop) {
        ObtainPhotoFragment photoFragment = new ObtainPhotoFragment();
        photoFragment.setAboutText("从相册选择", "拍照");
        photoFragment.setPhotoClickListener(new ObtainPhotoFragment.OnPhotoClickListener() {
            @Override
            public void onPhotograph() {
                getRadioPhoto(isCrop);

            }

            @Override
            public void onAlbum() {
                getCameraPhoto(isCrop);
            }
        });
        photoFragment.show(getFragmentManager(), "ObtainPhotoFragment");
    }

    public void getPhoto(final boolean isCrop, final int maxCount) {
        ObtainPhotoFragment photoFragment = new ObtainPhotoFragment();
        photoFragment.setAboutText("从相册选择", "拍照");
        photoFragment.setPhotoClickListener(new ObtainPhotoFragment.OnPhotoClickListener() {
            @Override
            public void onPhotograph() {
                getMultiAlbum(isCrop, maxCount);

            }

            @Override
            public void onAlbum() {
                getCameraPhoto(isCrop);
            }
        });
        photoFragment.show(getFragmentManager(), "ObtainPhotoFragment");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    mSelectList.clear();
                    mSelectList = PictureSelector.obtainMultipleResult(data);
                    // 例如 LocalMedia 里面返回三种path
                    // 1.media.getPath(); 为原图path
                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
                    // 如果裁剪并压缩了，以取压缩路径为准，因为是先裁剪后压缩的
                    break;
            }
        }
    }

    public List<LocalMedia> getSelectList() {
        return mSelectList;
    }
}
