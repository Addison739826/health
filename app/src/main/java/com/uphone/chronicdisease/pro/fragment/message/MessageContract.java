package com.uphone.chronicdisease.pro.fragment.message;

import android.support.v7.widget.RecyclerView;

import com.uphone.chronicdisease.adapter.MessageAdapter;
import com.uphone.chronicdisease.base.mvp.BasePresenter;
import com.uphone.chronicdisease.base.mvp.BaseView;

/**  * <pre>  *     author : radish  *     e-mail : 15703379121@163.com  *     time   : 2019/4/16  *     desc   :  * </pre>  */
public interface MessageContract {
    // update UI
    interface View extends BaseView {
        void updateUI(String body);
    }

    // 连接 数据
    interface Presenter extends BasePresenter<View> {
        void loadNetData(RecyclerView mRvMessage, MessageAdapter mAdapter);
    }
}
