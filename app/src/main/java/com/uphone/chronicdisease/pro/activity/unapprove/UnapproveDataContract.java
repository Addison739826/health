package com.uphone.chronicdisease.pro.activity.unapprove;

import com.uphone.chronicdisease.base.mvp.BasePresenter;
import com.uphone.chronicdisease.base.mvp.BaseView;

import okhttp3.ResponseBody;

/**
 * Created by DELL on 2019/12/22.
 */

public interface UnapproveDataContract {

    interface View extends BaseView {
        void updateView(ResponseBody responseBean, int status);
    }

    interface Presenter extends BasePresenter<UnapproveDataContract.View> {
        void postUnapprove(String username, String password);
    }
}
