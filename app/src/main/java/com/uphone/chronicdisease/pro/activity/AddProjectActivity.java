package com.uphone.chronicdisease.pro.activity;

import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.BaseGActivity;

/**
 * @ClassName: AddProjectActivity
 * @Description: 加入项目
 * @Date: 2019/12/23
 * @Author: zx
 */
public class AddProjectActivity  extends BaseGActivity {

    @Override
    protected void initBundle() {

    }

    @Override
    protected int initLayout() {
        return R.layout.activity_join_project;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initTitle() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}
