package com.uphone.chronicdisease.pro.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.BaseGActivity;
import com.uphone.chronicdisease.view.dialog.AboutUsHelpDialog;
import com.uphone.chronicdisease.view.dialog.DataReportDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @ClassName: AboutUstActivity
 * @Description: 关于我们
 * @Date: 2019/12/23
 * @Author: zx
 */
public class AboutUstActivity extends BaseGActivity {
    @BindView(R.id.tv_title_name)
    TextView tvTitleName;

    @BindView(R.id.btn_help)
    Button btn_help;

    @Override
    protected void initBundle() {
        isUseImmersionBar = false;
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_about_us;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initTitle() {
        tvTitleName.setText("关于我们");
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }

    @OnClick({ R.id.iv_back,R.id.btn_help})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_help:
                AboutUsHelpDialog aboutUsHelpDialog = new AboutUsHelpDialog();
                Bundle bundle = new Bundle();
                bundle.putString("instruction", "");
                bundle.putString("url", "");
                aboutUsHelpDialog.setArguments(bundle);
                aboutUsHelpDialog.show(getFragmentManager(), "aboutUsHelpDialog");
                break;

        }
    }
}
