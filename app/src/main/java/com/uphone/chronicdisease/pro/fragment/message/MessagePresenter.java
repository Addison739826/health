package com.uphone.chronicdisease.pro.fragment.message;

import android.arch.lifecycle.LifecycleOwner;
import android.support.v7.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.uber.autodispose.AutoDispose;
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.adapter.MessageAdapter;
import com.uphone.chronicdisease.base.mvp.BasePAV;
import com.uphone.chronicdisease.bean.MessageBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.RxSchedulers;
import com.alibaba.fastjson.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

/**
 * <pre>  *     author : radish  *     e-mail : 15703379121@163.com  *     time   : 2019/4/16  *     desc   :  * </pre>
 */
public class MessagePresenter extends BasePAV<MessageContract.View> implements MessageContract.Presenter {
    @Inject
    public MessagePresenter() {

    }

    @Override
    public void loadNetData(RecyclerView mRvMessage,MessageAdapter mAdapter) {
        Map<String, String> map = new HashMap<>();
        map.put("token", "admin");
        map.put("userId", "1");
        MyApp.apiService(ApiService.class)
                .postService(ApiService.getMessage, setParams(map))
                .compose(RxSchedulers.io_main())
                .doOnSubscribe(d -> mView.showLoading())
                .doFinally(() -> mView.closeLoading())
                .as(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from((LifecycleOwner) mView)))
                .subscribe(bean -> {
                    JSONObject jsonObject = new JSONObject(bean.string());
                    int code = jsonObject.getInt("code");
                    List<MessageBean> list = JSONArray.parseArray(jsonObject.getString("data"), MessageBean.class);
                    if (code == 0) {
                        //ToastUtils.showShort(code+"");
                        mAdapter.setNewData(list);
                    }else {
                        mView.onSuccess(code,jsonObject.getString("msg"));
                    }
                    mView.updateUI(bean.string());
                }, throwable -> {
                    // 异常
                    mView.onFail();
                }, () -> {
                    // 完成
                });
    }
}
