package com.uphone.chronicdisease.pro.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONArray;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.contrarywind.view.WheelView;
import com.radish.baselibrary.Intent.IntentUtils;
import com.radish.baselibrary.utils.ToastUtil;
import com.uphone.chronicdisease.MainActivity;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.adapter.PatientDetailAdapter;
import com.uphone.chronicdisease.adapter.ResearchAdapter;
import com.uphone.chronicdisease.base.BaseGActivity;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.bean.DialysisWayBean;
import com.uphone.chronicdisease.bean.IsUnreadBean;
import com.uphone.chronicdisease.bean.MessageBean;
import com.uphone.chronicdisease.bean.PatientDetailBean;
import com.uphone.chronicdisease.bean.Patientlist;
import com.uphone.chronicdisease.bean.ResearchBean;
import com.uphone.chronicdisease.bean.TreatmentPlanBean;
import com.uphone.chronicdisease.bean.researchStrpBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.simple.RxSimpleTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.view.SingleOptionsPicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

/**
 * 病人信息
 */
public class PatientsInfoActivity extends BaseGActivity {
    private String resId = "";
    private String dwId = "";
    private String tpId = "";

    @BindView(R.id.tv_title_name)
    TextView tvTitleName;
    @BindView(R.id.ll_info)
    LinearLayout llInfo;
    @BindView(R.id.ll_basicInfo)
    LinearLayout llBasicInfo;
    @BindView(R.id.tv_modify)
    TextView tvModify;
    @BindView(R.id.tv_save)
    TextView tv_save;

    /* @BindView(R.id.rl_t0)
     RelativeLayout mRl_t0;*/
    @BindView(R.id.wheelview)
    WheelView wheelview;

    @BindView(R.id.tv_sex)
    TextView tv_sex;
    @BindView(R.id.ll_sex)
    LinearLayout mIvSex;

    @BindView(R.id.tv_age)
    TextView mTvAge;
    @BindView(R.id.ll_age)
    LinearLayout mIvAge;

    @BindView(R.id.tv_height)
    TextView mTvHeight;
    @BindView(R.id.ll_height)
    LinearLayout mIvHeight;

    @BindView(R.id.tv_group)
    TextView mTvGroup;
    @BindView(R.id.ll_group)
    LinearLayout mIvGroup;

    @BindView(R.id.tv_type)
    TextView mTvType;
    @BindView(R.id.ll_type)
    LinearLayout mIvType;

    @BindView(R.id.tv_dose)
    TextView mTvDose;
    @BindView(R.id.iv_dose)
    ImageView mIvDose;

    //个人信息
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_group_show)
    TextView tv_group_show;
    @BindView(R.id.tv_diagnosis)
    TextView tv_diagnosis;
    @BindView(R.id.tv_number)
    TextView tv_number;
    @BindView(R.id.tv_sex_show)
    TextView tv_sex_show;
    @BindView(R.id.tv_age_show)
    TextView tv_age_show;
    @BindView(R.id.tv_patient)
    TextView tv_patient;
    @BindView(R.id.tv_type_show)
    TextView tv_type_show;
    @BindView(R.id.tv_type1)
    TextView tv_type1;
    @BindView(R.id.rv_research)
    RecyclerView mRvResearch;
    @BindView(R.id.box_plan1)
    CheckBox box_plan1;
    @BindView(R.id.box_plan2)
    CheckBox box_plan2;

    @BindView(R.id.tv_plan1)
    TextView tv_plan1;
    @BindView(R.id.tv_plan2)
    TextView tv_plan2;

    @BindView(R.id.et_patient_diagnose)
    EditText et_patient_diagnose;
    @BindView(R.id.et_patient_name)
    EditText et_patient_name;

    private PatientDetailAdapter mAdapter;
    private String patId;
    private String token;
    private String userId;
    private String projId;
    private List<String> listString;
    private List<TreatmentPlanBean> listtp = new ArrayList<>();
    private PatientDetailBean mBean;
    @Override
    protected void initBundle() {
        isUseImmersionBar = false;
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_patients_info;
    }

    @Override
    protected void initView() {
        projId =  SPUtils.getInstance().getString("projId");
        token  =  SPUtils.getInstance().getString("token");
        userId =   SPUtils.getInstance().getString("userId");
        patId = getIntent().getStringExtra("patId");
        mAdapter = new PatientDetailAdapter(PatientsInfoActivity.this);
        mRvResearch.setLayoutManager(new LinearLayoutManager(PatientsInfoActivity.this));
        mRvResearch.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mAdapter.setEnableLoadMore(false);

            }
        }, mRvResearch);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                PatientDetailBean.Step bean = mAdapter.getData().get(position);
                IntentUtils.getInstance()
                        .with(context, PatientsInfoDetailActivity.class)
                        .putString("dataId", bean.getDataId()+"")//把列表patId传过去
                        .putString("patId",patId)//把列表patId传过去
                        .putString("title",bean.getTitle())//把列表patId传过去
                        .putString("tempId",bean.getTempId()+"")

                        .start();
            }
        });
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.getInstance()
                        .with(context, PatientsInfoDetailActivity.class)
                        .start();
            }
        });
    }

    @Override
    protected void initTitle() {
        tvTitleName.setText("患者信息");
    }

    @Override
    protected void initData() {
        TreatmentPlan();
        patientDetail();
    }

    @Override
    protected void initListener() {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.tv_save,R.id.tv_del, R.id.tv_modify, R.id.iv_back, R.id.tv_analysis, R.id.ll_sex, R.id.ll_age, R.id.ll_height, R.id.ll_group, R.id.ll_type, R.id.iv_dose,R.id.box_plan1,R.id.box_plan2,R.id.tv_dose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_analysis:
                IntentUtils.getInstance()
                        .with(context, PatientsAnalysisActivity.class)
                        .putString("patId", patId)
                        .putString("title", tv_name.getText().toString())
                        .start();
                break;
            case R.id.tv_del:
                patientDel();
                break;
            case  R.id.tv_save:
                llBasicInfo.setVisibility(View.GONE);
                llInfo.setVisibility(View.VISIBLE);

                tv_save.setVisibility(View.GONE);
                tvModify.setVisibility(View.VISIBLE);



                if(TextUtils.isEmpty(et_patient_name.getText().toString())){
                    ToastUtil.showShort("请输入患者姓名！");
                    return;

                }
                if(TextUtils.isEmpty(tv_sex.getText().toString())){
                    ToastUtil.showShort("请选择性别！");
                    return;

                }
                if(TextUtils.isEmpty(mTvAge.getText().toString())){
                    ToastUtil.showShort("请选择年龄！");
                    return;

                }

                if(TextUtils.isEmpty(mTvHeight.getText().toString())){
                    ToastUtil.showShort("请选择身高！");
                    return;

                }
                if(TextUtils.isEmpty(et_patient_diagnose.getText().toString())){
                    ToastUtil.showShort("请输入诊断！");
                    return;

                }
                if(TextUtils.isEmpty(mTvGroup.getText().toString())){
                    ToastUtil.showShort("请选择研究组！");
                    return;

                }

                if(TextUtils.isEmpty(mTvType.getText().toString())){
                    ToastUtil.showShort("请选择透析方式！");
                    return;

                }
                if(box_plan1.isChecked() == false && box_plan2.isChecked() == false){
                    ToastUtil.showShort("请勾选治疗方案！");
                    return;

                }
                patientAdd();
                break;
            case R.id.tv_modify:

                llBasicInfo.setVisibility(View.VISIBLE);
                llInfo.setVisibility(View.GONE);


                tv_save.setVisibility(View.VISIBLE);
                tvModify.setVisibility(View.GONE);
                Log.e("getPatient",mBean.getPatient().getPatName());
                mBean.getPatient().getPatName();
                et_patient_name.setText(mBean.getPatient().getPatName());
                tv_sex.setText(mBean.getPatient().getSex() == 0 ? "女" : "男");
                mTvAge.setText(mBean.getPatient().getAge()+"");
                et_patient_diagnose.setText("诊  断："+mBean.getPatient().getDiagnosis());
                mTvGroup.setText(mBean.getResearch().getResName());
                mTvType.setText(mBean.getPatient().getDiagnosis());
                if(mBean.getDw() != null){
                    mTvType.setText(mBean.getDw().getTitle());
                }
                if("酮酸治疗".equals(mBean.getTp().getTitle())){
                    box_plan1.setChecked(true);
                }else {
                    box_plan2.setChecked(true);
                }
                break;
            /*case R.id.rl_t0:
                IntentUtils.getInstance()
                        .with(context, PatientsInfoDetailActivity.class)
                        .start();
                break;*/
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_sex:
                List<String> listSex = new ArrayList<>();
                listSex.add("男");
                listSex.add("女");
                SingleOptionsPicker.openOptionsPicker(this, listSex, 2, tv_sex);

                break;
            case R.id.ll_age:
                List<String> listAge = new ArrayList<>();
                for (int i = 1990; i < 2070; i++) {
                    listAge.add(i+"");
                }
                SingleOptionsPicker.openOptionsPicker(this, listAge, 2, mTvAge);

                break;

            case R.id.ll_height:
                List<String> listheight = new ArrayList<>();
                for (int i = 140; i < 230; i++) {
                    listheight.add(i + "cm");
                }
                SingleOptionsPicker.openOptionsPicker(this, listheight, 2, mTvHeight);

                break;
            case R.id.ll_group:
                //research();
                break;
            case R.id.ll_type:
                Dialy();
                break;
            case R.id.iv_dose:
                SingleOptionsPicker.openOptionsPicker(PatientsInfoActivity.this, listString, 2, mTvDose);
                break;
            case R.id.tv_dose:

                SingleOptionsPicker.openOptionsPicker(PatientsInfoActivity.this, listString, 2, mTvDose);
                break;
            case R.id.box_plan1:
                if (box_plan1.isChecked()) {
                    tpId = listtp.get(0).getTpId()+"";
                } else {

                }
                break;
            case R.id.box_plan2:
                if (box_plan2.isChecked()) {
                    tpId = listtp.get(1).getTpId()+"";
                } else {

                }
                break;

        }
    }

    private void Dialy(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("projId", projId);
        MyApp.apiService(ApiService.class)
                .dialysisWay(setParams(map))
                .compose(new RxSimpleTransformer<ArrayList<DialysisWayBean>>())
                .subscribe(new SimpleObserver<ArrayList<DialysisWayBean>>(mActivity){
                    @Override
                    public void onNext(ArrayList<DialysisWayBean> list) {
                        List<String> listtype = new ArrayList<>();
                        for(int i=0;i<list.size();i++){
                            listtype.add(list.get(i).getTitle());
                        }
                        //SingleOptionsPicker.openOptionsPicker(AddPatientsInfoActivity.this, listtype, 2, mTvType);
                        String select = mTvType.getText().toString();
                        new SingleOptionsPicker(PatientsInfoActivity.this, select, listtype,
                                new SingleOptionsPicker.OnPickerOptionsClickListener() {
                                    @Override
                                    public void onOptionsSelect(int options1, int options2, int options3, View view) {
                                        mTvType.setText(list.get(options1).getTitle());
                                        //ToastUtils.showShort(list.get(options1).getResId()+"");
                                        dwId = list.get(options1).getDwId()+"";
                                        //researchStrp(resId);
                                    }
                                }).show();

                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();

                    }
                });
    }

    //http://122.14.213.160:7081/jeefast-rest/api/research/list?projId=1&token=admin
    private void research(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("projId", projId);
        MyApp.apiService(ApiService.class)
                .research(setParams(map))
                .compose(new RxSimpleTransformer<ArrayList<ResearchBean>>())
                .subscribe(new SimpleObserver<ArrayList<ResearchBean>>(mActivity){
                    @Override
                    public void onNext(ArrayList<ResearchBean> list) {
                        List<String> listgroup = new ArrayList<>();
                        for(int i=0;i<list.size();i++){
                            listgroup.add(list.get(i).getResName());
                        }
                        String select = mTvGroup.getText().toString();
                        new SingleOptionsPicker(PatientsInfoActivity.this, select, listgroup,
                                new SingleOptionsPicker.OnPickerOptionsClickListener() {
                                    @Override
                                    public void onOptionsSelect(int options1, int options2, int options3, View view) {
                                        mTvGroup.setText(list.get(options1).getResName());
                                        //ToastUtils.showShort(list.get(options1).getResId()+"");
                                        resId = list.get(options1).getResId()+"";
                                        //researchStrp(resId);
                                    }
                                }).show();



                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();

                    }
                });
    }

   /* private void researchStrp(String resId){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("resId", resId);
        MyApp.apiService(ApiService.class)
                .researchStrp(setParams(map))
                .compose(new RxSimpleTransformer<ArrayList<researchStrpBean>>())
                .subscribe(new SimpleObserver<ArrayList<researchStrpBean>>(mActivity){
                    @Override
                    public void onNext(ArrayList<researchStrpBean> list) {
                        mAdapter.setNewData(list);
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();

                    }
                });
    }*/

    private void patientDetail() {
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("patId", patId);
        MyApp.apiService(ApiService.class)
                .patientDetail(setParams(map))
                .compose(new RxSimpleTransformer<>())
                .subscribe(new SimpleObserver<PatientDetailBean>() {
                    @Override
                    public void onNext(PatientDetailBean bean) {
                        mBean = bean;
                        tv_name.setText(bean.getPatient().getPatName());

                        if ("开同组".equals(bean.getResearch().getResName())) {
                            tv_group_show.setBackgroundResource(R.drawable.btn_shape_orange);
                        } else {
                            tv_group_show.setBackgroundResource(R.drawable.btn_shape_green);
                        }
                        tv_group_show.setText(bean.getResearch().getResName());

                        tv_number.setText(bean.getPatient().getProjId() + "");
                        tv_sex_show.setText(bean.getPatient().getSex() == 0 ? "女" : "男");
                        tv_age_show.setText(bean.getPatient().getAge() + "岁");
                        tv_patient.setText(bean.getPatient().getDiagnosis());
                        tv_type_show.setText(bean.getTp().getTitle());
                        resId = bean.getPatient().getResId()+"";
                        mAdapter.setNewData(bean.getStep());
                        if(bean.getDw() != null){
                            tv_diagnosis.setText(bean.getDw().getTitle());
                            tv_type1.setText(bean.getDw().getTitle());
                        }
                    }
                });
    }


    private void TreatmentPlan(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("projId", projId);
        MyApp.apiService(ApiService.class)
                .treatmentPlan(setParams(map))
                .compose(new RxSimpleTransformer<ArrayList<TreatmentPlanBean>>())
                .subscribe(new SimpleObserver<ArrayList<TreatmentPlanBean>>(mActivity){
                    @Override
                    public void onNext(ArrayList<TreatmentPlanBean> list) {
                        listtp = list;
                        if(list.size()>0 && list != null){
                            tv_plan1.setText(list.get(0).getTitle());
                            tv_plan2.setText(list.get(1).getTitle());

                            if(list.get(1).getUnit() != null && !list.get(1).getUnit().equals("")){
                                listString = Arrays.asList(list.get(1).getUnit().split(","));
                            }

                            mTvDose.setText(listString.get(0));
                        }

                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();

                    }
                });
    }



    //添加病人信息
    private void patientAdd(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("patName", et_patient_name.getText().toString());
        map.put("sex", tv_sex.getText().toString()=="女"? "0" : "1");
        map.put("age", mTvAge.getText().toString());
        map.put("tall", mTvHeight.getText().toString().substring(0,3));
        map.put("projId", projId);

        map.put("resId", resId);//研究组id
        map.put("diagnosis", et_patient_diagnose.getText().toString());
        map.put("tpId", tpId);//治疗方式
        map.put("dwId", dwId);//:透析方式
        map.put("docId", userId);

//        patName:患者姓名
//        sex:性别
//        age:年龄
//        tall:身高
//        projId:项目ID
//        resId:研究组ID
//        diagnosis:诊断【传文本】
//        tpId:治疗方式
//        docId:医生ID
        MyApp.apiService(ApiService.class)
                .patientAdd(setParams(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(BaseBean bean) {
                        BaseBean baseBean = bean;
                        if(baseBean.getCode() == 0){
                            ToastUtils.showShort("保存成功！");
                            finish();
                        }else {
                            ToastUtils.showShort(baseBean.getMsg());
                        }


                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    //删除患者
    private void patientDel(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("id", patId);
        MyApp.apiService(ApiService.class)
                .patientDelete(setParams(map))
                .compose(new RxSimpleTransformer(new Object()))
                .subscribe(new SimpleObserver<BaseBean>(mActivity){
                    @Override
                    public void onNext(BaseBean data) {

                        super.onNext(data);
                        if(data.getCode() == 0){
                           ToastUtil.showShort("删除患者成功！");
                       }

                    }
                });
    }

}
