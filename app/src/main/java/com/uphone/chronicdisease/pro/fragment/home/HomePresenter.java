package com.uphone.chronicdisease.pro.fragment.home;

import android.arch.lifecycle.LifecycleOwner;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.uber.autodispose.AutoDispose;
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.base.mvp.BasePAV;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.RxSchedulers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * <pre>  *     author : radish  *     e-mail : 15703379121@163.com  *     time   : 2019/4/16  *     desc   :  * </pre>
 */
public class HomePresenter extends BasePAV<HomeContract.View> implements HomeContract.Presenter {
    @Inject
    public HomePresenter() {

    }

    @Override
    public void loadNetData() {
        Map<String, String> map = new HashMap<>();
        map.put("token", "d7f55d40ff168e4158b4833f37d51ae6");
        map.put("uid", "8");
        map.put("lid", "1");
        MyApp.apiService(ApiService.class)
                .postService("question/source", RequestBody.create(MediaType.get("application/json"), new Gson().toJson(map)))
                .compose(RxSchedulers.io_main())
                .doOnSubscribe(d -> mView.showLoading())
                .doFinally(() -> mView.closeLoading())
                .as(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from((LifecycleOwner) mView)))
                .subscribe(bean -> {
                    mView.updateUI(bean.string());
                }, throwable -> {
                    // 异常
                    LogUtils.e("throwable:" + throwable.toString());
                    mView.onFail();
                }, () -> {
                    // 完成
                });
    }
}
