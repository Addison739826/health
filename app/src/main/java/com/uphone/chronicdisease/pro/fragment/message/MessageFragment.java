package com.uphone.chronicdisease.pro.fragment.message;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.blankj.utilcode.util.SPUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.radish.baselibrary.Intent.IntentUtils;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.adapter.MessageAdapter;
import com.uphone.chronicdisease.base.BaseGLFragment;
import com.uphone.chronicdisease.bean.AuditFailedBean;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.bean.MessageBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.simple.RxListPageTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.pro.activity.PatientsInfoActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

public class MessageFragment extends BaseGLFragment {

    @BindView(R.id.rv_message)
    RecyclerView mRvMessage;
    @BindView(R.id.status)
    View status;
    private MessageAdapter mAdapter;
    private List<MessageBean> list;
    private String title = "";
    private String token;
    private String userId;
    public static MessageFragment newInstance(String title) {
        Bundle args = new Bundle();
        MessageFragment fragment = new MessageFragment();
        fragment.title = title;
        fragment.setArguments(args);

        return fragment;
    }
    @Override
    protected void initBundle() {

    }

    @Override
    protected int initLayout() {
        return  R.layout.fragment_message;
    }

    @Override
    protected void initView() {
        token  =  SPUtils.getInstance().getString("token");
        userId =   SPUtils.getInstance().getString("userId");
        readAll();
        mAdapter = new MessageAdapter(getActivity());
        mRvMessage.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRvMessage.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                //mAdapter.setEnableLoadMore(false);

                pageNum += 1;
                getMessage();


            }
        }, mRvMessage);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MessageBean bean = mAdapter.getData().get(position);
                IntentUtils.getInstance()
                        .with(getActivity(), PatientsInfoActivity.class)
                        .putString("patId", bean.getToId()+"")//把列表patId传过去
                        .start();
            }
        });
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
        //mAdapter.setNewData(list);

        //mPresenter.loadNetData(mRvMessage,mAdapter);
        getMessage();
    }

    private void getMessage(){
        Map<String, String> map = new HashMap<>();
        map.put("page",String.valueOf(pageNum));
        map.put("limit",String.valueOf(pageSize));
        map.put("token", token);
        map.put("userId", userId);
        MyApp.apiService(ApiService.class)
                .getMessage(setParams(map))
                .compose(new RxListPageTransformer(mAdapter, pageNum))
                .subscribe(new SimpleObserver(getActivity()));
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void initListener() {

    }

    private void readAll(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("userId", userId);
        MyApp.apiService(ApiService.class)
                .readAll(setParams(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(BaseBean bean) {
                        BaseBean baseBean = bean;
                        if(baseBean.getCode() == 0){

                        }else {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
