package com.uphone.chronicdisease.pro.activity.login;

import android.arch.lifecycle.LifecycleOwner;
import android.util.Log;
import android.widget.Button;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.radish.baselibrary.utils.EncryptUtils;
import com.radish.baselibrary.utils.LogUtils;
import com.radish.baselibrary.utils.NetUtils;
import com.uber.autodispose.AutoDispose;
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.base.mvp.BasePAV;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.RetrofitManager;
import com.uphone.chronicdisease.http.RxSchedulers;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.view.CountDown;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

/**
 * Created by hzy on 2019/1/18
 * LoginPresenter
 *
 * @author hzy
 *
 */
public class LoginPresenter extends BasePAV<LoginContract.View> implements LoginContract.Presenter {

    @Inject
    LoginPresenter() {

    }

    @Override
    public void postLogin(String username, String password) {
      //  mView.showLoading();
//        Map<String, String> map = new HashMap<>();
//        map.put("mobile", username);
//        map.put("password",  EncryptUtils.encryptMD5ToString(password));
//        MyApp.apiService(ApiService.class)
//                .postService("login/login", RequestBody.create(MediaType.get("application/json"), new Gson().toJson(map)))
//                .compose(RxSchedulers.io_main())
//                .as(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from((LifecycleOwner) mView)))
//                .subscribe(bean -> {
//                    mView.closeLoading();
//                    if (bean != null) {
//                        try {
//                            JSONObject jsonObject = new JSONObject(bean.string());
//                            int status = jsonObject.getInt("status");
//                            LogUtils.e("status:" + status + "  responseBean:" + bean.string());
//                            if (status != 1) {
//                                ToastUtils.showShort(jsonObject.getString("msg"));
//                            }
//                            mView.updateView(bean,status);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, throwable -> {
//                    Log.e("addison",throwable.getMessage());
//                    mView.closeLoading();
//                    mView.onFail();
//                });


    }

    @Override
    public void getCode(String phone, Button smBtn) {
        mView.showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("phone", phone);
       MyApp.apiService(ApiService.class)
               // .getCode(setParams(map))
               .postService(ApiService.getcode,setParams(map))
                .compose(RxSchedulers.io_main())
                .as(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from((LifecycleOwner) mView)))
                .subscribe(bean -> {
                    mView.closeLoading();
                    if (bean != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(bean.string());
                            int code = jsonObject.getInt("code");
                            LogUtils.e("code:" + code );
                           if (code == 0) {
                               new CountDown().getTime(smBtn,
                                       CountDown.COUNTTIME_ADDCARD);
                               mView.updateView(bean,code);

                            }else {

                           }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }, throwable -> {
                    mView.closeLoading();
                    mView.onFail();
                });

//        RetrofitManager.sApiService().getCode(setParams(map))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<ResponseBody>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        ToastUtils.showShort("call");
//                        mView.closeLoading();
//                    }
//
//                    @Override
//                    public void onNext(ResponseBody responseBody) {
//                        ToastUtils.showShort(" response");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        ToastUtils.showShort("onError");
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        ToastUtils.showShort("onComplete");
//                    }
//                });

    }
}
