package com.uphone.chronicdisease.pro.activity.login;

import android.widget.Button;

import com.uphone.chronicdisease.base.mvp.BasePresenter;
import com.uphone.chronicdisease.base.mvp.BaseView;

import okhttp3.ResponseBody;

/**
 * Created by hzy on 2019/1/18
 * LoginContract
 *
 * @author Administrator
 * */
public interface LoginContract {


    interface View extends BaseView {
        void updateView(ResponseBody responseBean,int status);
    }

    interface Presenter extends BasePresenter<View> {
        void postLogin(String username, String password);
        void getCode(String phone, Button smBtn);
    }



}
