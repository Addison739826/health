package com.uphone.chronicdisease.pro.activity.project;

import com.uphone.chronicdisease.base.mvp.BasePresenter;
import com.uphone.chronicdisease.base.mvp.BaseView;
import com.uphone.chronicdisease.pro.activity.login.LoginContract;

import okhttp3.ResponseBody;

/**
 * Created by DELL on 2019/12/22.
 */

public interface CdkProjectDetailContract {

    interface View extends BaseView {
        void updateView(ResponseBody responseBean, int status);
    }

    interface Presenter extends BasePresenter<CdkProjectDetailContract.View> {
        void postCdkProjectDatail(String username, String password);
    }
}
