package com.uphone.chronicdisease.pro.activity.unapprove;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.radish.baselibrary.Intent.IntentUtils;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.adapter.CdkProjectDetailAdapter;
import com.uphone.chronicdisease.adapter.UnapproveDataAdapter;
import com.uphone.chronicdisease.base.mvp.BaseMvpActivity;
import com.uphone.chronicdisease.bean.AuditFailedBean;
import com.uphone.chronicdisease.bean.CdkProjectBean;
import com.uphone.chronicdisease.bean.Patientlist;
import com.uphone.chronicdisease.bean.UnapproveBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.simple.RxListPageTransformer;
import com.uphone.chronicdisease.http.simple.RxSimpleTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.pro.activity.PatientsInfoActivity;
import com.uphone.chronicdisease.pro.activity.project.CdkProjectDetailContract;
import com.uphone.chronicdisease.pro.activity.project.CdkProjectDetailePresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.ResponseBody;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

/**
 * @ClassName: UnapproveDataActivity
 * @Description: 审核未通过数据
 * @Date: 2019/12/23
 * @Author: zx
 */
public class UnapproveDataActivity extends BaseMvpActivity<CdkProjectDetailePresenter> implements CdkProjectDetailContract.View {
    @BindView(R.id.tv_title_name)
    TextView tvTitleName;
    @BindView(R.id.rv_unapprove)
    RecyclerView mRvUnapprove;
    private UnapproveDataAdapter mAdapter;
    private List<UnapproveBean> list;
    private String token;
    private String userId;
    @Override
    public void updateView(ResponseBody responseBean, int status) {

    }

    @Override
    protected void initInject() {

    }

    @Override
    protected void initBundle() {
        isUseImmersionBar = false;
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_unapprove;
    }

    @Override
    protected void initView() {
        token  =  SPUtils.getInstance().getString("token");
        userId =   SPUtils.getInstance().getString("userId");
    }

    @Override
    protected void initTitle() {
        tvTitleName.setText("审核未通过数据");
    }

    @Override
    protected void initData() {
     /*   list = new ArrayList<>();
        list.add(new UnapproveBean("T0"));
        list.add(new UnapproveBean("T3"));*/

        mAdapter = new UnapproveDataAdapter(mActivity);
        mRvUnapprove.setLayoutManager(new LinearLayoutManager(mActivity));
        mRvUnapprove.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                pageNum += 1;
                auditFailed();

            }
        }, mRvUnapprove);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                AuditFailedBean bean = mAdapter.getData().get(position);
                IntentUtils.getInstance()
                        .with(context, PatientsInfoActivity.class)
                        .putString("patId", bean.getPatientId()+"")//把列表patId传过去
                        .start();
            }
        });
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });

        auditFailed();
    }

    @Override
    protected void initListener() {

    }

    @OnClick({ R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

        }
    }


    private void auditFailed(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("docId", userId);
        map.put("page",String.valueOf(pageNum));
        map.put("limit",String.valueOf(pageSize));
        MyApp.apiService(ApiService.class)
                .auditFailed(setParams(map))
                .compose(new RxListPageTransformer(mAdapter, pageNum))
                .subscribe(new SimpleObserver(mActivity));
    }
}
