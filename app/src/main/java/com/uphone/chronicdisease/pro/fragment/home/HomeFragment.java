package com.uphone.chronicdisease.pro.fragment.home;

import android.os.Bundle;
import android.widget.TextView;

import com.radish.baselibrary.utils.LogUtils;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.mvp.BaseMvpFragment;

import butterknife.BindView;

/**
 * <pre>  *     author : radish  *     e-mail : 15703379121@163.com  *     time   : 2019/4/16  *     desc   :  * </pre>
 */
public class HomeFragment extends BaseMvpFragment<HomePresenter> implements HomeContract.View {
    private String title = "";

    public static HomeFragment newInstance(String title) {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.title = title;
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.tv)
    TextView tv;

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected void initBundle() {

    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView() {
        tv.setText(title + "");
    }

    @Override
    protected void loadData() {
        mPresenter.loadNetData();
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void updateUI(String body) {
        LogUtils.e("更新UI" + body);
        tv.setText(title + "\n" + body);
    }
}
