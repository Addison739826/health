package com.uphone.chronicdisease.pro.fragment.personal;

import android.app.Dialog;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.gyf.immersionbar.components.ImmersionOwner;
import com.gyf.immersionbar.components.ImmersionProxy;
import com.radish.baselibrary.Intent.IntentUtils;
import com.radish.baselibrary.utils.ToastUtil;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.mvp.BaseMvpFragment;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.bean.DoctorInfoBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.pro.activity.AboutUstActivity;
import com.uphone.chronicdisease.pro.activity.ModifyUserInfoActivity;
import com.uphone.chronicdisease.pro.activity.login.LoginActivity;
import com.uphone.chronicdisease.pro.activity.unapprove.UnapproveDataActivity;
import com.uphone.chronicdisease.view.MyDialog;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

/**
 * @ClassName: PersonalFragment
 * @Description: 个人中心
 * @Date: 2019/12/23
 * @Author: zx
 */
public class PersonalFragment extends BaseMvpFragment<PersonalPresenter> implements PersonalContract.View, ImmersionOwner {

    @BindView(R.id.ll_tab)
    LinearLayout mLltab;
    @BindView(R.id.ll_about_us)
    LinearLayout mLlAboutUs;

    @BindView(R.id.rl_group1)
    RelativeLayout mRlgroup1;
    @BindView(R.id.tv_group1)
    TextView mTvgroup1;
    @BindView(R.id.rl_group1_line)
    View mRlGroup1Line;

    @BindView(R.id.rl_group2)
    RelativeLayout mRlgroup2;
    @BindView(R.id.tv_group2)
    TextView mTvgroup2;
    @BindView(R.id.rl_group2_line)
    View mRlGroup2Line;


    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_docName)
    TextView tv_docName;

    @BindView(R.id.tv_docCompany)
    TextView tv_docCompany;

    private int grouptotal1,grouptotal2 ;

    private String token;
    private String userId;
   private List<List<DoctorInfoBean.Result>> result = new ArrayList();
    public static PersonalFragment newInstance(String title) {
        Bundle args = new Bundle();
        PersonalFragment fragment = new PersonalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.ll_about_us, R.id.iv_head, R.id.iv_quite, R.id.rl_group1, R.id.rl_group2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_head:
                IntentUtils.getInstance()
                        .with(getActivity(), ModifyUserInfoActivity.class)
                        .start();
                break;
            case R.id.ll_about_us:
                IntentUtils.getInstance()
                        .with(getActivity(), AboutUstActivity.class)
                        .start();
                break;

            case R.id.iv_quite:
                dialog("确认退出当前账号？");
                break;
            case R.id.rl_group1:
                mTvgroup1.setTextColor(getResources().getColor(R.color.colorPrimary));
                mTvgroup2.setTextColor(getResources().getColor(R.color.text));
                mRlGroup1Line.setVisibility(View.VISIBLE);
                mRlGroup2Line.setVisibility(View.INVISIBLE);
                mLltab.removeAllViews();
                List<String> list1 = new ArrayList<>();
                list1.add("T0首诊");
                list1.add("T3首诊");
                list1.add("T6首诊");
                showData(result.get(0),list1,grouptotal1);
                break;
            case R.id.rl_group2:
                mTvgroup1.setTextColor(getResources().getColor(R.color.text));
                mTvgroup2.setTextColor(getResources().getColor(R.color.colorPrimary));
                mRlGroup1Line.setVisibility(View.INVISIBLE);
                mRlGroup2Line.setVisibility(View.VISIBLE);
                mLltab.removeAllViews();
                List<String> list2 = new ArrayList<>();
                list2.add("T0首诊");
                list2.add("T1首诊");
                list2.add("T2首诊");
                list2.add("T3首诊");
                list2.add("T6首诊");
                showData(result.get(1),list2,grouptotal2);
                break;
            default:
                break;
        }
    }

    @Override
    public void updateUI(String body) {

    }

    @Override
    protected void initInject() {

        getFragmentComponent().inject(this);
    }

    @Override
    protected void initBundle() {

    }


    @Override
    protected int initLayout() {

        return R.layout.fragment_personal;
    }

    @Override
    protected void initView() {
        token  =  SPUtils.getInstance().getString("token");
        userId =   SPUtils.getInstance().getString("userId");
        Log.i("userId",userId);

        //ToastUtil.showShort(userId);
        //ToastUtil.showShort(token);


    }


    @Override
    protected void loadData() {


    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {
        super.initData();

        //

    }

    /**
     * tab动态添加
     */
   private void showData(List<DoctorInfoBean.Result> titleData,List<String> list,int total) {
        for (int i = 0; i < titleData.size(); i++) {
            final DoctorInfoBean.Result pojo = titleData.get(i);
            LinearLayout llWashingRoomItem = new LinearLayout(getActivity());
            llWashingRoomItem.setLayoutParams(new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            llWashingRoomItem = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.tab_personal, null);
            TextView mSubjectName = llWashingRoomItem.findViewById(R.id.tv_subject_name);
            TextView tv_not_pass = llWashingRoomItem.findViewById(R.id.tv_not_pass);
            TextView tv_unFinish = llWashingRoomItem.findViewById(R.id.tv_unFinish);
            TextView tv_finish = llWashingRoomItem.findViewById(R.id.tv_finish);
            TextView tv_rate = llWashingRoomItem.findViewById(R.id.tv_rate);

            BigDecimal b1 = new BigDecimal(Double.toString(pojo.getFinish()));
            BigDecimal b2 = new BigDecimal(Double.toString(total));
            if(total != 0){
                tv_rate.setText(b1.divide(b2, 2, BigDecimal.ROUND_HALF_UP).doubleValue()*100+"%");
            }



            mSubjectName.setText(pojo.getName());
            tv_not_pass.setText(pojo.getUn()+"");
            tv_unFinish.setText(pojo.getUnfinished()+"");
            tv_finish.setText(pojo.getFinish()+"");

            tv_not_pass.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
            tv_not_pass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IntentUtils.getInstance()
                            .with(getActivity(), UnapproveDataActivity.class)
                            .start();
                }
            });

            mLltab.addView(llWashingRoomItem);
        }
    }

    /**
     * 退出弹出框
     */
    public void dialog(String title) {
        new MyDialog(getActivity(), R.style.MyDialo, title, "否", "是",
                new MyDialog.DialogClickListener() {
                    @Override
                    public void onRightBtnClick(Dialog dialog) {
                        dialog.dismiss();
                        IntentUtils.getInstance()
                                .with(getActivity(), LoginActivity.class).start();
                        getActivity().finish();



                    }

                    @Override
                    public void onLeftBtnClick(Dialog dialog) {

                        dialog.dismiss();
                    }
                }).show();
    }

    /**
     * ImmersionBar代理类
     */
    private ImmersionProxy mImmersionProxy = new ImmersionProxy(this);

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mImmersionProxy.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImmersionProxy.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mImmersionProxy.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mImmersionProxy.onResume();
        doctorInfo();
    }

    @Override
    public void onPause() {
        super.onPause();
        mImmersionProxy.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mImmersionProxy.onDestroy();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        mImmersionProxy.onHiddenChanged(hidden);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mImmersionProxy.onConfigurationChanged(newConfig);
    }

    /**
     * 懒加载，在view初始化完成之前执行
     * On lazy after view.
     */
    @Override
    public void onLazyBeforeView() {
    }

    /**
     * 懒加载，在view初始化完成之后执行
     * On lazy before view.
     */
    @Override
    public void onLazyAfterView() {
    }

    /**
     * Fragment用户可见时候调用
     * On visible.
     */
    @Override
    public void onVisible() {
    }

    /**
     * Fragment用户不可见时候调用
     * On invisible.
     */
    @Override
    public void onInvisible() {
    }

    @Override
    public void initImmersionBar() {

    }

    @BindView(R.id.status)
    TextView status;

    @Override
    public boolean immersionBarEnabled() {
        ImmersionBar.with(this)
                .titleBar(status)
                .init();
        return false;
    }


    private void doctorInfo(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("docId", userId);
        MyApp.apiService(ApiService.class)
                .doctorInfo(setParams(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseBean<DoctorInfoBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(BaseBean<DoctorInfoBean> bean) {
                        BaseBean<DoctorInfoBean> baseBean = bean;
                        if(baseBean.getCode() == 0){
                            tv_docName.setText(baseBean.getData().getDoctorInfo().getName());
                            tv_docCompany.setText(baseBean.getData().getDoctorInfo().getCompany());
                            result = bean.getData().getResult();
                            grouptotal1 = bean.getData().getResearchList().get(0).getCount();
                            grouptotal2 = bean.getData().getResearchList().get(1).getCount();
                            mTvgroup1.setText(bean.getData().getResearchList().get(0).getResName()+"("+bean.getData().getResearchList().get(0).getCount()+")");
                            mTvgroup2.setText(bean.getData().getResearchList().get(1).getResName()+"("+bean.getData().getResearchList().get(1).getCount()+")");

                            List<String> list1 = new ArrayList<>();
                            list1.add("T0首诊");
                            list1.add("T3首诊");
                            list1.add("T6首诊");
                            showData(result.get(0),list1,grouptotal1);

                        }else {

                            //ToastUtil.showShort("获取数据失败");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
