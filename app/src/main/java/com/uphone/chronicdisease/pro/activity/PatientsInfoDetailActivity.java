package com.uphone.chronicdisease.pro.activity;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.radish.baselibrary.Intent.IntentUtils;
import com.radish.baselibrary.utils.ToastUtil;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.adapter.SelectPictureAdapter;
import com.uphone.chronicdisease.adapter.holder.SelectPicHelper;
import com.uphone.chronicdisease.base.BaseTakePhotoActivity;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.bean.DictBean;
import com.uphone.chronicdisease.bean.IsUnreadBean;
import com.uphone.chronicdisease.bean.PatientDetailBean;
import com.uphone.chronicdisease.bean.ResearchDataDetailBean;
import com.uphone.chronicdisease.bean.SelectPictureBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.simple.RxSimpleTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.view.MySeekBar;
import com.uphone.chronicdisease.view.SingleOptionsPicker;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.uphone.chronicdisease.bean.SelectPictureBean.TYPE_ADD;
import static com.uphone.chronicdisease.bean.SelectPictureBean.TYPE_PICTURE;
import static com.uphone.chronicdisease.util.CommonUtil.setParams;

/**
 * @ClassName: PatientsInfoDetailActivity
 * @Description: 患者信息详情（T0首诊界面）
 * @Date: 2019/12/26
 * @Author: zx
 */
public class PatientsInfoDetailActivity extends BaseTakePhotoActivity {
    @BindView(R.id.tv_title_name)
    TextView tvTitleName;
    private SelectPicHelper mSelectPicHelper;
    //照片添加
    @BindView(R.id.rv_photos)
    RecyclerView mRvPhones;
    private SelectPictureAdapter mAdapter;

    @BindView(R.id.tv_weight)
    TextView mTvWeight;
    @BindView(R.id.ll_weight)
    LinearLayout mIvWeight;

    @BindView(R.id.tv_grip)
    TextView mTvGrip;
    @BindView(R.id.ll_grip)
    LinearLayout mIvGrip;

    @BindView(R.id.tv_ply)
    TextView mTvPly;
    @BindView(R.id.ll_ply)
    LinearLayout mIvPly;

    @BindView(R.id.tv_rate)
    TextView mTvRate;
    @BindView(R.id.tv_SGA)
    TextView tv_SGA;

    @BindView(R.id.ll_rate)
    LinearLayout mIvRate;

    @BindView(R.id.seekBar_food)
    MySeekBar seekBar_food;
    @BindView(R.id.seekBar_spirit)
    MySeekBar seekBar_spirit;
    @BindView(R.id.seekBar_weak)
    MySeekBar seekBar_weak;
    private int mMaxNums = 9;
    private boolean hasAddType = true;
    private String token;
    private String userId;
    private String dataId;
    private String patId;
    private String title;
    private String tempId;

    @BindView(R.id.ll_images)
    LinearLayout ll_images;

    @BindView(R.id.isMust0)
    TextView isMust0;
    @BindView(R.id.isMust1)
    TextView isMust1;
    @BindView(R.id.isMust2)
    TextView isMust2;

    @BindView(R.id.isMust3)
    TextView isMust3;

    @BindView(R.id.isMust4)
    TextView isMust4;
    @BindView(R.id.isMust5)
    TextView isMust5;
    @BindView(R.id.isMust6)
    TextView isMust6;
    @BindView(R.id.isMust7)
    TextView isMust7;
    @BindView(R.id.isMust8)
    TextView isMust8;

    private DictBean dictBean;

    @Override
    protected void initBundle() {
        isUseImmersionBar = false;
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_patients_info_detail;
    }

    @Override
    protected void initView() {
        //mSelectPicHelper = new SelectPicHelper(this, mRvPhones, 9, SelectPictureAdapter.TYPE_NORMAL, 6);
        token  =  SPUtils.getInstance().getString("token");
        userId =   SPUtils.getInstance().getString("userId");
        dataId = getIntent().getStringExtra("dataId");
        patId = getIntent().getStringExtra("patId");
        title = getIntent().getStringExtra("title");
        tempId = getIntent().getStringExtra("tempId");
        mRvPhones.setLayoutManager(new GridLayoutManager(mActivity, 6));
       /* List<SelectPictureBean> list = new ArrayList();
        list.add(new SelectPictureBean(TYPE_ADD, null));
        mAdapter = new SelectPictureAdapter(list, SelectPictureAdapter.TYPE_NORMAL);
        mRvPhones.setAdapter(mAdapter);*/
        researchDataShow();
    }

    public void onActivityResult(List<LocalMedia> selectList) {
        if((mAdapter.getItemCount() + selectList.size() == mMaxNums + 1)){
            mAdapter.remove(mAdapter.getItemCount() - 1);
            hasAddType = false;

        }
        ArrayList<LocalMedia> medias = (ArrayList<LocalMedia>) selectList;
        for(LocalMedia localMedia: medias){
            String compressPath = localMedia.getCompressPath();
            mAdapter.addData(mAdapter.getItemCount(), new SelectPictureBean(TYPE_PICTURE, compressPath));
        }



    }

    public ArrayList<SelectPictureBean> getData(){
        if(mAdapter == null){
            return new ArrayList<>();
        }
        return (ArrayList<SelectPictureBean>) mAdapter.getData();
    }

    @Override
    protected void initTitle() {
        tvTitleName.setText(title);
    }

    @Override
    protected void initData() {
        getDict();
    }

    private List<DictBean.MiBean> list1 = new ArrayList<>();
    private List<String> list11 = new ArrayList<>();
    private List<DictBean.MiBean> list2 = new ArrayList<>();
    private List<String> list22 = new ArrayList<>();
    private List<DictBean.MiBean> list3 = new ArrayList<>();
    private List<String> list33 = new ArrayList<>();

    //主观指标模板详情
    private void getDict() {
        HashMap<String, String> requestDataMap = new HashMap<>();
        requestDataMap.put("tempId", tempId);
        requestDataMap.put("token", token);
        MyApp.apiService(ApiService.class)
                .getDict(setParams(requestDataMap))
                .compose(new RxSimpleTransformer<DictBean>())
                .subscribe(new SimpleObserver<DictBean>(mActivity) {
                    @Override
                    public void onNext(DictBean bean) {
                        dictBean = bean;
                        for (int i = 0; i < bean.getMi().size(); i++) {
                            if (bean.getMi().get(i).getMiType() == 1) {
                                list1.add(bean.getMi().get(i));
                                list11.add(bean.getMi().get(i).getMiRemark());
                                if(bean.getMi().get(0).getMiShow() == 1){
                                    seekBar_food.setVisibility(View.VISIBLE);
                                }else {
                                    seekBar_food.setVisibility(View.GONE);
                                }
                                if(bean.getMi().get(0).getMiMust() == 1){
                                    isMust0.setVisibility(View.VISIBLE);
                                }else {
                                    isMust0.setVisibility(View.GONE);
                                }
                            } else if (bean.getMi().get(i).getMiType() == 2) {
                                list2.add(bean.getMi().get(i));
                                list22.add(bean.getMi().get(i).getMiRemark());
                                if(bean.getMi().get(1).getMiShow() == 1){
                                    seekBar_spirit.setVisibility(View.VISIBLE);
                                }else {
                                    seekBar_spirit.setVisibility(View.GONE);
                                }

                                if(bean.getMi().get(1).getMiMust() == 1){
                                    isMust1.setVisibility(View.VISIBLE);
                                }else {
                                    isMust1.setVisibility(View.GONE);
                                }
                            } else if (bean.getMi().get(i).getMiType() == 3) {
                                list3.add(bean.getMi().get(i));
                                list33.add(bean.getMi().get(i).getMiRemark());
                                if(bean.getMi().get(2).getMiShow() == 1){
                                    seekBar_weak.setVisibility(View.VISIBLE);
                                }else {
                                    seekBar_weak.setVisibility(View.GONE);
                                }

                                if(bean.getMi().get(2).getMiMust() == 1){
                                    isMust2.setVisibility(View.VISIBLE);
                                }else {
                                    isMust2.setVisibility(View.GONE);
                                }
                            }
                        }
                        seekBar_food.setData(list11);
                        seekBar_spirit.setData(list22);
                        seekBar_weak.setData(list33);

                        for (int i = 0; i < bean.getAi().size(); i++) {
                            if(bean.getAi().get(i).getAiType() == 1){
                                if((bean.getAi().get(0).getAiShow() == 1)){
                                    mIvWeight.setVisibility(View.VISIBLE);
                                }else {
                                    mIvWeight.setVisibility(View.INVISIBLE);
                                }

                                if(bean.getAi().get(0).getAiMust() == 1){
                                    isMust4.setVisibility(View.VISIBLE);
                                }else {
                                    isMust4.setVisibility(View.INVISIBLE);
                                }

                            }

                            if(bean.getAi().get(i).getAiType() == 2){
                                if((bean.getAi().get(1).getAiShow() == 1)){
                                    mIvGrip.setVisibility(View.VISIBLE);
                                }else {
                                    mIvGrip.setVisibility(View.INVISIBLE);
                                }
                                if(bean.getAi().get(1).getAiMust() == 1){
                                    isMust5.setVisibility(View.VISIBLE);
                                }else {
                                    isMust5.setVisibility(View.INVISIBLE);
                                }

                            }

                            if(bean.getAi().get(i).getAiType() == 3){
                                if((bean.getAi().get(2).getAiShow() == 1)){
                                    mIvPly.setVisibility(View.VISIBLE);
                                }else {
                                    mIvPly.setVisibility(View.INVISIBLE);
                                }
                                if(bean.getAi().get(2).getAiMust() == 1){
                                    isMust6.setVisibility(View.VISIBLE);
                                }else {
                                    isMust6.setVisibility(View.INVISIBLE);
                                }
                            }
                            if(bean.getAi().get(i).getAiType() == 4){
                                if((bean.getAi().get(3).getAiShow() == 1)){
                                    mIvRate.setVisibility(View.VISIBLE);
                                }else {
                                    mIvRate.setVisibility(View.INVISIBLE);
                                }
                                if(bean.getAi().get(3).getAiMust() == 1){
                                    isMust7.setVisibility(View.VISIBLE);
                                }else {
                                    isMust7.setVisibility(View.INVISIBLE);
                                }
                            }


                            if(bean.getAi().get(i).getAiType() == 5){
                                if((bean.getAi().get(4).getAiShow() == 1)){
                                    tv_SGA.setVisibility(View.VISIBLE);
                                }else {
                                    tv_SGA.setVisibility(View.INVISIBLE);
                                }
                                if(bean.getAi().get(4).getAiMust() == 1){
                                    isMust8.setVisibility(View.VISIBLE);
                                }else {
                                    isMust8.setVisibility(View.INVISIBLE);
                                }
                            }




                        }


                        for (int i = 0; i < bean.getOi().size(); i++) {
                            if((bean.getOi().get(i).getOiType() == 1)){
                                if((bean.getOi().get(0).getOiShow() == 1)){
                                    ll_images.setVisibility(View.VISIBLE);
                                }else {
                                    ll_images.setVisibility(View.GONE);
                                }

                                if(bean.getOi().get(0).getOiMust() == 1){
                                    isMust3.setVisibility(View.VISIBLE);
                                }else {
                                    isMust3.setVisibility(View.GONE);
                                }
                            }

                        }




                    }
                });
    }

    @Override
    protected void initListener() {

    }


    @OnClick({R.id.iv_back, R.id.tv_SGA, R.id.ll_weight, R.id.ll_grip, R.id.ll_ply, R.id.ll_rate, R.id.tv_del})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_SGA:
               /* IntentUtils.getInstance()
                        .with(context, SGAscoreActivity.class)
                        .start();*/
                Intent intent = new Intent(context,SGAscoreActivity.class);
                startActivityForResult(intent,111);
                break;
            case R.id.ll_weight:
                List<String> listWeight = new ArrayList<>();
                listWeight.add("60kg");
                listWeight.add("70kg");
                SingleOptionsPicker.openOptionsPicker(this, listWeight, 2, mTvWeight);
                break;

            case R.id.ll_grip:
                List<String> listgrip = new ArrayList<>();
                listgrip.add("60kg");
                listgrip.add("50kg");
                SingleOptionsPicker.openOptionsPicker(this, listgrip, 2, mTvGrip);
                break;
            case R.id.ll_ply:
                List<String> listPly = new ArrayList<>();
                listPly.add("60cm");
                listPly.add("50cm");
                SingleOptionsPicker.openOptionsPicker(this, listPly, 2, mTvPly);
                break;
            case R.id.ll_rate:
                List<String> listRate = new ArrayList<>();
                listRate.add("60%");
                listRate.add("50%");
                SingleOptionsPicker.openOptionsPicker(this, listRate, 2, mTvRate);
                break;
            case R.id.tv_del:
                if(TextUtils.isEmpty(mTvWeight.getText().toString())){
                    ToastUtil.showShort("请选择体重！");
                    return;
                }
                if(TextUtils.isEmpty(mTvGrip.getText().toString())){
                    ToastUtil.showShort("请选择握力！");
                    return;
                }
                for (int i = 0; i < dictBean.getOi().size(); i++) {
                    if((dictBean.getOi().get(i).getOiType() == 1)){
                        if(dictBean.getOi().get(0).getOiMust() == 0){
                            ToastUtil.showShort("请选择图片！");
                            return;
                        }
                    }
                }

                for (int i = 0; i < dictBean.getAi().size(); i++) {
                    if(dictBean.getAi().get(i).getAiType() == 1){
                        if(dictBean.getAi().get(0).getAiMust() == 0){
                            ToastUtil.showShort("请选择体重！");
                            return;
                        }

                    }

                    if(dictBean.getAi().get(i).getAiType() == 2){
                        if(dictBean.getAi().get(1).getAiMust() == 0){
                            ToastUtil.showShort("请选择握力！");
                            return;
                        }
                    }

                    if(dictBean.getAi().get(i).getAiType() == 3){
                        if(dictBean.getAi().get(2).getAiMust() == 0){
                            ToastUtil.showShort("请选择肱三头肌皮褶厚度！");
                            return;
                        }
                    }
                    if(dictBean.getAi().get(i).getAiType() == 4){
                        if(dictBean.getAi().get(3).getAiMust() == 0){
                            ToastUtil.showShort("请选择体脂率！");
                            return;
                        }
                    }
                    if(dictBean.getAi().get(i).getAiType() == 5){
                        if(dictBean.getAi().get(4).getAiMust() == 0){
                            ToastUtil.showShort("请选择SGA评分！");
                            return;
                        }
                    }


                }
                researchData();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    if((mAdapter.getItemCount() +getSelectList().size() == mMaxNums + 1)){
                        mAdapter.remove(mAdapter.getItemCount() - 1);
                        hasAddType = false;

                    }
                    ArrayList<LocalMedia> medias = (ArrayList<LocalMedia>) getSelectList();
                    for(LocalMedia localMedia: medias){
                        String compressPath = localMedia.getCompressPath();
                        mAdapter.addData(mAdapter.getItemCount(), new SelectPictureBean(TYPE_PICTURE, compressPath));
                    }

                    uploadImg();
                    break;
                case 111:
                    String total = data.getStringExtra("total");
                    tv_SGA.setText("SGA评分（"+total+")");
                    break;

            }
        }
    }

    private List<String> upUrl = new ArrayList<>();

    // 上传图片
    private void uploadImg() {
        upUrl.clear();
        for (int i = 1; i < mAdapter.getData().size(); i++) {
            SelectPictureBean item = mAdapter.getData().get(i);
            File file = new File(item.getPicture());
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("file", file.getName(), requestBody);
            builder.addFormDataPart("token",  SPUtils.getInstance().getString("token"));
            MultipartBody body = builder.build();
            MyApp.apiService(ApiService.class)
                    .uploadImages(body)
                    .compose(new RxSimpleTransformer<String>())
                    .subscribe(new SimpleObserver<String>(mActivity) {
                        @Override
                        public void onNext(String result) {
                            String images = result;
                            Log.e("addison", images);
                            upUrl.add(images);
                        }
                    });
        }
    }


    // 上传调研数据
    private void researchData() {
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("dataId", dataId);//dataId:调研数据ID
        map.put("miEat", seekBar_food.getProgress()+"");// miEat:食欲指标分数
        map.put("miSpirit", seekBar_spirit.getProgress()+"");//miSpirit:精神指标分数
        map.put("miWeak", seekBar_weak.getProgress()+"");//miWeak:乏力指标分数
        map.put("aiWeight", mTvWeight.getText().toString());//aiWeight:体重
        map.put("aiGrip", mTvGrip.getText().toString());// aiGrip:握力
        map.put("aiSkinfoldThicknessOfTricepsBrachii", mTvPly.getText().toString());//aiSkinfoldThicknessOfTricepsBrachii:肱三头肌皮褶厚度
        map.put("aiBodyFatPercentage", mTvRate.getText().toString());// aiBodyFatPercentage:体脂率
        map.put("aiSga", "28");//aiSga:改良SGA评分
        map.put("aiDPcare", "");// aiDPcare:风险评估
        for(int i=0;i<upUrl.size();i++){
            map.put("dataImg"+(i+1), upUrl.get(i));// dataImg1:图1（以此类推，图2 dataImg2）
        }
        //map.put("dataImg1Time", "");// dataImg1Time:图1上传时间（以此类推，图2dataImg2Time）
        MyApp.apiService(ApiService.class)
                .researchData(setParams(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(BaseBean bean) {
                        BaseBean baseBean = bean;
                        if(baseBean.getCode() == 0){
                           ToastUtils.showShort("保存成功！");
                        }else {
                            ToastUtils.showShort(baseBean.getMsg());
                        }


                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


    private void researchDataShow() {
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("patId", patId);
        map.put("dataId", dataId);
        MyApp.apiService(ApiService.class)
                .researchDataDetail(setParams(map))
                .compose(new RxSimpleTransformer<>())
                .subscribe(new SimpleObserver<ResearchDataDetailBean>() {
                    @Override
                    public void onNext(ResearchDataDetailBean bean) {
                       /* seekBar_food.setProgress(bean.getMiEat());
                        seekBar_spirit.setProgress(bean.getMiSpirit());
                        seekBar_weak.setProgress(bean.getMiWeak());*/

                        mTvWeight.setText(bean.getAiWeight()+"");
                        mTvGrip.setText(bean.getAiGrip()+"");
                        mTvPly.setText(bean.getAiSkinfoldThicknessOfTricepsBrachii()+"");
                        mTvRate.setText(bean.getAiBodyFatPercentage()+"");
                        tv_SGA.setText("SGA评分 ("+bean.getAiSga()+")");
                        List<String> listImage = new ArrayList<>();
                        listImage.add(bean.getDataImg1());
                        List<SelectPictureBean> list = new ArrayList();
                        list.add(new SelectPictureBean(TYPE_ADD, null));
                        if(bean.getDataImg1() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg1()));
                        }
                        if(bean.getDataImg2() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg2()));
                        }
                        if(bean.getDataImg3() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg3()));
                        }
                        if(bean.getDataImg4() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg4()));
                        }
                        if(bean.getDataImg5() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg5()));
                        }
                        if(bean.getDataImg6() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg6()));
                        }

                        if(bean.getDataImg7() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg7()));
                        }
                        if(bean.getDataImg8() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg8()));
                        }
                        if(bean.getDataImg9() != null){
                            list.add(new SelectPictureBean(TYPE_PICTURE, bean.getDataImg9()));
                        }
                        mAdapter = new SelectPictureAdapter(list, SelectPictureAdapter.TYPE_NORMAL);
                        mRvPhones.setAdapter(mAdapter);
                        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
                            @Override
                            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                                switch (view.getId()) {
                                    case R.id.iv_add_img:
                                        int maxCount = Math.max(mMaxNums + 1 - mAdapter.getItemCount(), 1);
                                        getPhoto(false, maxCount);
                                        break;
                                    case R.id.iv_delete:
                                        mAdapter.remove(position);

                       /* if (!hasAddType) { // 说明目前没有添加图标
                            mAdapter.addData(mMaxNums - 1, new SelectPictureBean(TYPE_ADD, null));
                            hasAddType = true;
                        }*/
                                        break;
                                }
                            }
                        });

                    }
                });
    }


}
