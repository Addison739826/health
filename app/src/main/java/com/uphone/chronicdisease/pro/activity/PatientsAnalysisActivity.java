package com.uphone.chronicdisease.pro.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.adapter.IndexAdapter;
import com.uphone.chronicdisease.base.BaseGActivity;
import com.uphone.chronicdisease.bean.PatientDataBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.simple.RxSimpleTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.util.chart.XDataFormatter;
import com.uphone.chronicdisease.view.dialog.DataReportDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

public class PatientsAnalysisActivity extends BaseGActivity {


    @BindView(R.id.tv_title_name)
    TextView tvTitleName;
    @BindView(R.id.tv_analysis)
    TextView tvAnalysis;
    @BindView(R.id.rv_index)
    RecyclerView rvIndex;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.rv_select)
    RecyclerView rvSelect;
    @BindView(R.id.bar_char)
    BarChart barChart;
    @BindView(R.id.line_char)
    LineChart lineChart;


    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_group)
    TextView tv_group;
    @BindView(R.id.tv_number)
    TextView tv_number;
    @BindView(R.id.tv_sex)
    TextView tv_sex;
    @BindView(R.id.tv_age)
    TextView tv_age;
    @BindView(R.id.tv_diagnosis)
    TextView tv_diagnosis;
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.ll_chart)
    LinearLayout llChart;
    private String token;
    private String userId;
    private String patId;
    private String title;
    private List<PatientDataBean.DataBean> listIndex = new ArrayList<>();
    private IndexAdapter adapter;
    /**
     * x轴坐标数，现在默认为所有指标数
     */
    private int selectNum = 0;
    private final int[] colors = new int[]{
            ColorTemplate.VORDIPLOM_COLORS[0],
            ColorTemplate.VORDIPLOM_COLORS[1],
            ColorTemplate.VORDIPLOM_COLORS[2],
            ColorTemplate.VORDIPLOM_COLORS[3],
            ColorTemplate.VORDIPLOM_COLORS[4],

    };

    @Override
    protected void initBundle() {
        isUseImmersionBar = false;
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_patients_analysis;
    }

    @Override
    protected void initView() {
        token = SPUtils.getInstance().getString("token");
        userId = SPUtils.getInstance().getString("userId");
        patId = getIntent().getStringExtra("patId");
        title = getIntent().getStringExtra("title");
    }

    @Override
    protected void initTitle() {
        tvTitleName.setText(title+"患者数据图");
    }

    @Override
    protected void initData() {
        initBar();
        initLine();
        researchData();
    }

    protected Typeface tfLight;

    private void initBar() {
        tfLight = Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf");
        barChart.getDescription().setEnabled(false);
//        barChart.setDrawBorders(true);
        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);
        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);
        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
        l.setTypeface(tfLight);
        l.setYOffset(0f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setTypeface(tfLight);
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new XDataFormatter(listIndex));

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        xAxis.setDrawGridLines(false);//不绘制格网线
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        barChart.getAxisRight().setEnabled(false);
        changeBarData();
    }

    private void initLine() {
        lineChart.setDrawGridBackground(false);
        lineChart.getDescription().setEnabled(false);
        lineChart.setDrawBorders(false);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setTypeface(tfLight);
        // xAxis.setGranularity(1f);
        //  xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        // xAxis.setValueFormatter(new XDataFormatter(listIndex));


        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        xAxis.setDrawGridLines(false);//不绘制格网线
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        lineChart.getAxisRight().setEnabled(false);
        // enable touch gestures
        lineChart.setTouchEnabled(true);

        // enable scaling and dragging
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(false);

    }

    private void changeLineData() {
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        //x坐标为阶段，几个指标几条折线
        for (int i = 0; i < listIndex.size(); i++) {
            ArrayList<Entry> values = new ArrayList<>();
            //判断是否被选中
            if (listIndex.get(i).isChecked()) {
                float y = 0f;
                //选中的指标需要加colum个点
                for (int j = 0; j < colum; j++) {
                    int index = (i + 1) * (colum + 1) + j + 1;
                    String[] split = data.get(index).getDdValue().split("/");
                    if (split.length == 0) {
                        y = 0;
                    } else {
                        try {
                            y = Float.parseFloat(split[0]);
                        } catch (NumberFormatException e) {
                            y = 0;
                        }
                    }
                    values.add(new Entry(j, y));
                }
                LineDataSet d = new LineDataSet(values, listIndex.get(i).getDdValue());
                d.setLineWidth(2.5f);
                d.setCircleRadius(4f);
                int color = Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
                d.setColor(color);
                d.setCircleColor(color);
                dataSets.add(d);
            }
        }
        lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(legendList));
        lineChart.getXAxis().setAxisMinimum(0f);
        lineChart.getXAxis().setGranularity(1);

        //lineChart.getXAxis().setAxisMaximum(colum);
        LineData data = new LineData(dataSets);
        lineChart.setData(data);
        lineChart.invalidate();
    }

    private List<Integer> colorList = new ArrayList<>();

    private void changeBarData() {
        barChart.getXAxis().setValueFormatter(new XDataFormatter(listIndex));
        List<ArrayList<BarEntry>> valuesList = new ArrayList<>();
        List<BarDataSet> setList = new ArrayList<>();
        BarDataSet[] setArray = new BarDataSet[colum];
        for (int j = 0; j < colum; j++) {
            ArrayList<BarEntry> values = new ArrayList<>();
            for (int i = 0; i < listIndex.size(); i++) {
                if (listIndex.get(i).isChecked()) {
                    float y = 0f;
                    String[] split = data.get((i + 1) * (colum + 1) + j + 1).getDdValue().split("/");
                    if (split.length == 0) {
                        y = 0;
                    } else {
                        try {
                            y = Float.parseFloat(split[0]);
                        } catch (NumberFormatException e) {
                            y = 0;
                        }
                    }
                    values.add(new BarEntry(11 + i, y));
                }
            }
            valuesList.add(values);

        }
        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            for (int i = 0; i < colum; i++) {
                BarDataSet set = (BarDataSet) barChart.getData().getDataSetByIndex(i);
                set.setValues(valuesList.get(i));
                barChart.getData().notifyDataChanged();
                barChart.notifyDataSetChanged();
            }
        } else {
            for (int i = 0; i < colum; i++) {
                BarDataSet set = new BarDataSet(valuesList.get(i), legendList.get(i));
                colorList.add(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255)));
                set.setColor(colorList.get(i));
                setList.add(set);
                setArray[i] = set;
            }
            BarData data = new BarData(setArray);
            data.setValueFormatter(new LargeValueFormatter());
            data.setValueTypeface(tfLight);
            barChart.setData(data);
        }
        float groupSpace = 0.08f;
        float barSpace = 0.03f;
        float barWidth = (1 - groupSpace) / colum - barSpace;
        // specify the width each bar should have
        barChart.getBarData().setBarWidth(barWidth);
        barChart.getXAxis().setValueFormatter(new XDataFormatter(listIndex));
        // restrict the x-axis range
        barChart.getXAxis().setAxisMinimum(0);
        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        barChart.getXAxis().setAxisMaximum(barChart.getBarData().getGroupWidth(groupSpace, barSpace) * selectNum + 0);
        if (colum != 0) {
            barChart.groupBars(0, groupSpace, barSpace);
        }
        barChart.invalidate();
    }

    @Override
    protected void initListener() {

    }

    boolean isSelectAll = false;

    @OnClick({R.id.iv_back, R.id.btn_cancel, R.id.btn_confirm, R.id.tv_analysis, R.id.tv_select_all, R.id.btn_email})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_analysis:
                drawerLayout.openDrawer(Gravity.END);
                break;
            case R.id.btn_cancel:
                drawerLayout.closeDrawers();
                break;
            case R.id.btn_confirm:
                drawerLayout.closeDrawers();
                break;
            case R.id.tv_select_all:
                isSelectAll = !isSelectAll;
                for (int i = 0; i < listIndex.size(); i++) {
                    listIndex.get(i).setChecked(isSelectAll);
                    adapter.notifyDataSetChanged();
                }
                if (isSelectAll) {
                    selectNum = listIndex.size();
                } else {
                    selectNum = 0;
                }
                changeBarData();
                changeLineData();
                break;
            case R.id.btn_email:
                //Glide.with(this).load(createImage(llChart)).into(iv);
                uploadFIle(bitmapToFile(createImage(llChart)));

                break;
        }
    }

    /**
     * 上传图片
     *
     * @param file
     */


    private void uploadFIle(File file) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("file", file.getName(), requestBody);
        builder.addFormDataPart("token", token);
        MultipartBody body = builder.build();
        MyApp.apiService(ApiService.class)
                .uploadImages(body)
                .compose(new RxSimpleTransformer<String>())
                .subscribe(new SimpleObserver<String>(mActivity) {
                    @Override
                    public void onNext(String result) {


                        Log.e("addison",result);
                        DataReportDialog dataReportDialog = new DataReportDialog();
                        Bundle bundle = new Bundle();
                        bundle.putString("url", result);
                        dataReportDialog.setArguments(bundle);
                        dataReportDialog.show(getFragmentManager(), "dataReportDialog");
                    }
                });
    }

    /**
     * view 转bitmap
     *
     * @param v
     * @return
     */
    private Bitmap createImage(View v) {
        int w = v.getWidth();
        int h = v.getHeight();
        Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        c.drawColor(Color.WHITE);
        /** 如果不设置canvas画布为白色，则生成透明 */
        v.layout(0, 0, w, h);
        v.draw(c);
        return bmp;
    }


    /**
     * bitmap转图片
     * 压缩图片（质量压缩）
     *
     * @param bitmap
     */
    public static File bitmapToFile(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (baos.toByteArray().length / 1024 > 500) {  //循环判断如果压缩后图片是否大于500kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            options -= 10;//每次都减少10
            bitmap.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            long length = baos.toByteArray().length;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        String filename = format.format(date);
        File file = new File(Environment.getExternalStorageDirectory(), filename + ".png");
        try {
            FileOutputStream fos = new FileOutputStream(file);
            try {
                fos.write(baos.toByteArray());
                fos.flush();
                fos.close();
            } catch (IOException e) {

                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        recycleBitmap(bitmap);
        return file;
    }


    /**
     * 释放bitmap
     *
     * @param bitmaps
     */
    public static void recycleBitmap(Bitmap... bitmaps) {
        if (bitmaps == null) {
            return;
        }
        for (Bitmap bm : bitmaps) {
            if (null != bm && !bm.isRecycled()) {
                bm.recycle();
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    private IndexAdapter indexAdapter;
    List<PatientDataBean.DataBean> data = new ArrayList<>();
    /**
     * 列表有几列数据,不带行参数
     */
    private int colum = 0;
    private List<String> legendList = new ArrayList<>();

    // 患者数据
    private void researchData() {
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("pid", patId);//dataId:调研数据ID
        MyApp.apiService(ApiService.class)
                .patientData(setParams(map))
                .compose(new RxSimpleTransformer<>())
                .subscribe(new SimpleObserver<PatientDataBean>(mActivity) {

                    @Override
                    public void onNext(PatientDataBean bean) {
                        colum = bean.getStep().size();
                        legendList.addAll(bean.getStep());
                        tv_name.setText(bean.getPatient().getPatName());
                        if ("开同组".equals(bean.getResearch())) {
                            tv_group.setBackgroundResource(R.drawable.btn_shape_orange);
                        } else {
                            tv_group.setBackgroundResource(R.drawable.btn_shape_green);
                        }
                        tv_group.setText(bean.getResearch());
                        tv_number.setText(bean.getPatient().getProjId() + "");
                        tv_sex.setText(bean.getPatient().getSex() == 0 ? "女" : "男");
                        tv_age.setText(bean.getPatient().getAge() + "岁");
                        tv_diagnosis.setText(bean.getPatient().getDiagnosis());

                        data.add(new PatientDataBean.DataBean("阶段\\指标"));
                        //需要横向的数据，返回竖向的数据，人才啊,最后一列还能是空。
                        for (int i = 0, len = bean.getStep().size(); i < len; i++) {
                            data.add(new PatientDataBean.DataBean(bean.getStep().get(i)));
                        }
                        if (null != bean.getData() && 0 != bean.getData().size()) {
                            for (int i = 0; i < bean.getData().get(0).size(); i++) {
                                if (null != bean.getData().get(0).get(i)) {
                                    data.add(new PatientDataBean.DataBean(bean.getData().get(0).get(i).getName()));
                                    for (int j = 0; j < bean.getStep().size(); j++) {
                                        if (null != bean.getData() && null != bean.getData().get(j) && i < bean.getData().get(j).size()) {
                                            data.add(bean.getData().get(j).get(i));
                                        } else {
                                            data.add(new PatientDataBean.DataBean(""));
                                        }
                                    }
                                } else {
                                    data.add(new PatientDataBean.DataBean(""));
                                }
                            }
                        }
                        indexAdapter = new IndexAdapter(data);
                        // bean.getStep()可能为null
                        rvIndex.setLayoutManager(new GridLayoutManager(mActivity, bean.getStep().size() + 1));
                        rvIndex.setAdapter(indexAdapter);


                        for (int i = bean.getStep().size() + 1; i < data.size(); i++) {
                            if (i % (bean.getStep().size() + 1) == 0) {
                                listIndex.add(data.get(i));
                            }
                        }

                        adapter = new IndexAdapter(true, listIndex);
                        rvSelect.setLayoutManager(new LinearLayoutManager(mActivity));
                        rvSelect.setAdapter(adapter);
                        selectNum = listIndex.size();
                        selectIndex();
                        changeBarData();
                        changeLineData();
                    }
                });
    }

    private void selectIndex() {
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                listIndex.get(position).setChecked(!listIndex.get(position).isChecked());
                adapter.notifyDataSetChanged();
                selectNum = 0;
                List<PatientDataBean.DataBean> selectData = new ArrayList<>();
                selectData.addAll(data);
                for (int i = listIndex.size() - 1; i > -1; i--) {
                    if (listIndex.get(i).isChecked()) {
                        selectNum++;
                    } else {
                        for (int j = 0; j < colum + 1; j++) {

                            selectData.remove((i + 1) * (colum + 1));
                        }
                    }
                }
                indexAdapter.setNewData(selectData);
                changeBarData();
                changeLineData();
            }
        });
    }


}
