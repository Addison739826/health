package com.uphone.chronicdisease.pro.activity.regist;

import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.BaseGActivity;

/**
 * @ClassName: RegistActivity
 * @Description: 注册
 * @Date: 2019/12/23
 * @Author: zx
 */
public class RegistActivity  extends BaseGActivity {
    @Override
    protected void initBundle() {

    }

    @Override
    protected int initLayout() {
        return R.layout.activity_regist;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initTitle() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}
