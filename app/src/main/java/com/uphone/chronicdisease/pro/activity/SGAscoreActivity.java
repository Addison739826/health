package com.uphone.chronicdisease.pro.activity;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.radish.baselibrary.utils.ToastUtil;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.BaseGActivity;
import com.uphone.chronicdisease.view.MySeekBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @ClassName: PatientsInfoDetailActivity
 * @Description: SGA评分（T0首诊）
 * @Date: 2019/12/26
 * @Author: zx
 */
public class SGAscoreActivity extends BaseGActivity {
    @BindView(R.id.tv_title_name)
    TextView tvTitleName;
    @BindView(R.id.seekBar1)
    MySeekBar seekBar1;
    @BindView(R.id.seekBar2)
    MySeekBar seekBar2;
    @BindView(R.id.seekBar3)
    MySeekBar seekBar3;
    @BindView(R.id.seekBar4)
    MySeekBar seekBar4;
    @BindView(R.id.seekBar5)
    MySeekBar seekBar5;
    @BindView(R.id.seekBar6)
    MySeekBar seekBar6;
    @BindView(R.id.seekBar7)
    MySeekBar seekBar7;
    private int total;

    private List<String> list1 = new ArrayList<>();
    private List<String> list2 = new ArrayList<>();
    private List<String> list3 = new ArrayList<>();
    private List<String> list4 = new ArrayList<>();
    private List<String> list5 = new ArrayList<>();
    private List<String> list6 = new ArrayList<>();
    private List<String> list7 = new ArrayList<>();

    @Override
    protected void initBundle() {
        isUseImmersionBar = false;
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_sga;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initTitle() {
        tvTitleName.setText("改良SGA评分");
    }

    @Override
    protected void initData() {
        for(int i =0;i<7;i++){
            list1.add("0分");
            list1.add("1分：无变化");
            list1.add("2分：<5%");
            list1.add("3分：5-10%");
            list1.add("4分：10-15%");
            list1.add("5分：>15%");

            list2.add("0分");
            list2.add("1分：无变化");
            list2.add("2分：需要对饮食进行调整，基本还可摄入固态食物");
            list2.add("3分：全流质，或总量明显减少");
            list2.add("4分：紧能摄入低热量液体");
            list2.add("5分：无法进食");

            list3.add("0分");
            list3.add("1分：无变化");
            list3.add("2分：恶心");
            list3.add("3分：呕吐或中度肠道反映");
            list3.add("4分：腹泻");
            list3.add("5分：重度厌食");

            list4.add("0分");
            list4.add("1分：无损害或有改善");
            list4.add("2分：行走受限");
            list4.add("3分：正常活动受限");
            list4.add("4分：仅能进行轻微活动");
            list4.add("5分：卧床");

            list5.add("0分");
            list5.add("1分：MDH<12,其他情况良好");
            list5.add("2分：MDH=1-2年，有轻微合并症");
            list5.add("3分：MDH=2-4年，或年龄>75,或有中度合并症");
            list5.add("4分：MDH>4年或有重度合并症");
            list5.add("5分：非常严重的多器官合并症");


            list6.add("0分");
            list6.add("1分：无");
            list6.add("2分：轻度");
            list6.add("3分：中度");
            list6.add("4分：中重度");
            list6.add("5分：重度");

        }

        seekBar1.setData(list1);
        seekBar2.setData(list2);
        seekBar3.setData(list3);
        seekBar4.setData(list4);
        seekBar5.setData(list5);
        seekBar6.setData(list6);
        seekBar7.setData(list6);
    }

    @Override
    protected void initListener() {

    }

    @OnClick({R.id.iv_back,R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_save:
                total = seekBar1.getProgress()+seekBar2.getProgress()+seekBar3.getProgress()+seekBar4.getProgress()+seekBar5.getProgress()+seekBar6.getProgress()+seekBar7.getProgress();
                Intent intent =new Intent();
                intent.putExtra("total",total+"");
                setResult(RESULT_OK,intent);
                finish();
                break;

        }
    }
}
