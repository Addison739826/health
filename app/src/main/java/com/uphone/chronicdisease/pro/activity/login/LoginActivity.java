package com.uphone.chronicdisease.pro.activity.login;


import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.blankj.utilcode.util.SPUtils;
import com.radish.baselibrary.Intent.IntentUtils;
import com.uphone.chronicdisease.MainActivity;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.mvp.BaseMvpActivity;
import com.uphone.chronicdisease.bean.LoginBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.simple.RxSimpleTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.ResponseBody;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

/**
 * Created by hzy on 2019/1/18
 * LoginActivity  登录界面
 *
 * @author zx
 */
public class LoginActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.View {
    @BindView(R.id.smsCodeBt)
    Button smsCodeBt;// 发送验证码
    @BindView(R.id.et_code)
    EditText smsCodeEt;//输入验证码
    @BindView(R.id.et_phone)
    EditText mEdtPhone;

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @OnClick({R.id.bt_login, R.id.smsCodeBt})
    public void onViewClicked(View view) {
        String phone = mEdtPhone.getText().toString();
        String code = smsCodeEt.getText().toString();
        switch (view.getId()) {
            case R.id.bt_login:
                //mPresenter.postLogin("15703379121", "123456");
               /*if(TextUtils.isEmpty(phone)){
                    Toast.makeText(context,"请输入手机号码！",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(code)){
                    Toast.makeText(context,"请输入验证码！",Toast.LENGTH_SHORT).show();
                    return;
                }*/

               //TODO 校验短信验证码是否正确
                getLogin(phone,code);
                /*IntentUtils.getInstance()
                        .with(context, MainActivity.class).start();
                finish();
*/
                break;

            case R.id.smsCodeBt:
                if(TextUtils.isEmpty(phone)){
                    Toast.makeText(context,"请输入手机号码！",Toast.LENGTH_SHORT).show();
                    return;
                }
                //ToastUtils.textToast(mActivity,"验证码已发送至"+phone);
                //TODO 获取短信验证码接口
                mPresenter.getCode(phone, smsCodeBt);
                //Toast.makeText(context,"验证码已发送至"+phone,Toast.LENGTH_SHORT).show();
               /*new CountDown().getTime(smsCodeBt,
                        CountDown.COUNTTIME_ADDCARD);*/
                break;
            default:
                break;
        }
    }

    private void getLogin(String mobile,String code) {
        HashMap<String, String> requestDataMap = new HashMap<>();
        requestDataMap.put("mobile", mobile);
        requestDataMap.put("code", code);

        MyApp.apiService(ApiService.class)
                .login(setParams(requestDataMap))
                .compose(new RxSimpleTransformer())
                .subscribe(new SimpleObserver<LoginBean>(mActivity) {
                    @Override
                    public void onNext(LoginBean bean) {

                            SPUtils.getInstance().put("token", bean.getToken());
                            SPUtils.getInstance().put("userId", bean.getId()+"");//docId:医生ID 也是userId 也是uid
                        IntentUtils.getInstance()
                                .with(context, MainActivity.class).start();
                        finish();


                    }
                });
    }


    @Override
    public void updateView(ResponseBody responseBean, int status) {
        if (status == 1) {
            IntentUtils.getInstance()
                    .with(this, MainActivity.class)
                    .start();
            finish();
        }
    }

    @Override
    protected void initBundle() {

    }

    @Override
    protected int initLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initTitle() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }



}
