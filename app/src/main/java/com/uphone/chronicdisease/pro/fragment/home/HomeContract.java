package com.uphone.chronicdisease.pro.fragment.home;

import com.uphone.chronicdisease.base.mvp.BasePresenter;
import com.uphone.chronicdisease.base.mvp.BaseView;

/**  * <pre>  *     author : radish  *     e-mail : 15703379121@163.com  *     time   : 2019/4/16  *     desc   :  * </pre>  */
public interface HomeContract {
    // update UI
    interface View extends BaseView {
        void updateUI(String body);
    }

    // 连接 数据
    interface Presenter extends BasePresenter<View> {
        void loadNetData();
    }
}
