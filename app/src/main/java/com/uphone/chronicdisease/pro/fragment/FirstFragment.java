package com.uphone.chronicdisease.pro.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.radish.baselibrary.Intent.IntentUtils;
import com.radish.baselibrary.base.BaseLazyFragment;
import com.radish.baselibrary.utils.ToastUtil;
import com.uphone.chronicdisease.MainActivity;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.BaseGLFragment;
import com.uphone.chronicdisease.bean.CdkProjectBean;
import com.uphone.chronicdisease.bean.ProjListBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.simple.RxSimpleTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.pro.activity.project.CdkProjectDetailActivity;
import com.uphone.chronicdisease.pro.fragment.home.HomeFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

/**
 * @ClassName: FirstFragment
 * @Description: 首页
 * @Date: 2019/12/23
 * @Author: zx
 */
public class FirstFragment extends BaseGLFragment {

    private String title = "";
    @BindView(R.id.ll_ckd)
    LinearLayout mLlCkd;
    @BindView(R.id.ll_diabetes)//糖尿病
    LinearLayout ll_diabetes;

    @BindView(R.id.ll_Hypertension)//高血压
    LinearLayout ll_Hypertension;
    @BindView(R.id.ll_Osteoporosis)//骨质疏松
    LinearLayout ll_Osteoporosis;
    @BindView(R.id.ll_rehabilitation)//胸痛康复
    LinearLayout ll_rehabilitation;
    @BindView(R.id.ll_Stroke)//卒中康复
    LinearLayout ll_Stroke;
    @BindView(R.id.ll_hepatitis)//慢性肝炎
    LinearLayout ll_hepatitis;


    @BindView(R.id.tv_diabetes)//慢性肝炎字
    TextView tv_diabetes;
    @BindView(R.id.tv_diabetes_pro)//慢性肝炎字
    TextView tv_diabetes_pro;

    @BindView(R.id.tv_Hypertension)//慢性肝炎字
            TextView tv_Hypertension;
    @BindView(R.id.tv_Hypertension_pro)//慢性肝炎字
            TextView tv_Hypertension_pro;





    @BindView(R.id.tv_project0)
    TextView tv_project0;

    private String project0;
    private String project1;
    public static FirstFragment newInstance(String title) {
        Bundle args = new Bundle();
        FirstFragment fragment = new FirstFragment();
        fragment.title = title;
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.ll_ckd,R.id.ll_diabetes,R.id.ll_Hypertension})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_ckd:
                //mPresenter.postLogin("15703379121", "123456");
                break;
            case R.id.ll_diabetes:
                ToastUtil.showShort("抱歉，您目前无权限参与该项目！");
                break;
            case R.id.ll_Hypertension:
                ToastUtil.showShort("抱歉，您目前无权限参与该项目！");
                break;
            default:
                break;
        }
    }



    @Override
    protected void initBundle() {

    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_first;
    }

    @Override
    protected void initView() {
        mLlCkd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SPUtils.getInstance().put("projId", project0);
                IntentUtils.getInstance()
                        .with(getActivity(), CdkProjectDetailActivity.class)
                        .start();
            }
        });

    }

    @Override
    protected void loadData() {
        projList();
    }

    @Override
    protected void initListener() {

    }


    private void projList(){
        Map<String, String> map = new HashMap<>();
        map.put("token",SPUtils.getInstance().getString("token"));
        MyApp.apiService(ApiService.class)
                .projList(setParams(map))
                .compose(new RxSimpleTransformer<>())
                .subscribe(new SimpleObserver<ArrayList<ProjListBean>>(getActivity()){
                    @Override
                    public void onNext(ArrayList<ProjListBean> list) {

                        if(list.size() == 1){
                            tv_project0.setText(list.get(0).getProjName());
                            project0 = list.get(0).getId()+"";
                            ll_diabetes.setVisibility(View.INVISIBLE);
                            ll_Hypertension.setVisibility(View.INVISIBLE);
                            ll_Osteoporosis.setVisibility(View.INVISIBLE);
                            ll_rehabilitation.setVisibility(View.INVISIBLE);
                            ll_Stroke.setVisibility(View.INVISIBLE);
                            ll_hepatitis.setVisibility(View.INVISIBLE);

                        }
                        if(list.size() == 2){
                            tv_project0.setText(list.get(0).getProjName());
                            project0 = list.get(0).getId()+"";
                            tv_diabetes.setText(list.get(1).getProjName());

                            ll_diabetes.setVisibility(View.VISIBLE);
                            ll_Hypertension.setVisibility(View.INVISIBLE);
                            ll_Osteoporosis.setVisibility(View.INVISIBLE);
                            ll_rehabilitation.setVisibility(View.INVISIBLE);
                            ll_Stroke.setVisibility(View.INVISIBLE);
                            ll_hepatitis.setVisibility(View.INVISIBLE);
                        }
                        if(list.size() == 3){
                            tv_project0.setText(list.get(0).getProjName());
                            project0 = list.get(0).getId()+"";
                            tv_diabetes.setText(list.get(1).getProjName());
                            tv_Hypertension.setText(list.get(2).getProjName());

                            ll_diabetes.setVisibility(View.VISIBLE);
                            ll_Hypertension.setVisibility(View.VISIBLE);
                            ll_Osteoporosis.setVisibility(View.INVISIBLE);
                            ll_rehabilitation.setVisibility(View.INVISIBLE);
                            ll_Stroke.setVisibility(View.INVISIBLE);
                            ll_hepatitis.setVisibility(View.INVISIBLE);
                        }
                        if(list.size() == 4){
                            tv_project0.setText(list.get(0).getProjName());
                            project0 = list.get(0).getId()+"";

                            ll_diabetes.setVisibility(View.VISIBLE);
                            ll_Hypertension.setVisibility(View.VISIBLE);
                            ll_Osteoporosis.setVisibility(View.VISIBLE);
                            ll_rehabilitation.setVisibility(View.INVISIBLE);
                            ll_Stroke.setVisibility(View.INVISIBLE);
                            ll_hepatitis.setVisibility(View.INVISIBLE);
                        }
                        if(list.size() == 5){
                            tv_project0.setText(list.get(0).getProjName());
                            project0 = list.get(0).getId()+"";

                            ll_diabetes.setVisibility(View.VISIBLE);
                            ll_Hypertension.setVisibility(View.VISIBLE);
                            ll_Osteoporosis.setVisibility(View.VISIBLE);
                            ll_rehabilitation.setVisibility(View.VISIBLE);
                            ll_Stroke.setVisibility(View.INVISIBLE);
                            ll_hepatitis.setVisibility(View.INVISIBLE);
                        }

                        if(list.size() == 6){
                            tv_project0.setText(list.get(0).getProjName());
                            project0 = list.get(0).getId()+"";

                            ll_diabetes.setVisibility(View.VISIBLE);
                            ll_Hypertension.setVisibility(View.VISIBLE);
                            ll_Osteoporosis.setVisibility(View.VISIBLE);
                            ll_rehabilitation.setVisibility(View.VISIBLE);
                            ll_Stroke.setVisibility(View.VISIBLE);
                            ll_hepatitis.setVisibility(View.INVISIBLE);
                        }
                        if(list.size() == 7){
                            tv_project0.setText(list.get(0).getProjName());
                            project0 = list.get(0).getId()+"";

                            ll_diabetes.setVisibility(View.VISIBLE);
                            ll_Hypertension.setVisibility(View.VISIBLE);
                            ll_Osteoporosis.setVisibility(View.VISIBLE);
                            ll_rehabilitation.setVisibility(View.VISIBLE);
                            ll_Stroke.setVisibility(View.VISIBLE);
                            ll_hepatitis.setVisibility(View.VISIBLE);
                        }


                    }
                });
    }
}
