package com.uphone.chronicdisease.pro.activity.project;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.radish.baselibrary.Intent.IntentUtils;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.adapter.CdkProjectDetailAdapter;
import com.uphone.chronicdisease.adapter.SingleTextAdapter;
import com.uphone.chronicdisease.base.mvp.BaseMvpActivity;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.bean.CdkProjectBean;
import com.uphone.chronicdisease.bean.CheckBean;
import com.uphone.chronicdisease.bean.Patientlist;
import com.uphone.chronicdisease.bean.ResearchConditionBean;
import com.uphone.chronicdisease.http.ApiService;
import com.uphone.chronicdisease.http.simple.RxListPageTransformer;
import com.uphone.chronicdisease.http.simple.RxSimpleTransformer;
import com.uphone.chronicdisease.http.simple.SimpleObserver;
import com.uphone.chronicdisease.pro.activity.AddPatientsInfoActivity;
import com.uphone.chronicdisease.pro.activity.PatientsInfoActivity;
import com.uphone.chronicdisease.view.dialog.DataReportCKDDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;
import static com.uphone.chronicdisease.util.CommonUtil.setParams1;

/**
 * Created by zx on 2019/12/22
 * CdkProjectDetailActivity  CKD患者管理
 *
 * @author zx
 */

public class CdkProjectDetailActivity extends BaseMvpActivity<CdkProjectDetailePresenter> implements CdkProjectDetailContract.View {

    @BindView(R.id.rv_patient)
    RecyclerView mRvPatient;
    @BindView(R.id.tv_patient)
    TextView mTvPatient;
    @BindView(R.id.tv_introduce)
    TextView mTvIntroduce;

    @BindView(R.id.ll_patient)
    LinearLayout mLlPatient;
    @BindView(R.id.ll_introduce)
    LinearLayout mLLIntroduce;
    @BindView(R.id.tv_title_name)
    TextView mTvTitleName;

    @BindView(R.id.btn_select_data)
    Button mBtnSelectData;


    @BindView(R.id.ll_search)
    LinearLayout mLlSearch;

    @BindView(R.id.rl_choose)
    RelativeLayout mRlChoose;
    @BindView(R.id.ll_back)
    LinearLayout mLlBack;
    @BindView(R.id.btn_choose)
    Button mBtnChoose;

    @BindView(R.id.ll_screen)
    LinearLayout mTvScreen;
    @BindView(R.id.rv_team)
    RecyclerView rvTeam;
    @BindView(R.id.rv_step)
    RecyclerView rvStep;
    @BindView(R.id.rv_result)
    RecyclerView rvResult;

    @BindView(R.id.tv_introduce_txt)
    TextView tv_introduce_txt;
    @BindView(R.id.btn_sure)
    Button btn_sure;
    @BindView(R.id.btn_reset)
    Button btn_reset;

    @BindView(R.id.edt_keyword)
    EditText edt_keyword;
    @BindView(R.id.iv_search)
    ImageView iv_search;

    private String[] data = new String[]{""};
    private boolean isManager;
    private boolean isCheckAll;
    private CdkProjectDetailAdapter mAdapter;
    private List<CdkProjectBean> list = new ArrayList<>();
    private CdkProjectBean mData;
    private DrawerLayout mDrawerLayout = null;
    private SingleTextAdapter teamAdapter, stepAdapter, resultAdapter;
    private List<CheckBean> teamList = new ArrayList<>();
    private List<CheckBean> stepList = new ArrayList<>();
    private List<CheckBean> resultList = new ArrayList<>();
    private List<Patientlist> mBeanChoose = new ArrayList();
    private List<String> teamListId ;
    private List<String>  stepListId;
    private List<String>  resultListId;
    private String ids1 = "";
    private String ids2 = "";
    private String ids3 = "";
    private String projId;
    private String token;
    private String userId;
    @OnClick({R.id.tv_patient, R.id.tv_introduce, R.id.btn_select_data, R.id.ll_back, R.id.btn_choose, R.id.ll_screen, R.id.ll_add_info, R.id.iv_back,R.id.btn_sure,R.id.btn_reset,R.id.iv_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_patient:
                mTvPatient.setTextColor((getResources().getColor(R.color.colorPrimary)));
                mTvIntroduce.setTextColor((getResources().getColor(R.color.text)));
                mLlPatient.setVisibility(View.VISIBLE);
                mLLIntroduce.setVisibility(View.GONE);
                break;
            case R.id.tv_introduce:
                mTvPatient.setTextColor((getResources().getColor(R.color.text)));
                mTvIntroduce.setTextColor((getResources().getColor(R.color.colorPrimary)));
                mLlPatient.setVisibility(View.GONE);
                mLLIntroduce.setVisibility(View.VISIBLE);
                getProj();
                break;
            case R.id.btn_select_data:
                mAdapter.showSwitch(true);
                mLlSearch.setVisibility(View.GONE);
                mRlChoose.setVisibility(View.VISIBLE);

                for (int i = 0; i < mAdapter.getData().size(); i++) {
                    Patientlist item =  (mAdapter.getData().get(i));
                    if (item.isChoosed) {
                        mBeanChoose.add(item);
                    } else {
                        mBeanChoose.remove((mAdapter.getData().get(i)));
                    }
                }
                if ("生成数据报表".equals(mBtnSelectData.getText().toString())) {
                    if (mBeanChoose.size() == 0 || mBeanChoose == null) {
                        ToastUtils.showShort("请选择数据！");
                        return;
                    }
                }

                if (isManager) {
                    String ids = "";

                    for (int i = 0;i<mBeanChoose.size();i++){
                        ids = mBeanChoose.get(i).getPatient().getId()+",";
                    }
                    DataReportCKDDialog dataReportDialog = new DataReportCKDDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("ids", ids);
                    bundle.putString("url", "");
                    dataReportDialog.setArguments(bundle);
                    dataReportDialog.show(getFragmentManager(), "dataReportDialog");
                }
                isManager = true;
                mBtnSelectData.setText("生成数据报表");
                break;
            case R.id.ll_back:
                isManager = false;
                mAdapter.showSwitch(false);
                mLlSearch.setVisibility(View.VISIBLE);
                mRlChoose.setVisibility(View.GONE);
                mBtnSelectData.setText("选择数据");
                break;
            case R.id.btn_choose:
                isCheckAll = !isCheckAll;
                mBtnChoose.setText(isCheckAll ? "取消全选" : "全选");
                if (isCheckAll) {
                    for (int i = 0; i < mAdapter.getData().size(); i++) {
                        (mAdapter.getData().get(i)).setChoosed(false);
                        mAdapter.changeSwitch(i);
                        mBeanChoose.add(mAdapter.getData().get(i));
                    }

                } else {
                    for (int i = 0; i < mAdapter.getData().size(); i++) {
                        (mAdapter.getData().get(i)).setChoosed(true);
                        mAdapter.changeSwitch(i);
                        mBeanChoose.remove(mAdapter.getData().get(i));
                    }
                }
                break;

            case R.id.ll_screen://筛选标签
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                break;
            case R.id.ll_add_info://添加患者信息
                IntentUtils.getInstance()
                        .with(context, AddPatientsInfoActivity.class)
                        .start();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_sure:
                teamListId = new ArrayList<>();
                stepListId = new ArrayList<>();
                resultListId = new ArrayList<>();
                ids1 = "";
                ids2 = "";
                ids3 = "";
                    for(int i=0;i<teamList.size();i++){
                        if(teamList.get(i).isChecked()){
                            teamListId.add(String.valueOf(teamList.get(i).getId()));
                        }else {
                            teamListId.remove(String.valueOf(teamList.get(i).getId()));
                        }
                    }
                    if(teamListId.size() == 0){
                        ids1 = "";
                    }else {
                        for(int i=0;i<teamListId.size();i++){
                            ids1+=teamListId.get(i)+",";
                        }
                    }

               for(int i=0;i<stepList.size();i++){
                    if(stepList.get(i).isChecked()){
                        stepListId.add(stepList.get(i).getId()+"");
                    }else {
                        stepListId.remove(stepList.get(i).getId()+"");
                    }

                }

              if(stepListId.size() == 0){
                  ids2 = "";
              }else {
                  for(int i=0;i<stepListId.size();i++){
                      ids2+=stepListId.get(i)+",";
                  }

              }

                for(int i=0;i<resultList.size();i++){
                    if(resultList.get(i).isChecked()){
                        resultListId.add(resultList.get(i).getId()+"");

                    }else {
                        resultListId.remove(resultList.get(i).getId()+"");
                    }
                }
                if(resultListId.size() == 0){
                    ids3 = "";
                }else {
                    for(int i=0;i<resultListId.size();i++){
                        ids3+=resultListId.get(i)+",";
                    }
                }


                getPatients(ids1,ids2,ids3);
                mDrawerLayout.closeDrawers();
                break;
            case R.id.btn_reset:
                mDrawerLayout.closeDrawers();
                break;
            case  R.id.iv_search:
                getPatients(ids1,ids2,ids3);
                break;

            default:
                break;
        }
    }

    @Override
    public void updateView(ResponseBody responseBean, int status) {

    }

    @Override
    protected void initInject() {

    }

    @Override
    protected void initBundle() {
        isUseImmersionBar = false;
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_ckd_project_details;
    }

    @Override
    protected void initView() {
        projId =  SPUtils.getInstance().getString("projId");
        token  =  SPUtils.getInstance().getString("token");
        userId =   SPUtils.getInstance().getString("userId");
        mLlPatient.setVisibility(View.VISIBLE);
        mLLIntroduce.setVisibility(View.GONE);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            /**
             * 当抽屉滑动状态改变的时候被调用
             * 状态值是STATE_IDLE（闲置--0）, STATE_DRAGGING（拖拽的--1）, STATE_SETTLING（固定--2）中之一。
             * 抽屉打开的时候，点击抽屉，drawer的状态就会变成STATE_DRAGGING，然后变成STATE_IDLE
             */
            @Override
            public void onDrawerStateChanged(int arg0) {

            }

            /**
             * 当抽屉被滑动的时候调用此方法
             * arg1 表示 滑动的幅度（0-1）
             */
            @Override
            public void onDrawerSlide(@NonNull View arg0, float arg1) {

            }

            /**
             * 当一个抽屉被完全打开的时候被调用
             */
            @Override
            public void onDrawerOpened(@NonNull View arg0) {

            }

            /**
             * 当一个抽屉完全关闭的时候调用此方法
             */
            @Override
            public void onDrawerClosed(@NonNull View arg0) {

            }
        });

        researchCondition();
    }

    @Override
    protected void initTitle() {
        mTvTitleName.setText("CKD患者管理");
    }

    @Override
    protected void initData() {
        list = new ArrayList<>();

//        teamList.add(new CheckBean("开同组"));
//        teamList.add(new CheckBean("联合用药组"));
//
//        stepList.add(new CheckBean("TO"));
//        stepList.add(new CheckBean("T1"));
//        stepList.add(new CheckBean("T2"));
//        stepList.add(new CheckBean("T3"));
//        stepList.add(new CheckBean("T4"));
//        stepList.add(new CheckBean("T5"));
//        stepList.add(new CheckBean("T6"));
//        stepList.add(new CheckBean("已完成"));
//
//        resultList.add(new CheckBean("肾炎"));
//        resultList.add(new CheckBean("肾衰竭"));



        mAdapter = new CdkProjectDetailAdapter(mActivity);
        mRvPatient.setLayoutManager(new LinearLayoutManager(mActivity));
        mRvPatient.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                pageNum += 1;
                getPatients(ids1,ids2,ids3);
            }
        }, mRvPatient);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Patientlist bean = mAdapter.getData().get(position);
                if (isManager) {
                    mAdapter.changeSwitch(position);

                } else {

                    IntentUtils.getInstance()
                            .with(context, PatientsInfoActivity.class)
                            .putString("patId", bean.getPatient().getId()+"")//把列表patId传过去
                            .start();
                }

            }
        });
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    /*case R.id.iv_select://筛选
                        mAdapter.changeSwitch(position);

                        break;*/
                    default:
                        break;
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        getPatients("","","");
    }

    private void getPatients(String ids1, String ids2, String ids3) {
        Map<String, String> map = new HashMap<>();
        map.put("token",token);
        map.put("docId", userId);
        map.put("page", String.valueOf(pageNum));
        map.put("limit", String.valueOf(pageSize));
        if(!"".equals(edt_keyword.getText().toString())){
            map.put("keyword", edt_keyword.getText().toString());
        }
        if(!TextUtils.isEmpty(ids1)){
            map.put("resId", ids1);
        }
        if(!TextUtils.isEmpty(ids2)){
            map.put("stepId", ids2);
        }
        if(!TextUtils.isEmpty(ids3)){
            map.put("dig", ids3);
        }

        //Map<String, RequestBody> requestBodyMap = setParams(map);
        MyApp.apiService(ApiService.class)
                .getPatients(setParams(map))
                .compose(new RxListPageTransformer(mAdapter, pageNum))
                .subscribe(new SimpleObserver(mActivity));
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    //http://122.14.213.160:7081/jeefast-rest/api/proj/text?token=admin&projId=11
    private void getProj() {
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("projId", projId);
        MyApp.apiService(ApiService.class)
                .projText(setParams(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(BaseBean bean) {
                        BaseBean baseBean = bean;
                        if (baseBean.getCode() == 0) {
                            //ToastUtils.showShort("text");
                            tv_introduce_txt.setText(baseBean.getData().toString());
                        }


                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }



    /**
     * 患者筛选（搜索条件）
     * http://122.14.213.160:7081/jeefast-rest/api/researchCondition?token=admin&resId=1
     */
    private void researchCondition() {
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        MyApp.apiService(ApiService.class)
                .researchCondition(setParams(map))
                .compose(new RxSimpleTransformer<>())
                .subscribe(new SimpleObserver<ResearchConditionBean>() {
                    @Override
                    public void onNext(ResearchConditionBean bean) {

                       for(int i = 0;i<bean.getRes().size();i++){
                           teamList.add(new CheckBean(bean.getRes().get(i).getResName(),bean.getRes().get(i).getResId()));
                       }
                        for(int i = 0;i<bean.getStep().size();i++){
                            stepList.add(new CheckBean(bean.getStep().get(i).getStepName(),bean.getStep().get(i).getStepId()));
                        }
                        for(int i = 0;i<bean.getDia().size();i++){
                            resultList.add(new CheckBean(bean.getDia().get(i).getDiagnosis()));
                        }

                        teamAdapter = new SingleTextAdapter(R.layout.item_single_text_gray_bg, teamList);
                        stepAdapter = new SingleTextAdapter(R.layout.item_single_text_gray_bg_small, stepList);
                        resultAdapter = new SingleTextAdapter(R.layout.item_single_text_gray_bg, resultList);
                        rvTeam.setLayoutManager(new GridLayoutManager(mActivity, 2));
                        rvStep.setLayoutManager(new GridLayoutManager(mActivity, 4));
                        rvResult.setLayoutManager(new GridLayoutManager(mActivity, 2));

                        rvTeam.setAdapter(teamAdapter);
                        rvStep.setAdapter(stepAdapter);
                        rvResult.setAdapter(resultAdapter);
                        teamAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                                teamList.get(position).setChecked(!teamList.get(position).isChecked());
                                teamAdapter.notifyDataSetChanged();
                            }
                        });
                        stepAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                                stepList.get(position).setChecked(!stepList.get(position).isChecked());
                                stepAdapter.notifyDataSetChanged();
                            }
                        });
                        resultAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                                resultList.get(position).setChecked(!resultList.get(position).isChecked());
                                resultAdapter.notifyDataSetChanged();
                            }
                        });

                    }
                });
    }


}
