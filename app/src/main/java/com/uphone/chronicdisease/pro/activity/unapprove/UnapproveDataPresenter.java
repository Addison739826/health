package com.uphone.chronicdisease.pro.activity.unapprove;

import com.uphone.chronicdisease.base.mvp.BasePAV;
import com.uphone.chronicdisease.pro.activity.project.CdkProjectDetailContract;

import javax.inject.Inject;

/**
 * Created by DELL on 2019/12/22.
 */

public class UnapproveDataPresenter extends BasePAV<UnapproveDataContract.View> implements UnapproveDataContract.Presenter {

    @Inject
    UnapproveDataPresenter() {

    }

    @Override
    public void postUnapprove(String username, String password) {

    }
}
