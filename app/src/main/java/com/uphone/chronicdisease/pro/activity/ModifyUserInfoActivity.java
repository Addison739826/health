package com.uphone.chronicdisease.pro.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.uphone.chronicdisease.MyApp;
import com.uphone.chronicdisease.R;
import com.uphone.chronicdisease.base.BaseGActivity;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.http.ApiService;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.uphone.chronicdisease.util.CommonUtil.setParams;

public class ModifyUserInfoActivity extends BaseGActivity {

    @BindView(R.id.tv_title_name)
    TextView tvTitleName;

    @BindView(R.id.edt_name)
    EditText edt_name;
    @BindView(R.id.edt_company)
    EditText edt_company;
    private String projId;
    private String token;
    private String userId;

    @Override
    protected void initBundle() {
        isUseImmersionBar = false;
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_modify_user_info;
    }

    @Override
    protected void initView() {
        projId =  SPUtils.getInstance().getString("projId");
        token  =  SPUtils.getInstance().getString("token");
        userId =   SPUtils.getInstance().getString("userId");
    }

    @Override
    protected void initTitle() {
      tvTitleName.setText("修改个人信息");
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.iv_back, R.id.btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn:
                userUpdate();
                break;
        }
    }


    //http://122.14.213.160:7081/jeefast-rest/api/user/update?username=1&company=2&token=admin&userId=1
    private void userUpdate(){
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("userId", userId);
        map.put("username", edt_name.getText().toString());
        map.put("company", edt_company.getText().toString());
        MyApp.apiService(ApiService.class)
                .userUpdate(setParams(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(BaseBean bean) {
                        BaseBean baseBean = bean;
                        if(baseBean.getCode() == 0){
                            ToastUtils.showShort("更新成功");
                        }else {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
