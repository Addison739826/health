package com.uphone.chronicdisease.http.simple;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

import static com.uphone.chronicdisease.MyApp.getContext;


public class SimpleObserver<T> implements Observer<T> {

    public static final String TAG = "DataObserver";

    public Activity mActivity;

    private boolean mIsSave;


    public SimpleObserver(Activity activity, boolean isSave) {
        mActivity = activity;
        mIsSave = isSave;
    }

    public SimpleObserver(Activity activity) {
        mActivity = activity;
    }


    @Override
    public void onSubscribe(Disposable d) {
        if (mActivity != null) {
//            CProgressDialogUtils.showProgressDialog(mActivity);
            if (mActivity.isFinishing()) {
                d.dispose();
            }
        }
    }

    public SimpleObserver() {
    }

    @Override
    public void onNext(T t) {

    }

    @Override
    public void onError(Throwable e) {

        // 这里根据e 的不同类型来进行不同处理
        if (e instanceof ApiException) { // 是从 onNext()的code != 1 过来的


            if ("请重新登录，您的账号已在其它地方登录！".equals(e.getMessage())) {
                //token失效，跳转登录,提示语不对，或者说未登录的情况不应该是token失效
            } else {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        } else if (e instanceof HttpException && ((HttpException) e).code() == 504) {  // 如果504，现在认为是没有网络了


//                    Toast.makeText(MeiduoApp.getContext(), "联网失败,请稍后再试", Toast.LENGTH_SHORT).show();
            Toast.makeText(getContext(), "网络请求失败，请检查手机网络状态", Toast.LENGTH_SHORT).show();

        } else if (e instanceof HttpException) { // 非504的其他错误，都是错误


//                    Toast.makeText(MeiduoApp.getContext(), "服务器错误", Toast.LENGTH_SHORT).show();
            Toast.makeText(getContext(), "抱歉，网络开小差了，请稍后重试", Toast.LENGTH_SHORT).show();
            Log.e("Addison", e.getMessage() + "");
        } else {

//                Toast.makeText(MeiduoApp.getContext(), "联网失败!"+e.getMessage(), Toast.LENGTH_SHORT).show();
            Toast.makeText(getContext(), "抱歉，网络开小差了，请稍后重试", Toast.LENGTH_SHORT).show();
            Log.e("Addison", e.getMessage() + "");
        }

        // dismissDialog
        //CProgressDialogUtils.cancelProgressDialog();
    }

    @Override
    public void onComplete() {
        //CProgressDialogUtils.cancelProgressDialog();
        Log.e(TAG, "onComplete");
    }
}
