package com.uphone.chronicdisease.http.simple;

import android.app.Activity;
import android.util.Log;


import com.uphone.chronicdisease.view.MyDialog;


public class ApiException extends RuntimeException {

    public static final int TOKEN_FAILURE = 401;
//    public static final int WRONG_PASSWORD = 101;
    private int mCode;
    private static MyDialog mTokenDialog;

    public ApiException(String message) {
        super(message);
    }

    public ApiException(int code, String message) {
        super(message);
        this.mCode = code;
    }

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        this.mCode = code;
    }

    // 在 Observer 中走了 onError 的时候，在这里处理
    public boolean onRxError(Activity context){
        if(getCode() == TOKEN_FAILURE){
            Log.e("TAG", "TOKEN_FAILURE----");
            if(context != null){
             //   tokenFailure(context);
                return true;
            }
        }else{
            //AppUtils.showToast(getMessage());  // 目前和ios沟通的是，code不为1，只toast
        }
        return false;
    }



}