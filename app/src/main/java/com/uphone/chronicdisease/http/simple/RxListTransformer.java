package com.uphone.chronicdisease.http.simple;

import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.uphone.chronicdisease.bean.BaseBean;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * @describe 列表（无分页）的处理
 */
public class RxListTransformer<T> implements ObservableTransformer<BaseBean<ArrayList<T>>, ArrayList<T>> {


    private BaseQuickAdapter mAdapter;


    public RxListTransformer() {
    }
    public RxListTransformer(BaseQuickAdapter adapter) {
        mAdapter = adapter;
    }



    @Override
    public ObservableSource<ArrayList<T>> apply(Observable<BaseBean<ArrayList<T>>> upstream) {
        return upstream
                .doOnDispose(new Action() {
                    @Override
                    public void run() throws Exception {
                        Log.e("RxListTransformer", "dispose 掉了 --- ");
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<BaseBean<ArrayList<T>>, ArrayList<T>>() {
                    @Override
                    public ArrayList<T> apply(BaseBean<ArrayList<T>> baseBean) throws Exception {
                        if (baseBean.getCode() == 1) {
                            ArrayList<T> list = baseBean.getData();
                            if (mAdapter != null) {
                                mAdapter.setNewData(list);
                            }

                            if (list == null) {
                                list = new ArrayList<>();
                            }
                            return list;
                        } else {
                            throw new ApiException(baseBean.getCode(), baseBean.getMsg());
                        }
                    }
                });

    }
}
