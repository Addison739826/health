package com.uphone.chronicdisease.http.simple;




import com.uphone.chronicdisease.bean.BaseBean;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class RxSimpleTransformer<T> implements ObservableTransformer<BaseBean<T>, T> {

    private T mObj;

    public RxSimpleTransformer() {
    }

    public RxSimpleTransformer(T obj) {
        this.mObj = obj;
    }

    @Override
    public ObservableSource<T> apply(Observable<BaseBean<T>> upstream) {

        return upstream
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception { }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<BaseBean<T>, T>() {
                    @Override
                    public T apply(BaseBean<T> baseBean) throws Exception {
                        if(baseBean.getCode() == 0){
                            if(baseBean.getData() == null){
                                return mObj;
                            }else{
                                return baseBean.getData();
                            }

                        }else{
                            throw new ApiException(baseBean.getMsg());
                        }
                    }
                })
              ;
    }
}
