package com.uphone.chronicdisease.http.simple;

import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.bean.PagesBean;


import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class RxListPageTransformer<T> implements ObservableTransformer<BaseBean<PagesBean<T>>, PagesBean<T>> {

    private int mPageNum;
    private BaseQuickAdapter mAdapter;


    public RxListPageTransformer(BaseQuickAdapter adapter, int pageNum) {
        mPageNum = pageNum;
        mAdapter = adapter;
    }



    @Override
    public ObservableSource<PagesBean<T>> apply(Observable<BaseBean<PagesBean<T>>> upstream) {
        return upstream
                .doOnDispose(new Action() {
                    @Override
                    public void run() throws Exception {
                        Log.e("RxPageTransformer", "dispose 掉了 --- ");
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<BaseBean<PagesBean<T>>, PagesBean<T>>() {
                    @Override
                    public PagesBean<T> apply(BaseBean<PagesBean<T>> baseBean) throws Exception {
                        if(baseBean.getCode() == 0){
                            PagesBean data = baseBean.getData();
                            if(mPageNum == 1){
                                mAdapter.setNewData(data.getDataList());
                            }else{
                                mAdapter.addData(data.getDataList());
                            }
                            if(mAdapter.getData().size() < data.getCount()){
                                mAdapter.loadMoreComplete();
                            }else{
                                mAdapter.loadMoreEnd();
                            }
                            return data;
                        }else{
                            throw new ApiException(baseBean.getCode(), baseBean.getMsg());
                        }
                    }
                });
    }
}

