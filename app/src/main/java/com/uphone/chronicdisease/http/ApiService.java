package com.uphone.chronicdisease.http;

import com.uphone.chronicdisease.bean.AuditFailedBean;
import com.uphone.chronicdisease.bean.BaseBean;
import com.uphone.chronicdisease.bean.CdkProjectBean;
import com.uphone.chronicdisease.bean.CheckBean;
import com.uphone.chronicdisease.bean.DialysisWayBean;
import com.uphone.chronicdisease.bean.DictBean;
import com.uphone.chronicdisease.bean.DoctorInfoBean;
import com.uphone.chronicdisease.bean.IsUnreadBean;
import com.uphone.chronicdisease.bean.LoginBean;
import com.uphone.chronicdisease.bean.MessageBean;
import com.uphone.chronicdisease.bean.PagesBean;
import com.uphone.chronicdisease.bean.PatientDataBean;
import com.uphone.chronicdisease.bean.PatientDetailBean;
import com.uphone.chronicdisease.bean.Patientlist;
import com.uphone.chronicdisease.bean.ProjListBean;
import com.uphone.chronicdisease.bean.ResearchBean;
import com.uphone.chronicdisease.bean.ResearchConditionBean;
import com.uphone.chronicdisease.bean.ResearchDataDetailBean;
import com.uphone.chronicdisease.bean.TreatmentPlanBean;
import com.uphone.chronicdisease.bean.resBsaseInfoBean;
import com.uphone.chronicdisease.bean.researchStrpBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

/**
 * Created by hzy on 2019/1/10
 * ApiService
 *
 * @author Administrator
 */
public interface ApiService {

    // 服务器url
    public final static String ServiceUrl = "http://122.14.213.160:7081/jeefast-rest/api/";
    public final static String PicUrl = "http://uphonechina.com/jeefast-rest/api/";

    //获取验证码
    public final static String getcode = ServiceUrl + "code/getCode";
    //消息列表
    //http://122.14.213.160:7081/jeefast-rest/api/message/list?token=9b735053-a67c-4ec4-91be-288f8325e26c&userId=3&page=1&limit=10
    public final static String getMessage = ServiceUrl + "message/list";


    /*
     * 登录
     * http://122.14.213.160:7081/jeefast-rest/api/login?
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("login")
    Observable<BaseBean<LoginBean>> login(@PartMap Map<String, RequestBody> requestBodyMap);

    @Headers(ServiceUrl)
    @Multipart
    @POST("message/list")
    Observable<BaseBean<PagesBean<MessageBean>>> getMessage(@PartMap Map<String, RequestBody> requestBodyMap);


    @Multipart
    @POST("user/highScoreUpload")
    Observable<ResponseBody> userHightScoreUpload(@PartMap Map<String, RequestBody> params,
                                                  @Part MultipartBody.Part file);
    @POST()
    Observable<ResponseBody> postService(@Url String url, @Body RequestBody params);

    @Multipart
    @POST()
    Observable<ResponseBody> postService(@Url String url, @PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 研究组列表
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("research/list")
    Observable<BaseBean<ArrayList<ResearchBean>>> research(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 透析方式列表
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("dialysisWay/list")
    Observable<BaseBean<ArrayList<DialysisWayBean>>> dialysisWay(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 治疗方案列表
     * http://122.14.213.160:7081/jeefast-rest/api/treatmentPlan/list?token=admin&projId=1
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("treatmentPlan/list")
    Observable<BaseBean<ArrayList<TreatmentPlanBean>>> treatmentPlan(@PartMap Map<String, RequestBody> requestBodyMap);

    /**
     * 获取患者列表
     *http://122.14.213.160:7081/jeefast-rest/api/patient/list?docId=1&page=1&limit=10&token=admin&keyword=""&resId=""&stepId=""&dig=""
     * @param requestBodyMap
     * @return
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("patient/list")
    Observable<BaseBean<PagesBean<Patientlist>>> getPatients(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 获取项目介绍(code 500)
     * http://122.14.213.160:7081/jeefast-rest/api/proj/text?projId=11&token=admin
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("proj/text")
    Observable<BaseBean> projText(@PartMap Map<String, RequestBody> requestBodyMap);

    /**
     * 上传图片 单张
     * */
    @Headers(ServiceUrl)

    @POST("upload")
    Observable<BaseBean<String>> uploadImages(@Body MultipartBody imgs);

    /*
     * 创建患者 （信息有问题 code500）
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("patient/add")
    Observable<BaseBean> patientAdd(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 更新用户信息 （code 500）
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("user/update")
    Observable<BaseBean> userUpdate(@PartMap Map<String, RequestBody> requestBodyMap);


    /*
     * 获取研究阶段（根据研究组ID）
     * http://122.14.213.160:7081/jeefast-rest/api/researchStrp/list?resId=1&token=0904e01d-7675-420c-9e54-151fca56d96e
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("researchStrp/list")
    Observable<BaseBean<ArrayList<researchStrpBean>>> researchStrp(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 全部已读操作
     * http://122.14.213.160:7081/jeefast-rest/api/message/readAll?userId=3&token=9b735053-a67c-4ec4-91be-288f8325e26c
     * http://122.14.213.160:7081/jeefast-rest/api/doctor/info?docId=1&token=admin
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("message/readAll")
    Observable<BaseBean> readAll(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 医生信息
     * http://122.14.213.160:7081/jeefast-rest/api/doctor/info?docId=3&token=9b735053-a67c-4ec4-91be-288f8325e26c
     *
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("doctor/info")
    Observable<BaseBean<DoctorInfoBean>> doctorInfo(@PartMap Map<String, RequestBody> requestBodyMap);

    /**
     * 是否有未读消息
     *http://122.14.213.160:7081/jeefast-rest/api/message/unRead?userId=3&token=9b735053-a67c-4ec4-91be-288f8325e26c
     * @param requestBodyMap
     * @return
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("message/unRead")
    Observable<BaseBean<IsUnreadBean>> isUnRead(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 首页项目数据(只取得第一个字段，具体怎么用？)
     *
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("proj/list")
    Observable<BaseBean<ArrayList<ProjListBean>>> projList(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 上传调研数据(选择图片的界面，但是图片是单张？)
     *
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("researchData/update")
    Observable<BaseBean> researchData(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 患者数据分析（有曲线图的界面）
     *http://122.14.213.160:7081/jeefast-rest/api/patient/data?pid=1&token=admin
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("patient/data")
    Observable<BaseBean<PatientDataBean>> patientData(@PartMap Map<String, RequestBody> requestBodyMap);

    /*
     * 患者信息导出到邮箱（有曲线图的界面 曲线作为图片URL上传）coode=500
     *http://122.14.213.160:7081/jeefast-rest/api/mail?token=admin&pid=1&img=http://www.uphonechina.com/upload/test/1578107695447.png
     * */
    @Headers(ServiceUrl)
    @Multipart
    @POST("mail")
    Observable<BaseBean> mail(@PartMap Map<String, RequestBody> requestBodyMap);


    /**
     * 患者详情
     * http://122.14.213.160:7081/jeefast-rest/api/patient/detail?patId=1&token=9b735053-a67c-4ec4-91be-288f8325e26c
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST(" patient/detail")
    Observable<BaseBean<PatientDetailBean>> patientDetail(@PartMap Map<String, RequestBody> requestBodyMap);

    /**
     * 模板详情
     * http://122.14.213.160:7081/jeefast-rest/api/template/detail?tempId=1&token0904e01d-7675-420c-9e54-151fca56d96e
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("template/detail")
    Observable<BaseBean<DictBean>> getDict(@PartMap Map<String, RequestBody> requestBodyMap);

    /**
     * 审核未通过列表
     * http://122.14.213.160:7081/jeefast-rest/api/researchData/auditFailed?docId=1&token=admin&page=1&limit=10
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("researchData/auditFailed")
    Observable<BaseBean<PagesBean<AuditFailedBean>>> auditFailed(@PartMap Map<String, RequestBody> requestBodyMap);

    /**
     * 导出患者列表到邮箱
     * http://122.14.213.160:7081/jeefast-rest/api/list?token=admin&mail=370941835@qq.com&ids=1,2
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("export/list")
    Observable<BaseBean> patientMailList(@PartMap Map<String, RequestBody> requestBodyMap);


    /**
     * 信息反馈
     * http://122.14.213.160:7081/jeefast-rest/api/list?token=admin&fdInfo=1&uid=1
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("feedback/insert")
    Observable<BaseBean> feedback(@PartMap Map<String, RequestBody> requestBodyMap);


    /**
     * 添加患者信息时，治疗方案、透析方式、病因是否显示
     * http://122.14.213.160:7081/jeefast-rest/api/resBsaseInfo/list?token=0904e01d-7675-420c-9e54-151fca56d96e&projId=11&resId=1
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("resBsaseInfo/list")
    Observable<resBsaseInfoBean> resBsaseInfo(@PartMap Map<String, RequestBody> requestBodyMap);

    /**
     * 根据dataId获取研究数据
     * http://122.14.213.160:7081/jeefast-rest/api/researchData/detail?token=admin&patId=1&dataId=1
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("researchData/detail")
    Observable<BaseBean<ResearchDataDetailBean>> researchDataDetail(@PartMap Map<String, RequestBody> requestBodyMap);


    /**
     * 患者搜索（搜索条件）
     * http://122.14.213.160:7081/jeefast-rest/api/researchCondition?token=admin&resId=1
            */
    @Headers(ServiceUrl)
    @Multipart
    @POST("researchCondition")
    Observable<BaseBean<ResearchConditionBean>> researchCondition(@PartMap Map<String, RequestBody> requestBodyMap);

    /**
     * 删除患者
     * http://122.14.213.160:7081/jeefast-rest/api/patient/delete?token=5063d135-27cf-41c7-8956-125cdc7ab10a&id=11159274
     */
    @Headers(ServiceUrl)
    @Multipart
    @POST("patient/delete")
    Observable<BaseBean> patientDelete(@PartMap Map<String, RequestBody> requestBodyMap);

   /* *//**
     * 添加患者信息时，治疗方案、透析方式、病因是否显示
     * http://122.14.213.160:7081/jeefast-rest/api/resBsaseInfo/list?token=admin&resId=1
     *//*
    @Headers(ServiceUrl)
    @Multipart
    @POST("resBsaseInfo/list")
    Observable<BaseBean> resBsaseInfo(@PartMap Map<String, RequestBody> requestBodyMap);*/




}
