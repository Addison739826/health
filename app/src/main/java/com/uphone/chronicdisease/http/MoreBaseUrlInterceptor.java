package com.uphone.chronicdisease.http;

import android.text.TextUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class MoreBaseUrlInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        HttpUrl oldUrl = originalRequest.url();
        Request.Builder builder = originalRequest.newBuilder();
        //获取头信息中配置的的集合如：loginSys, his
        List<String> urlnameList = originalRequest.headers("urlname");
        if (urlnameList != null && urlnameList.size() > 0) {
            builder.removeHeader("urlname");
            String urlname = urlnameList.get(0);
            HttpUrl baseURL = null;
            //根据头信息中配置的value,来匹配新的base_url地址
           /* if ("auth".equals(urlname)) {
                baseURL = HttpUrl.parse(API_BASE_BASE_URL);
            } else if (TextUtils.equals("shop4", urlname)) {
                baseURL = HttpUrl.parse(Constant.API_BASE_SHOP_FOUR_URL);
            } else if (TextUtils.equals("basics", urlname)) {
                baseURL = HttpUrl.parse(Constant.API_BASE_SHOP_FOUR_URL_BASICS);
            } else if (TextUtils.equals("his", urlname)) {
                baseURL = HttpUrl.parse(Constant.API_BASE_SHOP_FOUR_URL_HIS);
            }*/
            baseURL = HttpUrl.parse("http://122.14.213.160:7081/jeefast-rest/api/");
            List<String> pathSegments = oldUrl.encodedPathSegments();

            HttpUrl.Builder builderTemp = oldUrl.newBuilder()
                    .scheme(baseURL.scheme())//http协议如：http或者https
                    .host(baseURL.host())//主机地址
                    .port(baseURL.port());//端口;
            for (int i = pathSegments.size() - 1; i >= 0; i--) {
                builderTemp.removePathSegment(i);
            }
            for (String string : pathSegments) {
                if (TextUtils.equals(string, "oa")) {
                    builderTemp.addEncodedPathSegment(urlname);
                } else {
                    builderTemp.addEncodedPathSegment(string);

                }
            }
            HttpUrl newHttpUrl = builderTemp.build();
            Request newRequest = builder.url(newHttpUrl).build();
            return chain.proceed(newRequest);
        } else {
            return chain.proceed(originalRequest);
        }

    }
}
