package com.uphone.chronicdisease.util;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.radish.baselibrary.Intent.IntentUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.uphone.chronicdisease.util.refresh.MyHouseListFooter;
import com.uphone.chronicdisease.util.refresh.MyHouseListHeader;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * <pre>
 *     author : radish
 *     e-mail : 15703379121@163.com
 *     time   : 2019/3/16
 *     desc   :
 * </pre>
 */
public class CommonUtil {

    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String TITLE = "title";

    public static void startIntent(Context context, Class clazz) {
        IntentUtils.getInstance().with(context, clazz).start();
    }

    public static void startIntent(Context context, Class clazz, int id) {
        IntentUtils.getInstance().with(context, clazz).putInt(ID, id).start();
    }

    public static void startIntent(Context context, Class clazz, int id, int type) {
        IntentUtils.getInstance().with(context, clazz).putInt(TYPE, type).putInt(ID, id).start();
    }

    public static void startIntentType(Context context, Class clazz, int type) {
        IntentUtils.getInstance().with(context, clazz).putInt(TYPE, type).start();
    }


    public static void startIntentTitle(Context context, Class clazz, String title) {
        IntentUtils.getInstance().with(context, clazz).putString(TITLE, title).start();
    }

    public static boolean checkViewEmpty(TextView tv) {
        return checkViewEmpty(null, tv);
    }

    public static boolean checkViewEmpty(String[] msgs, TextView... tvs) {
        for (int i = 0; i < tvs.length; i++) {
            String msg = null;
            if (msgs != null && msgs.length > i) {
                msg = msgs[i];
            }
            if (checkViewEmpty(msg, tvs[i])) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkViewEmpty(TextView... tvs) {
        return checkViewEmpty(null, tvs);
    }

    public static boolean checkViewEmpty(String msg, TextView tv) {
        if (tv != null) {
            if (!TextUtils.isEmpty(tv.getText().toString().trim())) {
                return false;
            }
            if (!TextUtils.isEmpty(msg)) {
                ToastUtils.showShort(msg);
            }
            return true;
        }
        if (!TextUtils.isEmpty(msg)) {
            ToastUtils.showShort(msg);
        }
        return true;
    }

    public static String getStr(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return str;
    }

    public static String getStr(String str, String defaultStr) {
        if (TextUtils.isEmpty(str)) {
            return getStr(defaultStr);
        }
        return str;
    }

    public static List getList(int size) {
        List list = new ArrayList();
        for (int i = 0; i < size; i++) {
            list.add(new Object());
        }
        return list;
    }

    public static <T> List<T> getList(int size, T t) {
        List<T> list = new ArrayList();
        for (int i = 0; i < size; i++) {
            list.add(t);
        }
        return list;
    }

    public static Class<?> analysisClazzInfo(Object object) {
        Type rootType = object.getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) rootType).getActualTypeArguments();
        return (Class<?>) params[0];
    }

    public static <T> List<T> getList(List<T> list, int count) {
        List<T> tempList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (i >= count) {
                break;
            }
            tempList.add(list.get(i));
        }
        return tempList;
    }

    public static int getRandom(int n) {
        return new Random().nextInt(n);
    }

    public static void initRefresh(Activity context, SmartRefreshLayout refreshLayout) {
        refreshLayout.setRefreshHeader(new MyHouseListHeader(context, (ViewGroup) context.getWindow().getDecorView()));
        refreshLayout.setRefreshFooter(new MyHouseListFooter(context, (ViewGroup) context.getWindow().getDecorView()));
    }

    /**
     * 因为后台需要的参数体是from-data形式的，进行转换
     *
     * @param requestDataMap
     * @return
     */
    public static Map<String, RequestBody> setParams(Map<String, String> requestDataMap) {
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        //requestDataMap.put("token",   SPUtils.getInstance().getString("token"));
        //requestDataMap.put("docId", SPUtils.getInstance().getString("token"));
        for (String key : requestDataMap.keySet()) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),
                    requestDataMap.get(key) == null ? "" : requestDataMap.get(key));
            requestBodyMap.put(key, requestBody);
        }
        return requestBodyMap;
    }

    /**
     * 因为后台需要的参数体是from-data形式的，进行转换
     *
     * @param requestDataMap
     * @return
     */
    public static Map<String, RequestBody> setParams1(Map<String, Object> requestDataMap) {
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
//        requestDataMap.put("token","admin");
//        requestDataMap.put("docId","1");
        for (String key : requestDataMap.keySet()) {
            Log.e("addison", new Gson().toJson(requestDataMap.get(key)));
            RequestBody requestBody;
            if (requestDataMap.get(key) instanceof String) {
                requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),
                        requestDataMap.get(key).toString());
            } else {
                requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),
                        new Gson().toJson(requestDataMap.get(key)));
            }
            requestBodyMap.put(key, requestBody);
        }
        return requestBodyMap;
    }


}
