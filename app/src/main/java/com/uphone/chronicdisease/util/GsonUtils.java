package com.uphone.chronicdisease.util;

import com.google.gson.Gson;

/**
 * author : radish
 * e-mail : 15703379121@163.com
 * time   : 2019/06/29
 * desc   : xxxx 描述
 */
public class GsonUtils {
    private static Gson gson;

    public static Gson getGson() {
        if (gson == null)
            synchronized (GsonUtils.class) {
                if (gson == null)
                    gson = new Gson();
            }
        return gson;
    }
}
