package com.uphone.chronicdisease.util.chart;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.uphone.chronicdisease.bean.CheckBean;
import com.uphone.chronicdisease.bean.PatientDataBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: lzy
 * @CreateDate: 2019/12/28
 * @Description:
 */
public class XDataFormatter extends ValueFormatter {
    List<CheckBean> list = new ArrayList<>();

    public XDataFormatter(List<PatientDataBean.DataBean> mlist) {
        for (int i = 0; i < mlist.size(); i++) {
            if (mlist.get(i).isChecked()) {
                list.add(new CheckBean(mlist.get(i).getDdValue()));
            }
        }
    }

    @Override
    public String getFormattedValue(float value) {
        if (value >= 0 && list.size() > 0) {
            return list.get((int) value % list.size()).getText();
        } else {
            return "";
        }
    }
}
