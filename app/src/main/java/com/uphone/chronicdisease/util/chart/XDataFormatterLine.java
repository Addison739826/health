package com.uphone.chronicdisease.util.chart;

import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: lzy
 * @CreateDate: 2019/12/28
 * @Description:
 */
public class XDataFormatterLine extends ValueFormatter {
    List<String> list = new ArrayList<>();

    public XDataFormatterLine(List<String> mlist) {
       list.addAll(mlist);
    }

    @Override
    public String getFormattedValue(float value) {
        if (value >= 0 && list.size() > 0) {
            return list.get((int) value % list.size());
        } else {
            return "";
        }
    }
}
