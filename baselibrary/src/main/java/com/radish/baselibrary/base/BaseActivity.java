package com.radish.baselibrary.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.radish.baselibrary.R;
import com.radish.baselibrary.utils.ActivityCollector;

/**
 * @作者 radish
 * @创建日期 2018/11/20 4:02 PM
 * @邮箱 15703379121@163.com
 */
public abstract class BaseActivity extends AppCompatActivity {

    private Intent intent;
    protected LinearLayout llRoot;
    public AppCompatActivity mActivity;//当做Context去使用
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        initSuperBefore();
        super.onCreate(savedInstanceState);
        mActivity = this;
        ActivityCollector.addActivity(this);

        initLayoutBefore();

        initBundle();

        //设置布局layout
        setContentView(R.layout.activity_base);
        llRoot = findView(R.id.ll_root);
        llRoot.addView(View.inflate(this,initLayout(), null),new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        initLayoutAfter();

        initView();

        initTitle();

        initData();

        initListener();

    }

    protected void initSuperBefore() {

    }

    protected void initLayoutAfter() {

    }

    protected void initLayoutBefore() {

    }

    protected abstract void initBundle();

    protected abstract int initLayout();

    protected abstract void initView();

    protected abstract void initTitle();

    protected abstract void initData();

    protected abstract void initListener();

    protected void startActivity(Class<?> clazz) {
        intent = new Intent(this, clazz);
        startActivity(intent);
    }

    protected void startActivity(Class<?> clazz, Bundle bundle, int requestCode) {
        intent = new Intent(this, clazz);
        intent.putExtras(bundle);
        startActivityForResult(intent, requestCode);
    }

    protected <T extends View> T findView(@IdRes int viewId) {
        return findViewById(viewId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityCollector.removeActivity(this);
    }
}
